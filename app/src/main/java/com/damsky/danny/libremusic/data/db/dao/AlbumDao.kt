package com.damsky.danny.libremusic.data.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.damsky.danny.libremusic.data.db.entity.Album
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayAlbum

/**
 * An interface that contains all the functions to be implemented by Room for working with
 * the albums table in the database.
 *
 * @author Danny Damsky
 * @see Album
 * @see BaseDao
 */
@Dao
interface AlbumDao : BaseDao<Album> {

    //******************************************************************//
    //******************GENERAL**DAO**FUNCTIONS************************//
    //****************************************************************//

    /**
     * Empties the albums table in the database.
     */
    @Query("DELETE FROM ALBUMS;")
    fun deleteAll()

    /**
     * @return a [LiveData] of a [List] of [DisplayAlbum]s. This LiveData represents all albums in
     * the database, it includes all of [Album]'s columns as well as another column representing the
     * [Album]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ALBUM_NAME=ALBUM_NAME) as SONGS_COUNT FROM ALBUMS;")
    fun getAllDisplayLive(): LiveData<List<DisplayAlbum>>

    /**
     * @return a [LiveData] of a [List] of [DisplayAlbum]s ordered by the album name in ascending order.
     * This LiveData represents all albums in the database,
     * it includes all of [Album]'s columns as well as another column representing the [Album]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ALBUM_NAME=ALBUM_NAME) as SONGS_COUNT FROM ALBUMS ORDER BY ALBUM_NAME ASC;")
    fun getAllDisplayOrderedByNameAscLive(): LiveData<List<DisplayAlbum>>

    /**
     * @return a [LiveData] of a [List] of [DisplayAlbum]s ordered by the album name in descending order.
     * This LiveData represents all albums in the database,
     * it includes all of [Album]'s columns as well as another column representing the [Album]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ALBUM_NAME=ALBUM_NAME) as SONGS_COUNT FROM ALBUMS ORDER BY ALBUM_NAME DESC;")
    fun getAllDisplayOrderedByNameDescLive(): LiveData<List<DisplayAlbum>>

    /**
     * @return a [LiveData] of a [List] of [DisplayAlbum]s ordered by the album's release year in ascending order.
     * This LiveData represents all albums in the database,
     * it includes all of [Album]'s columns as well as another column representing the [Album]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ALBUM_NAME=ALBUM_NAME) as SONGS_COUNT FROM ALBUMS ORDER BY ALBUM_YEAR_PUBLISHED ASC;")
    fun getAllDisplayOrderedByYearAscLive(): LiveData<List<DisplayAlbum>>

    /**
     * @return a [LiveData] of a [List] of [DisplayAlbum]s ordered by the album's release year in descending order.
     * This LiveData represents all albums in the database,
     * it includes all of [Album]'s columns as well as another column representing the [Album]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ALBUM_NAME=ALBUM_NAME) as SONGS_COUNT FROM ALBUMS ORDER BY ALBUM_YEAR_PUBLISHED DESC;")
    fun getAllDisplayOrderedByYearDescLive(): LiveData<List<DisplayAlbum>>

    /**
     * @return a [LiveData] of a [List] of [DisplayAlbum]s ordered by the album's artist name in ascending order.
     * This LiveData represents all albums in the database,
     * it includes all of [Album]'s columns as well as another column representing the [Album]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ALBUM_NAME=ALBUM_NAME) as SONGS_COUNT FROM ALBUMS ORDER BY ALBUM_ARTIST_NAME ASC;")
    fun getAllDisplayOrderedByArtistNameAscLive(): LiveData<List<DisplayAlbum>>

    /**
     * @return a [LiveData] of a [List] of [DisplayAlbum]s ordered by the album's artist name in descending order.
     * This LiveData represents all albums in the database,
     * it includes all of [Album]'s columns as well as another column representing the [Album]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ALBUM_NAME=ALBUM_NAME) as SONGS_COUNT FROM ALBUMS ORDER BY ALBUM_ARTIST_NAME DESC;")
    fun getAllDisplayOrderedByArtistNameDescLive(): LiveData<List<DisplayAlbum>>

    //******************************************************************//
    //****************ARTIST**ONE**TO**MANY**ALBUMS********************//
    //****************************************************************//

    /**
     * @param artistName the name of the albums' artist.
     * @return a [LiveData] of a [List] of [DisplayAlbum]s. This LiveData represents all albums in
     * the database for the given [artistName]. It includes all of [Album]'s columns as well as another column representing the
     * [Album]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ALBUM_NAME=ALBUM_NAME) as SONGS_COUNT FROM ALBUMS WHERE ALBUM_ARTIST_NAME=:artistName;")
    fun getAllDisplayForArtistLive(artistName: String): LiveData<List<DisplayAlbum>>

    /**
     * @param artistName the name of the albums' artist.
     * @return a [LiveData] of a [List] of [DisplayAlbum]s ordered by the album name in ascending order.
     * This LiveData represents all albums in the database for the given [artistName].
     * It includes all of [Album]'s columns as well as another column representing the [Album]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ALBUM_NAME=ALBUM_NAME) as SONGS_COUNT FROM ALBUMS WHERE ALBUM_ARTIST_NAME=:artistName ORDER BY ALBUM_NAME ASC;")
    fun getAllDisplayForArtistOrderedByNameAscLive(artistName: String): LiveData<List<DisplayAlbum>>

    /**
     * @param artistName the name of the albums' artist.
     * @return a [LiveData] of a [List] of [DisplayAlbum]s ordered by the album name in descending order.
     * This LiveData represents all albums in the database for the given [artistName].
     * It includes all of [Album]'s columns as well as another column representing the [Album]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ALBUM_NAME=ALBUM_NAME) as SONGS_COUNT FROM ALBUMS WHERE ALBUM_ARTIST_NAME=:artistName ORDER BY ALBUM_NAME DESC;")
    fun getAllDisplayForArtistOrderedByNameDescLive(artistName: String): LiveData<List<DisplayAlbum>>

    /**
     * @param artistName the name of the albums' artist.
     * @return a [LiveData] of a [List] of [DisplayAlbum]s ordered by the album's release year in ascending order.
     * This LiveData represents all albums in the database for the given [artistName].
     * It includes all of [Album]'s columns as well as another column representing the [Album]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ALBUM_NAME=ALBUM_NAME) as SONGS_COUNT FROM ALBUMS WHERE ALBUM_ARTIST_NAME=:artistName ORDER BY ALBUM_YEAR_PUBLISHED ASC;")
    fun getAllDisplayForArtistOrderedByYearAscLive(artistName: String): LiveData<List<DisplayAlbum>>

    /**
     * @param artistName the name of the albums' artist.
     * @return a [LiveData] of a [List] of [DisplayAlbum]s ordered by the album's release year in descending order.
     * This LiveData represents all albums in the database for the given [artistName].
     * It includes all of [Album]'s columns as well as another column representing the [Album]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ALBUM_NAME=ALBUM_NAME) as SONGS_COUNT FROM ALBUMS WHERE ALBUM_ARTIST_NAME=:artistName ORDER BY ALBUM_YEAR_PUBLISHED DESC;")
    fun getAllDisplayForArtistOrderedByYearDescLive(artistName: String): LiveData<List<DisplayAlbum>>
}