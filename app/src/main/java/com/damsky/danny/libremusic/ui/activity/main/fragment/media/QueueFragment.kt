package com.damsky.danny.libremusic.ui.activity.main.fragment.media

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplaySong
import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnQueueChangedListener
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.AbstractSongRowItem
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapter
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks
import com.damsky.danny.libremusic.util.RowItemMenuAction

/**
 * An implementation of [AbstractMediaFragment].
 * This implementation displays a list of Songs that represent the current Queue.
 *
 * @author Danny Damsky
 */
class QueueFragment : AbstractMediaFragment(), OnQueueChangedListener {
    override var isRegistered: Boolean = false

    override val songCallbacks = object : MainActivityRecyclerViewAdapterCallbacks<DisplaySong> {
        override fun onRowClick(item: DisplaySong, itemPosition: Int) =
                onSongCallbacksActionPlayForQueue(itemPosition)

        override fun onRowMenuClick(item: DisplaySong, itemPosition: Int, menuAction: RowItemMenuAction) = when (menuAction) {
            RowItemMenuAction.ACTION_PLAY -> onSongCallbacksActionPlayForQueue(itemPosition)
            RowItemMenuAction.ACTION_ADD_TO_QUEUE -> onSongCallbacksActionAddToQueue(item.id!!)
            RowItemMenuAction.ACTION_ADD_TO_PLAYLIST -> onSongCallbacksActionAddToPlaylist(item.id!!)
            RowItemMenuAction.ACTION_SHARE -> onSongCallbacksActionShare(item.id!!)
            RowItemMenuAction.ACTION_SET_AS_RINGTONE -> onSongCallbacksActionSetAsRingtone(item)
            RowItemMenuAction.ACTION_REMOVE_FROM_QUEUE -> onSongCallbacksRemoveFromQueue(itemPosition)
            RowItemMenuAction.ACTION_VIEW_ALL_SONGS,
            RowItemMenuAction.ACTION_RENAME_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_FROM_PLAYLIST ->
                onSongCallbacksInvalidAction(item, itemPosition, menuAction)
        }
    }

    private fun onSongCallbacksActionPlayForQueue(itemPosition: Int) {
        roomDbViewModel.songQueue.setCurrentSongIndex(itemPosition)
        MediaPlayerService.start(mainActivity)
    }

    private fun onSongCallbacksRemoveFromQueue(itemPosition: Int) {
        if (!roomDbViewModel.songQueue.removeSongAt(itemPosition))
            mainActivity.showSnackBarLong(R.string.error_queue_almost_empty)
    }

    private val songsAdapterObserver = object : RecyclerView.AdapterDataObserver() {

    }

    private lateinit var songsAdapter: MainActivityRecyclerViewAdapter<DisplaySong, AbstractSongRowItem>

    override fun onViewStubInflated() {
        initSongsAdapter()
        initRecyclerViewProperties()
        setSongsAdapter()
        registerOnQueueChangedListener()
    }

    override fun onDestroyView() {
        unregisterOnQueueChangedListener()
        super.onDestroyView()
    }

    private fun initSongsAdapter() {
        songsAdapter = MainActivityRecyclerViewAdapter(AbstractSongRowItem.DIFF_CALLBACK, songCallbacks)
    }

    private fun initRecyclerViewProperties() {
        recyclerView.layoutManager = LinearLayoutManager(mainActivity)
        recyclerView.setHasFixedSize(true)
    }

    private fun setSongsAdapter() {
        songsAdapter.registerAdapterDataObserver(songsAdapterObserver)
        recyclerView.adapter = songsAdapter
    }

    private fun registerOnQueueChangedListener() =
            roomDbViewModel.songQueue.registerOnQueueChangedListener(this)

    private fun unregisterOnQueueChangedListener() =
            roomDbViewModel.songQueue.unregisterOnQueueChangedListener(this)

    override fun onQueueChanged(queue: List<AbstractSongRowItem>) = songsAdapter.submitList(queue)

    override fun onBackPressed() = true

    override fun reset() = recyclerView.smoothScrollToPosition(0)
}