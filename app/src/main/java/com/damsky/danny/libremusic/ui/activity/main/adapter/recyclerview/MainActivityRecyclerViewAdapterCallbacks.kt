package com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview

import com.damsky.danny.libremusic.util.RowItemMenuAction

/**
 * This interface consists of callback functions from the RecyclerView's adapter - [MainActivityRecyclerViewAdapter].
 *
 * @param T the type of this callback must be the same type as the items that the [MainActivityRecyclerViewAdapter]
 * takes in.
 *
 * @author Danny Damsky
 */
interface MainActivityRecyclerViewAdapterCallbacks<T> {

    /**
     * This call is triggered after a row in the RecyclerView was clicked.
     *
     * @param item the object that represents the row in the RecyclerView.
     * @param itemPosition the position of the item in the RecyclerView.
     */
    fun onRowClick(item: T, itemPosition: Int)

    /**
     * This call is triggered after an item was pressed on the RecyclerView's row's PopupMenu.
     *
     * @param item the object that represents the row in the RecyclerView.
     * @param itemPosition the position of the item in the RecyclerView.
     * @param menuAction the action representing the MenuItem in the row's PopupMenu.
     */
    fun onRowMenuClick(item: T, itemPosition: Int, menuAction: RowItemMenuAction)

}