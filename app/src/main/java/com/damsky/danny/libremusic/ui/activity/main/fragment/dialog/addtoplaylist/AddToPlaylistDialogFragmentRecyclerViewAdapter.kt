package com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.addtoplaylist

import android.support.annotation.IntRange
import android.support.v4.util.ArraySet
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayPlaylist
import kotlinx.android.synthetic.main.row_recyclerview_dialog_add_to_playlist.view.*

/**
 * The [RecyclerView.Adapter] for [AddToPlaylistDialogFragment].
 * It displays the names of the available playlists alongside a checkbox indicating whether or not
 * they should be linked with the corresponding songs from [AddToPlaylistDialogFragment].
 *
 * The IDs of the selected playlists can be accessed by calling [getSelectedIds].
 * The IDs of the unselected playlists can be accessed by calling [getUnselectedIds].
 *
 * @param playlists the complete [List] of [DisplayPlaylist]s from the database.
 * @param songsCount the total amount of songs that will be linked to each playlist.
 * The smallest value here should be 1.
 *
 * @author Danny Damsky
 */
class AddToPlaylistDialogFragmentRecyclerViewAdapter(private val playlists: List<DisplayPlaylist>,
                                                     @IntRange(from = 1) private val songsCount: Int) :
        RecyclerView.Adapter<AddToPlaylistDialogFragmentRecyclerViewAdapter.ViewHolder>() {

    private val selectedIds = ArraySet<Long>()
    private val unselectedIds = ArraySet<Long>()

    override fun onCreateViewHolder(viewHolder: ViewGroup, position: Int): ViewHolder {
        val itemView = LayoutInflater.from(viewHolder.context)
                .inflate(R.layout.row_recyclerview_dialog_add_to_playlist, viewHolder, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount() = playlists.size

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val displayPlaylist = playlists[position]
        val id = displayPlaylist.playlist.id!!
        viewHolder.checkbox.text = displayPlaylist.playlist.name
        configureCurrentState(viewHolder, displayPlaylist, id)
        configureCheckboxListener(viewHolder, id)
    }

    private fun configureCurrentState(viewHolder: ViewHolder, displayPlaylist: DisplayPlaylist, id: Long) {
        when {
            selectedIds.contains(id) -> viewHolder.checkbox.isChecked = true
            unselectedIds.contains(id) -> viewHolder.checkbox.isChecked = false
            displayPlaylist.songsCount == this.songsCount -> onSongsExistInPlaylist(viewHolder, id)
            else -> onSongsDontExistInPlaylist(viewHolder, id)
        }
    }

    private fun onSongsExistInPlaylist(viewHolder: ViewHolder, playlistId: Long) {
        selectId(playlistId)
        viewHolder.checkbox.isChecked = true
    }

    private fun onSongsDontExistInPlaylist(viewHolder: ViewHolder, playlistId: Long) {
        unSelectId(playlistId)
        viewHolder.checkbox.isChecked = false
    }

    private fun configureCheckboxListener(viewHolder: ViewHolder, playlistId: Long) =
            viewHolder.checkbox.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked)
                    selectId(playlistId)
                else
                    unSelectId(playlistId)
            }

    private fun selectId(id: Long) {
        selectedIds.add(id)
        unselectedIds.remove(id)
    }

    private fun unSelectId(id: Long) {
        selectedIds.remove(id)
        unselectedIds.add(id)
    }

    /**
     * @return a [Set] containing the IDs of the selected playlist items. These are the items
     * that should be linked to the selected songs from the [AddToPlaylistDialogFragment].
     */
    fun getSelectedIds(): Set<Long> = selectedIds

    /**
     * @return a [Set] containing the IDs of the unselected playlist items. These are the items
     * that should be removed from the selected songs from the [AddToPlaylistDialogFragment].
     */
    fun getUnselectedIds(): Set<Long> = unselectedIds

    override fun onViewRecycled(holder: ViewHolder) {
        holder.checkbox.setOnCheckedChangeListener(null)
        super.onViewRecycled(holder)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val checkbox: CheckBox = itemView.checkbox
    }

}