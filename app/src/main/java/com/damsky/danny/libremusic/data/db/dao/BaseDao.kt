package com.damsky.danny.libremusic.data.db.dao

import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.IGNORE
import android.arch.persistence.room.Update

/**
 * An interface that serves as the base for all DAO (Data Access Object) implementations.
 * It contains basic CRUD functions.
 *
 * @author Danny Damsky
 */
interface BaseDao<T> {

    /**
     * Inserts an object into the selected table in the database.
     *
     * @param item the object to insert.
     */
    @Insert(onConflict = IGNORE)
    fun insert(item: T)

    /**
     * Inserts multiple objects into the selected table in the database.
     *
     * @param items the objects to insert.
     */
    @Insert(onConflict = IGNORE)
    fun insertAll(items: List<T>)

    /**
     * Updates an object in the selected table in the database.
     *
     * @param item the object to update.
     */
    @Update
    fun update(item: T)

    /**
     * Deletes an object from the selected table in the database.
     *
     * @param item the object to delete.
     */
    @Delete
    fun delete(item: T)

}