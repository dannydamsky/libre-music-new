package com.damsky.danny.libremusic.util

import android.util.Log
import com.crashlytics.android.Crashlytics

/**
 * A utility class for system-wide access to Crashlytics functions.
 *
 * @author Danny Damsky
 * @see Crashlytics
 */
class CrashUtils {

    companion object {

        /**
         * Logs an error message (Highest priority) using [Crashlytics].
         *
         * @param tag the tag of the calling class.
         * @param msg the error message.
         */
        @JvmStatic
        fun logError(tag: String, msg: String) = Crashlytics.log(Log.ERROR, tag, msg)

        /**
         * Logs an exception using [Crashlytics].
         *
         * @param throwable the exception to be logged.
         */
        @JvmStatic
        fun logException(throwable: Throwable) = Crashlytics.logException(throwable)
    }

}