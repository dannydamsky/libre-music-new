package com.damsky.danny.libremusic.ui.activity.main

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.RoomDbViewModel
import com.damsky.danny.libremusic.data.prefs.SharedPreferencesHelper
import com.damsky.danny.libremusic.service.LibraryScanService
import com.damsky.danny.libremusic.ui.activity.main.adapter.MainActivityViewPagerAdapter
import com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.NowPlayingDialogFragment
import com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.WelcomeDialogFragment
import com.damsky.danny.libremusic.ui.activity.main.widget.NowPlayingPanel
import com.damsky.danny.libremusic.util.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

/**
 * This class contains the default implementation of [MainActivity]'s functions.
 * It extends [IMainActivity] which is an interface that contains all of [MainActivity]'s callbacks.
 * The calls to it are passed from [MainActivity].
 *
 * @param mainActivity the current [MainActivity] instance.
 *
 * @author Danny Damsky
 * @see MainActivity
 * @see IMainActivity
 * @see IMainActivityExtras
 */
class MainActivityDefaultImpl(private val mainActivity: MainActivity) : IMainActivity,
        NowPlayingPanel.OnClickListener {

    private lateinit var viewPagerAdapter: MainActivityViewPagerAdapter
    private lateinit var roomDbViewModel: RoomDbViewModel

    override fun onCreate(savedInstanceState: Bundle?) = when {
        PermissionUtils.allGranted(mainActivity) -> createActivity()

        SharedPreferencesHelper.getInstance(mainActivity).isFirstRun() ->
            WelcomeDialogFragment.show(mainActivity.supportFragmentManager)

        else -> IntentUtils.requestPermissions(mainActivity, Constants.REQUEST_CODE_PERMISSIONS)
    }

    private fun createActivity() {
        mainActivity.setContentView(R.layout.activity_main)
        mainActivity.setSupportActionBar(mainActivity.toolbar)
        roomDbViewModel = ViewModelProviders.of(mainActivity).get(RoomDbViewModel::class.java)
        viewPagerAdapter = MainActivityViewPagerAdapter.setup(mainActivity, mainActivity.tabLayout, mainActivity.viewPager)
        LibraryScanService.startScan(mainActivity)
        configureViewPagerBehaviour()
        mainActivity.nowPlayingPanel.setOnClickListener(this)
    }

    private fun configureViewPagerBehaviour() {
        mainActivity.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {
                viewPagerAdapter.getFragmentAt(p0!!.position).reset()
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) = Unit
            override fun onTabSelected(p0: TabLayout.Tab?) = Unit
        })
    }

    override fun onBackPressed() {
        if (viewPagerAdapter.getFragmentAt(mainActivity.viewPager.currentItem).onBackPressed())
            mainActivity.finish()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (!PermissionUtils.allGranted(grantResults))
            mainActivity.finish()
        else {
            SharedPreferencesHelper.getInstance(mainActivity).setFirstRun(false)
            createActivity()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) =
            when (requestCode) {
                Constants.REQUEST_CODE_MODIFY_SYSTEM_PERMISSIONS ->
                    onRequestCodeModifySystemSettings()

                else -> onUnknownRequestCode(requestCode, resultCode, data)
            }

    private fun onRequestCodeModifySystemSettings() {
        if (PermissionUtils.canModifySystemSettings(mainActivity)) {
            val songId = mainActivity.intent.getLongExtra(Constants.EXTRA_RINGTONE_SONG_ID, 0)
            RingtoneUtils.setRingtone(mainActivity, songId) {
                showSnackBarShort(R.string.ringtone_success)
            }
        }
        mainActivity.intent.removeExtra(Constants.EXTRA_RINGTONE_SONG_ID)
    }

    private fun onUnknownRequestCode(requestCode: Int, resultCode: Int, data: Intent?) {
        CrashUtils.logError("MainActivityDefaultImpl.onActivityResult",
                "Unknown request code: requestCode=$requestCode, resultCode=$resultCode, data=$data")
    }

    override fun onWelcomeDialogPositiveButtonPressed() =
            IntentUtils.requestPermissions(mainActivity, Constants.REQUEST_CODE_PERMISSIONS)

    override fun onWelcomeDialogNegativeButtonPressed() = mainActivity.finish()

    override fun showSnackBarShort(textId: Int) = showSnackBar(textId, Snackbar.LENGTH_SHORT)

    override fun showSnackBarLong(textId: Int) = showSnackBar(textId, Snackbar.LENGTH_LONG)

    private fun showSnackBar(textId: Int, length: Int) {
        Snackbar.make(mainActivity.container, textId, length).apply {
            view.layoutParams = (view.layoutParams as CoordinatorLayout.LayoutParams).apply {
                setMargins(leftMargin, topMargin, rightMargin, mainActivity.nowPlayingPanel.height)
            }
        }.show()
    }

    override fun onNowPlayingPanelSheetClicked() =
            NowPlayingDialogFragment.show(mainActivity.supportFragmentManager)
}