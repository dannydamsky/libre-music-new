package com.damsky.danny.libremusic.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.damsky.danny.libremusic.data.db.RoomDb.Companion.getInstance
import com.damsky.danny.libremusic.data.db.dao.*
import com.damsky.danny.libremusic.data.db.entity.*

/**
 * An abstract class that is implemented behind-the-scenes by the Room database, this class
 * contains functions to access all of the database's DAO (Data Access Object) objects.
 *
 * To get an implemented instance of this class, call [getInstance].
 *
 * @author Danny Damsky
 */
@Database(entities = [Artist::class, Album::class, Song::class, Playlist::class, Genre::class,
    SongsPlaylistsLink::class], version = RoomDbConstants.DB_VERSION, exportSchema = false)
abstract class RoomDb : RoomDatabase() {
    companion object {
        private var INSTANCE: RoomDb? = null

        /**
         * @param context the function accesses the applicationContext property.
         * @return a fully implemented instance of the [RoomDb] class.
         * If the instance doesn't exist yet it is created and returned.
         */
        @JvmStatic
        fun getInstance(context: Context): RoomDb {
            if (INSTANCE == null)
                synchronized(RoomDb::class) {
                    if (INSTANCE == null)
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                RoomDb::class.java, RoomDbConstants.DB_NAME).build()
                }
            return INSTANCE!!
        }

    }

    /**
     * @return the fully-implemented instance of the DAO (Data Access Object) for the artists table.
     */
    abstract fun getArtistDao(): ArtistDao

    /**
     * @return the fully-implemented instance of the DAO (Data Access Object) for the albums table.
     */
    abstract fun getAlbumDao(): AlbumDao

    /**
     * @return the fully-implemented instance of the DAO (Data Access Object) for the songs table.
     */
    abstract fun getSongDao(): SongDao

    /**
     * @return the fully-implemented instance of the DAO (Data Access Object) for the playlists table.
     */
    abstract fun getPlaylistDao(): PlaylistDao

    /**
     * @return the fully-implemented instance of the DAO (Data Access Object) for the genres table.
     */
    abstract fun getGenreDao(): GenreDao

    /**
     * @return the fully-implemented instance of the DAO (Data Access Object) for the table linking the songs to the playlists.
     */
    abstract fun getSongsPlaylistsLinkDao(): SongsPlaylistsLinkDao
}
