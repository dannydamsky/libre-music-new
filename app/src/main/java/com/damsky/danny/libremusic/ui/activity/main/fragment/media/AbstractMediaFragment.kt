package com.damsky.danny.libremusic.ui.activity.main.fragment.media

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.RoomDbViewModel
import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplaySong
import com.damsky.danny.libremusic.data.db.nonentity.sorting.SongSorting
import com.damsky.danny.libremusic.data.prefs.SharedPreferencesHelper
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService
import com.damsky.danny.libremusic.ui.activity.main.MainActivity
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks
import com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.CreatePlaylistDialogFragment
import com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.addtoplaylist.AddToPlaylistDialogFragment
import com.damsky.danny.libremusic.util.*
import kotlinx.android.synthetic.main.viewstub_fragment_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * This is an abstract [Fragment] class that inflates the [R.layout.fragment_main] layout.
 * It contains some helpful functions for derivative classes as well as some pre-configured
 * and abstract ones.
 *
 * @author Danny Damsky
 * @property mainActivity A reference to the [MainActivity], resets itself whenever [onAttach] is called.
 * @property roomDbViewModel A reference to [RoomDbViewModel], gets set once in [onAttach]
 * @property songCallbacks Callbacks from the RecyclerView.Adapter that deal with displaying song items.
 */
abstract class AbstractMediaFragment : Fragment() {

    protected lateinit var mainActivity: MainActivity
    protected lateinit var roomDbViewModel: RoomDbViewModel
    protected lateinit var recyclerView: RecyclerView
    private lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private var propertiesHaveBeenInitializedOnce = false
    private var hasInflated = false
    private var onViewCreated = false
    protected abstract val songCallbacks: MainActivityRecyclerViewAdapterCallbacks<DisplaySong>

    final override fun onAttach(context: Context) {
        super.onAttach(context)
        mainActivity = context as MainActivity
        if (!propertiesHaveBeenInitializedOnce) {
            roomDbViewModel = ViewModelProviders.of(this).get(RoomDbViewModel::class.java)
            sharedPreferencesHelper = SharedPreferencesHelper.getInstance(context)
            propertiesHaveBeenInitializedOnce = true
        }
    }

    final override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
            inflater.inflate(R.layout.viewstub_fragment_main, container, false)!!

    final override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (userVisibleHint && !hasInflated) {
            val inflatedView: View = fragmentViewStub.inflate()
            onViewStubInflated(inflatedView)
        }
        onViewCreated = true
    }

    final override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (onViewCreated && isVisibleToUser && !hasInflated) {
            val inflatedView: View = fragmentViewStub.inflate()
            onViewStubInflated(inflatedView)
        }
    }

    private fun onViewStubInflated(originalViewContainerWithViewStub: View?) {
        hasInflated = true
        if (originalViewContainerWithViewStub != null) {
            inflateProgressbar.visibility = View.GONE
            recyclerView = originalViewContainerWithViewStub.findViewById(R.id.recyclerView)
        }
        onViewStubInflated()
    }

    /**
     * The [AbstractMediaFragment] uses a Lazy Loading method for the fragment,
     * it only inflates the layout when the layout is visible to the user.
     * Therefore the regular [Fragment] methods aren't available when extending this class,
     * and instead the child class must override this function in order to implement [View]
     * behaviour.
     */
    abstract fun onViewStubInflated()

    final override fun onDetach() {
        super.onDetach()
        hasInflated = false
    }

    /**
     * Gets called when the back button is pressed from [MainActivity].
     *
     * @see MainActivity.onBackPressed
     * @return true if [MainActivity] should exit, false otherwise.
     */
    abstract fun onBackPressed(): Boolean

    /**
     * Resets the state of the [Fragment].
     */
    abstract fun reset()

    /**
     * An asynchronous function that inserts songs to the SongQueue.
     *
     * @param functionToGetPlayableSongs a function that takes a [SongSorting] instance as a parameter
     * and should return an [ArrayList] of [PlayableSong] items.
     */
    protected fun onOtherCallbacksAddToQueue(functionToGetPlayableSongs: (songSorting: SongSorting) -> ArrayList<PlayableSong>) {
        GlobalScope.launch(Dispatchers.IO) {
            val songs = functionToGetPlayableSongs(sharedPreferencesHelper.getSongSorting())
            roomDbViewModel.songQueue.addSongs(songs)
        }
    }

    /**
     * A function that displays either [AddToPlaylistDialogFragment] or [CreatePlaylistDialogFragment]
     * according to if there are available playlists in the database or not.
     *
     * @param functionToGetSongIds this function return a [List] of song IDs, they will be
     * passed onto [AddToPlaylistDialogFragment] in the case that there are available playlists
     * in the database.
     */
    protected fun onOtherCallbacksAddToPlaylist(functionToGetSongIds: () -> List<Long>) {
        GlobalScope.launch(Dispatchers.Main) {
            val playlistsCount = withContext(Dispatchers.IO) { roomDbViewModel.playlistDao.getCount() }
            if (playlistsCount > 0) {
                val songIds = withContext(Dispatchers.IO) { functionToGetSongIds() }
                AddToPlaylistDialogFragment.show(mainActivity.supportFragmentManager, songIds)
            } else
                CreatePlaylistDialogFragment.show(mainActivity.supportFragmentManager)
        }
    }

    /**
     * This function is meant to be called from [songCallbacks] when the [RowItemMenuAction] equals
     * [RowItemMenuAction.ACTION_PLAY].
     * It will play the selected music and update the SongQueue accordingly.
     *
     * @param itemPosition the position of the song in the list.
     * @param functionToGetPlayableSongs the function that returns an [ArrayList] of [PlayableSong] objects
     * to be set to the SongQueue.
     */
    protected fun onSongCallbacksActionPlay(itemPosition: Int,
                                            functionToGetPlayableSongs: (songSorting: SongSorting) -> ArrayList<PlayableSong>) {
        GlobalScope.launch(Dispatchers.IO) {
            val songs = functionToGetPlayableSongs(sharedPreferencesHelper.getSongSorting())
            roomDbViewModel.songQueue.setSongs(songs, itemPosition)
            MediaPlayerService.start(mainActivity)
        }
    }

    /**
     * This function is meant to be called from [songCallbacks] when the [RowItemMenuAction] equals
     * [RowItemMenuAction.ACTION_ADD_TO_QUEUE].
     * It will add the selected song to the queue.
     *
     * @param songId the ID of the song to add to the SongQueue.
     */
    protected fun onSongCallbacksActionAddToQueue(songId: Long) {
        GlobalScope.launch(Dispatchers.IO) {
            roomDbViewModel.songQueue.addSong(roomDbViewModel.songDao.getPlayableSongById(songId))
        }
    }

    /**
     * This function is meant to be called from [songCallbacks] when the [RowItemMenuAction] equals
     * [RowItemMenuAction.ACTION_ADD_TO_PLAYLIST].
     * This function will launch either [AddToPlaylistDialogFragment] or [CreatePlaylistDialogFragment]
     * according to whether there are available playlists in the database.
     *
     * @param songId the ID to pass to the [AddToPlaylistDialogFragment] in case it gets called.
     */
    protected fun onSongCallbacksActionAddToPlaylist(songId: Long) {
        GlobalScope.launch(Dispatchers.Main) {
            val playlistsCount = withContext(Dispatchers.IO) { roomDbViewModel.playlistDao.getCount() }
            if (playlistsCount > 0)
                AddToPlaylistDialogFragment.show(mainActivity.supportFragmentManager, songId)
            else
                CreatePlaylistDialogFragment.show(mainActivity.supportFragmentManager)
        }
    }

    /**
     * This function is meant to be called from [songCallbacks] when the [RowItemMenuAction] equals
     * [RowItemMenuAction.ACTION_SHARE].
     * It will launch an Intent to allow the user to pick an application with which to share
     * the selected song.
     *
     * @param songId the ID of the song that will be shared in an Intent.
     */
    protected fun onSongCallbacksActionShare(songId: Long) =
            IntentUtils.shareSingleSong(mainActivity) {
                roomDbViewModel.songDao.getSongDataById(songId)
            }

    /**
     * This function is meant to be called from [songCallbacks] when the [RowItemMenuAction] equals
     * [RowItemMenuAction.ACTION_SET_AS_RINGTONE].
     * It will launch either launch an Intent to request the user for permission to modify system
     * settings (required in order to set a ringtone) or it will launch an Intent that will
     * set the ringtone on the user's device.
     *
     * @param song the song that shall be the user's ringtone.
     */
    protected fun onSongCallbacksActionSetAsRingtone(song: DisplaySong) =
            if (!PermissionUtils.canModifySystemSettings(mainActivity))
                requestModifySystemSettingsPermission(song.id!!)
            else
                setSongAsRingtone(song)

    private fun requestModifySystemSettingsPermission(songId: Long) {
        mainActivity.intent.putExtra(Constants.EXTRA_RINGTONE_SONG_ID, songId)
        ToastUtils.showLong(mainActivity, R.string.request_modify_system_settings)
        IntentUtils.requestModifySystemSettingsPermission(mainActivity,
                Constants.REQUEST_CODE_MODIFY_SYSTEM_PERMISSIONS)
    }

    private fun setSongAsRingtone(song: DisplaySong) =
            RingtoneUtils.setRingtone(mainActivity, song) {
                mainActivity.showSnackBarShort(R.string.ringtone_success)
            }

    /**
     * This function is an extreme-case scenario, in which an unwanted value is passed to the [Fragment]'s
     * [songCallbacks]. This function is meant for debugging purposes as well as informing the user
     * of the error.
     *
     * @param item the RecyclerView item that the call came from.
     * @param itemPosition the position of the song in the RecyclerView.
     * @param menuAction the unwanted value that was sent to [songCallbacks].
     */
    protected fun onSongCallbacksInvalidAction(item: DisplaySong, itemPosition: Int, menuAction: RowItemMenuAction) {
        mainActivity.showSnackBarLong(R.string.error_invalid_menu_action)
        CrashUtils.logError("${javaClass.name}.songCallbacks.onRowMenuClick",
                "INVALID MENU ACTION: menuAction=$menuAction, item=$item, itemPosition=$itemPosition")
    }
}