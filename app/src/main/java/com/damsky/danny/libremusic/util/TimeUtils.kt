package com.damsky.danny.libremusic.util

import java.util.concurrent.TimeUnit

/**
 * A utility class for correct time formatting.
 *
 * @author Danny Damsky
 */
class TimeUtils private constructor() {
    companion object {
        private const val TIME_SEPARATOR = ':'

        // This class re-uses the same StringBuilder object, for performance reasons
        private val stringBuilder = StringBuilder(8) // 8 is enough for the longest time string.

        /**
         * Formatting used: (HH:)MM:SS
         *
         * @param durationInMillis the duration in milliseconds to be returned in a formatted string.
         * @return a formatted time string.
         */
        @JvmStatic
        fun getFormattedDuration(durationInMillis: Long): String {
            val hours = getHours(durationInMillis)
            val minutes = getMinutes(durationInMillis, hours)
            val seconds = getSeconds(durationInMillis, hours, minutes)
            appendHours(hours)
            appendMinutes(minutes)
            appendSeconds(seconds)
            val timeString = stringBuilder.toString()
            stringBuilder.clear()
            return timeString
        }

        /**
         * @param durationInMillis the duration in milliseconds to be returned in hours
         * @return the total hours in the given duration.
         */
        @JvmStatic
        private fun getHours(durationInMillis: Long) =
                TimeUnit.MILLISECONDS.toHours(durationInMillis)

        /**
         * @param durationInMillis the duration in milliseconds to be returned in minutes
         * @return the total minutes in the given duration. (minus the minutes in the total hours)
         */
        @JvmStatic
        private fun getMinutes(durationInMillis: Long, hours: Long) =
                TimeUnit.MILLISECONDS.toMinutes(durationInMillis) - TimeUnit.HOURS.toMinutes(hours)

        /**
         * @param durationInMillis the duration in milliseconds to be returned in seconds
         * @return the total minutes in the given duration. (minus the seconds in the total minutes
         * and the seconds in the total hours)
         */
        @JvmStatic
        private fun getSeconds(durationInMillis: Long, hours: Long, minutes: Long) =
                TimeUnit.MILLISECONDS.toSeconds(durationInMillis) -
                        TimeUnit.MINUTES.toSeconds(minutes) -
                        TimeUnit.HOURS.toSeconds(hours)

        /**
         * Appends the total hours to the stringBuilder object given that the hours are more than 0.
         *
         * @param hours the total hours.
         */
        @JvmStatic
        private fun appendHours(hours: Long) {
            if (hours in 1..9)
                stringBuilder.append(0).append(hours).append(TIME_SEPARATOR)
            else if (hours > 9)
                stringBuilder.append(hours).append(TIME_SEPARATOR)
        }

        /**
         * Appends the total minutes to the stringBuilder object.
         *
         * @param minutes the total minutes.
         */
        @JvmStatic
        private fun appendMinutes(minutes: Long) {
            if (minutes < 10)
                stringBuilder.append(0)
            stringBuilder.append(minutes).append(TIME_SEPARATOR)
        }

        /**
         * Appends the total seconds to the stringBuilder object.
         *
         * @param seconds the total seconds.
         */
        @JvmStatic
        private fun appendSeconds(seconds: Long) {
            if (seconds < 10)
                stringBuilder.append(0)
            stringBuilder.append(seconds)
        }
    }
}