package com.damsky.danny.libremusic.service.mediaplayer.enumeration

import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService

/**
 * This enum class is used to map the action constants in [MediaPlayerService] to enum values.
 *
 * @author Danny Damsky
 *
 * @param action the action constant from [MediaPlayerService].
 * @param index the index of the action in the array of the enum values.
 *
 * @property action The action constant from [MediaPlayerService].
 * @property index The index of the action in the array of the enum values.
 */
enum class PlaybackAction(val action: String, val index: Int) {
    ACTION_PLAY(MediaPlayerService.ACTION_PLAY, 0),
    ACTION_PAUSE(MediaPlayerService.ACTION_PAUSE, 1),
    ACTION_NEXT(MediaPlayerService.ACTION_NEXT, 2),
    ACTION_PREVIOUS(MediaPlayerService.ACTION_PREVIOUS, 3),
    ACTION_STOP(MediaPlayerService.ACTION_STOP, 4),
    ACTION_SEEK_TO(MediaPlayerService.ACTION_SEEK_TO, 5)
}