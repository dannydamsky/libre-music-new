package com.damsky.danny.libremusic.data.db.nonentity.rowitem

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.content.Context
import android.content.res.Resources
import android.support.v7.util.DiffUtil
import android.view.View
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.entity.Artist
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayArtist
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks
import com.damsky.danny.libremusic.util.PopupMenuBuilder
import com.damsky.danny.libremusic.util.RowItemMenuAction

/**
 * An [EntityRowItem] for the [Artist] entity.
 *
 * @author Danny Damsky
 *
 * @param resources are used to retrieve string resources for the [ArtistRowItem].
 * @param displayArtist the entity object for this row item.
 */
class ArtistRowItem private constructor(resources: Resources, private val displayArtist: DisplayArtist) : EntityRowItem<Artist> {

    companion object {
        /**
         * An interface implementation used to compare different [ArtistRowItem]s.
         */
        @JvmField
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<ArtistRowItem>() {
            override fun areItemsTheSame(p0: ArtistRowItem, p1: ArtistRowItem) = p0 === p1
            override fun areContentsTheSame(p0: ArtistRowItem, p1: ArtistRowItem) = p0.displayArtist == p1.displayArtist
        }

        /**
         * @param resources are used to retrieve string resources for the [ArtistRowItem].
         * @param dbLiveData the [LiveData] object to transform into a [LiveData] of a [List] of
         * [ArtistRowItem]s.
         * @return a [LiveData] of a [List] of [ArtistRowItem]s which are related to the given
         * [dbLiveData].
         */
        @JvmStatic
        fun getLiveData(resources: Resources, dbLiveData: LiveData<List<DisplayArtist>>) = Transformations.switchMap(dbLiveData) { input ->
            val artistModelList = input.map { ArtistRowItem(resources, it) }
            val liveData = MutableLiveData<List<ArtistRowItem>>()
            liveData.postValue(artistModelList)
            liveData
        }!!
    }

    private val secondaryText = getSecondaryText(resources)
    private val popupMenuBuilder: PopupMenuBuilder<Artist> = getPopupMenuBuilder()

    private fun getSecondaryText(resources: Resources): String {
        val firstString = resources.getQuantityString(R.plurals.albums, displayArtist.albumsCount, displayArtist.albumsCount)
        val secondString = resources.getQuantityString(R.plurals.songs, displayArtist.songsCount, displayArtist.songsCount)
        return "$firstString | $secondString"
    }

    private fun getPopupMenuBuilder() = PopupMenuBuilder<Artist>()
            .inflate(R.menu.menu_row_item_artist)
            .setItem(this.displayArtist.artist)
            .setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.playSongs -> RowItemMenuAction.ACTION_PLAY
                    R.id.addToQueue -> RowItemMenuAction.ACTION_ADD_TO_QUEUE
                    R.id.addSongsToPlaylist -> RowItemMenuAction.ACTION_ADD_TO_PLAYLIST
                    R.id.shareSongs -> RowItemMenuAction.ACTION_SHARE
                    R.id.viewSongs -> RowItemMenuAction.ACTION_VIEW_ALL_SONGS
                    else -> RowItemMenuAction.ACTION_PLAY
                }
            }

    override fun getCoverData() = displayArtist.artist.coverData

    override fun getPlaceHolderImage() = R.drawable.ic_artist_round_72dp

    override fun getMainText() = displayArtist.artist.name

    override fun getSecondaryText() = secondaryText

    override fun getTertiaryText() = ""

    override fun buildPopupMenu(context: Context, anchorView: View, itemPosition: Int, callbacks: MainActivityRecyclerViewAdapterCallbacks<Artist>) =
            popupMenuBuilder
                    .setItemPosition(itemPosition)
                    .setRecyclerViewOnClickListener(callbacks)
                    .build(context, anchorView)

    override fun getObject() = displayArtist.artist

    override fun toString() = "ArtistRowItem(displayArtist=$displayArtist)"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ArtistRowItem

        if (displayArtist != other.displayArtist) return false

        return true
    }

    override fun hashCode() = displayArtist.hashCode()

}