package com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.content.Context
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.damsky.danny.libremusic.R

import com.damsky.danny.libremusic.data.db.nonentity.rowitem.EntityRowItem

/**
 * This is the [ListAdapter] for the RecyclerView of the Fragments that extend AbstractMediaFragment
 * that are typically held on the MainActivity.
 *
 * @param diffCallback the callback that compares different items as a result of the [submitList]
 * method being called. Any class that extends [EntityRowItem] should have
 * a static [DiffUtil.ItemCallback] constant.
 * @param callbacks these callbacks will be called upon click events from the RecyclerView's items.
 *
 * @author Danny Damsky
 */
class MainActivityRecyclerViewAdapter<M, T : EntityRowItem<M>> constructor(
        diffCallback: DiffUtil.ItemCallback<T>,
        private val callbacks: MainActivityRecyclerViewAdapterCallbacks<M>
) : ListAdapter<T, MainActivityRecyclerViewViewHolder<M, T>>(diffCallback) {

    private lateinit var context: Context

    private var liveData: LiveData<List<T>>? = null
    private val observer = Observer<List<T>> { submitList(it) }

//    init {
//        setHasStableIds(true)
//    }
//
//    override fun getItemId(position: Int): Long {
//        return getItem(position).hashCode().toLong()
//    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MainActivityRecyclerViewViewHolder<M, T> {
        context = viewGroup.context
        val itemView: View = LayoutInflater.from(context)
                .inflate(R.layout.row_recyclerview_main, viewGroup, false)
        return MainActivityRecyclerViewViewHolder(itemView, callbacks)
    }

    override fun onBindViewHolder(viewHolder: MainActivityRecyclerViewViewHolder<M, T>, position: Int) {
        val item = getItem(position)
        viewHolder.bind(item, position)
    }

    /**
     * @param liveData the [LiveData] that will be observed and submitted to the adapter on every
     * change internally via the [submitList] method. Note: In order to initialise the observing
     * of this [LiveData] the [startObserving] method must be called.
     */
    fun setLiveData(liveData: LiveData<List<T>>?) {
        this.liveData = liveData
    }

    /**
     * Starts observing the [LiveData] that was set in [setLiveData].
     *
     * @param owner the [LifecycleOwner] owner to use for observing the [LiveData] that was set
     * in [setLiveData].
     */
    fun startObserving(owner: LifecycleOwner) = liveData!!.observe(owner, observer)

    /**
     * Stops observing the [LiveData] that was set in [setLiveData].
     */
    fun stopObserving() = liveData!!.removeObserver(observer)

    override fun onViewRecycled(holder: MainActivityRecyclerViewViewHolder<M, T>) {
        holder.unbind()
        super.onViewRecycled(holder)
    }
}
