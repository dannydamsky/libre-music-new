package com.damsky.danny.libremusic.data.db.nonentity.rowitem

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.content.Context
import android.content.res.Resources
import android.support.v7.util.DiffUtil
import android.view.View
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.entity.Playlist
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayPlaylist
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks
import com.damsky.danny.libremusic.util.PopupMenuBuilder
import com.damsky.danny.libremusic.util.RowItemMenuAction

/**
 * An [EntityRowItem] for the [Playlist] entity.
 *
 * @author Danny Damsky
 *
 * @param resources are used to retrieve string resources for the [PlaylistRowItem].
 * @param displayPlaylist the entity object for this row item.
 */
class PlaylistRowItem private constructor(resources: Resources, private val displayPlaylist: DisplayPlaylist) : EntityRowItem<Playlist> {

    companion object {
        /**
         * An interface implementation used to compare different [PlaylistRowItem]s.
         */
        @JvmField
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<PlaylistRowItem>() {
            override fun areItemsTheSame(p0: PlaylistRowItem, p1: PlaylistRowItem) = p0 === p1
            override fun areContentsTheSame(p0: PlaylistRowItem, p1: PlaylistRowItem) = p0.displayPlaylist == p1.displayPlaylist
        }

        /**
         * @param resources are used to retrieve string resources for the [PlaylistRowItem].
         * @param dbLiveData the [LiveData] object to transform into a [LiveData] of a [List] of
         * [PlaylistRowItem]s.
         * @return a [LiveData] of a [List] of [PlaylistRowItem]s which are related to the given
         * [dbLiveData].
         */
        @JvmStatic
        fun getLiveData(resources: Resources, dbLiveData: LiveData<List<DisplayPlaylist>>) = Transformations.switchMap(dbLiveData) { input ->
            val playlistModelList = input.map { PlaylistRowItem(resources, it) }
            val liveData = MutableLiveData<List<PlaylistRowItem>>()
            liveData.postValue(playlistModelList)
            liveData
        }
    }

    private val secondaryText = buildSecondaryText(resources)
    private val popupMenuBuilder = buildPopupMenuBuilder()

    private fun buildSecondaryText(resources: Resources): String {
        val playlistSongsCount = displayPlaylist.songsCount
        return resources.getQuantityString(R.plurals.songs, playlistSongsCount, playlistSongsCount)
    }

    private fun buildPopupMenuBuilder() = PopupMenuBuilder<Playlist>()
            .inflate(R.menu.menu_row_item_playlist)
            .setItem(this.displayPlaylist.playlist)
            .setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.playSongs -> RowItemMenuAction.ACTION_PLAY
                    R.id.addToQueue -> RowItemMenuAction.ACTION_ADD_TO_QUEUE
                    R.id.renamePlaylist -> RowItemMenuAction.ACTION_RENAME_PLAYLIST
                    R.id.deletePlaylist -> RowItemMenuAction.ACTION_REMOVE_PLAYLIST
                    R.id.shareSongs -> RowItemMenuAction.ACTION_SHARE
                    R.id.viewSongs -> RowItemMenuAction.ACTION_VIEW_ALL_SONGS
                    else -> RowItemMenuAction.ACTION_PLAY
                }
            }


    override fun getCoverData() = displayPlaylist.playlist.coverData

    override fun getPlaceHolderImage() = R.drawable.ic_playlist_round_72dp

    override fun getMainText() = displayPlaylist.playlist.name

    override fun getSecondaryText() = secondaryText

    override fun getTertiaryText() = ""

    override fun buildPopupMenu(context: Context, anchorView: View, itemPosition: Int, callbacks: MainActivityRecyclerViewAdapterCallbacks<Playlist>) =
            popupMenuBuilder
                    .setItemPosition(itemPosition)
                    .setRecyclerViewOnClickListener(callbacks)
                    .build(context, anchorView)

    override fun getObject() = displayPlaylist.playlist

    override fun toString() = "PlaylistRowItem(displayPlaylist=$displayPlaylist)"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PlaylistRowItem

        if (displayPlaylist != other.displayPlaylist) return false

        return true
    }

    override fun hashCode() = displayPlaylist.hashCode()
}