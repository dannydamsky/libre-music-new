package com.damsky.danny.libremusic.ui.activity.main.fragment.media

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplaySong
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.AbstractSongRowItem
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapter
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks
import com.damsky.danny.libremusic.util.RowItemMenuAction

/**
 * An implementation of [AbstractMediaFragment].
 * This implementation displays a list of all the available songs in the database.
 *
 * @author Danny Damsky
 */
class SongsFragment : AbstractMediaFragment() {

    override val songCallbacks = object : MainActivityRecyclerViewAdapterCallbacks<DisplaySong> {
        override fun onRowClick(item: DisplaySong, itemPosition: Int) =
                callOnSongCallbacksActionPlay(itemPosition)

        override fun onRowMenuClick(item: DisplaySong, itemPosition: Int, menuAction: RowItemMenuAction) = when (menuAction) {
            RowItemMenuAction.ACTION_PLAY -> callOnSongCallbacksActionPlay(itemPosition)
            RowItemMenuAction.ACTION_ADD_TO_QUEUE -> onSongCallbacksActionAddToQueue(item.id!!)
            RowItemMenuAction.ACTION_ADD_TO_PLAYLIST -> onSongCallbacksActionAddToPlaylist(item.id!!)
            RowItemMenuAction.ACTION_SHARE -> onSongCallbacksActionShare(item.id!!)
            RowItemMenuAction.ACTION_SET_AS_RINGTONE -> onSongCallbacksActionSetAsRingtone(item)
            RowItemMenuAction.ACTION_REMOVE_FROM_QUEUE,
            RowItemMenuAction.ACTION_VIEW_ALL_SONGS,
            RowItemMenuAction.ACTION_RENAME_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_FROM_PLAYLIST ->
                onSongCallbacksInvalidAction(item, itemPosition, menuAction)
        }
    }

    private fun callOnSongCallbacksActionPlay(itemPosition: Int) =
            onSongCallbacksActionPlay(itemPosition) { songSorting ->
                songSorting.getPlayableSongs(roomDbViewModel.songDao) as ArrayList<PlayableSong>
            }

    private val songsAdapterObserver = object : RecyclerView.AdapterDataObserver() {

    }

    private lateinit var songsAdapter: MainActivityRecyclerViewAdapter<DisplaySong, AbstractSongRowItem>

    override fun onViewStubInflated() {
        initSongsAdapter()
        initRecyclerViewProperties()
        setSongsAdapter()
    }

    private fun initSongsAdapter() {
        songsAdapter = MainActivityRecyclerViewAdapter(AbstractSongRowItem.DIFF_CALLBACK, songCallbacks)
        songsAdapter.setLiveData(roomDbViewModel.songRowItemsLiveData)
    }

    private fun initRecyclerViewProperties() {
        recyclerView.layoutManager = LinearLayoutManager(mainActivity)
        recyclerView.setHasFixedSize(true)
    }

    private fun setSongsAdapter() {
        songsAdapter.registerAdapterDataObserver(songsAdapterObserver)
        songsAdapter.startObserving(this)
        recyclerView.adapter = songsAdapter
    }

    override fun onBackPressed() = true

    override fun reset() = recyclerView.smoothScrollToPosition(0)
}