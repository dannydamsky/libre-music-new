package com.damsky.danny.libremusic.ui.activity.main.fragment.media

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.entity.Album
import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplaySong
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.AlbumRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.AbstractSongRowItem
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapter
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks
import com.damsky.danny.libremusic.util.CrashUtils
import com.damsky.danny.libremusic.util.IntentUtils
import com.damsky.danny.libremusic.util.RowItemMenuAction

/**
 * An implementation of [AbstractMediaFragment].
 * This implementation displays a list of [Album]s and their appropriate songs.
 *
 * @author Danny Damsky
 */
class AlbumsFragment : AbstractMediaFragment() {

    private val albumCallbacks = object : MainActivityRecyclerViewAdapterCallbacks<Album> {
        override fun onRowClick(item: Album, itemPosition: Int) =
                setSongsAdapterFromAlbums(item.name, itemPosition)

        override fun onRowMenuClick(item: Album, itemPosition: Int, menuAction: RowItemMenuAction) = when (menuAction) {
            RowItemMenuAction.ACTION_PLAY -> callOnSongCallbacksActionPlay(0, item.name)
            RowItemMenuAction.ACTION_ADD_TO_QUEUE -> callOnAlbumCallbacksAddToQueue(item.name)
            RowItemMenuAction.ACTION_ADD_TO_PLAYLIST -> callOnAlbumCallbacksAddToPlaylist(item.name)
            RowItemMenuAction.ACTION_VIEW_ALL_SONGS -> setSongsAdapterFromAlbums(item.name, itemPosition)
            RowItemMenuAction.ACTION_SHARE -> callOnAlbumCallbacksShare(item.name)
            RowItemMenuAction.ACTION_REMOVE_FROM_QUEUE,
            RowItemMenuAction.ACTION_SET_AS_RINGTONE,
            RowItemMenuAction.ACTION_RENAME_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_FROM_PLAYLIST -> onAlbumCallbacksInvalidAction(item, itemPosition, menuAction)
        }
    }

    private fun callOnAlbumCallbacksAddToQueue(albumName: String) =
            onOtherCallbacksAddToQueue { songSorting ->
                songSorting.getPlayableSongsForAlbum(roomDbViewModel.songDao, albumName)
                        as ArrayList<PlayableSong>
            }

    private fun callOnAlbumCallbacksAddToPlaylist(albumName: String) =
            onOtherCallbacksAddToPlaylist {
                roomDbViewModel.songDao.getSongIdsForAlbum(albumName)
            }

    private fun callOnAlbumCallbacksShare(albumName: String) =
            IntentUtils.shareMultipleSongs(mainActivity) {
                roomDbViewModel.songDao.getSongsDataForAlbum(albumName)
            }

    private fun onAlbumCallbacksInvalidAction(item: Album, itemPosition: Int, menuAction: RowItemMenuAction) {
        mainActivity.showSnackBarLong(R.string.error_invalid_menu_action)
        CrashUtils.logError("AlbumsFragment.albumCallbacks.onRowMenuClick",
                "INVALID MENU ACTION: menuAction=$menuAction, item=$item, itemPosition=$itemPosition")
    }

    override val songCallbacks = object : MainActivityRecyclerViewAdapterCallbacks<DisplaySong> {
        override fun onRowClick(item: DisplaySong, itemPosition: Int) =
                callOnSongCallbacksActionPlay(itemPosition, item.albumName)

        override fun onRowMenuClick(item: DisplaySong, itemPosition: Int, menuAction: RowItemMenuAction) = when (menuAction) {
            RowItemMenuAction.ACTION_PLAY -> callOnSongCallbacksActionPlay(itemPosition, item.albumName)
            RowItemMenuAction.ACTION_ADD_TO_QUEUE -> onSongCallbacksActionAddToQueue(item.id!!)
            RowItemMenuAction.ACTION_ADD_TO_PLAYLIST -> onSongCallbacksActionAddToPlaylist(item.id!!)
            RowItemMenuAction.ACTION_SHARE -> onSongCallbacksActionShare(item.id!!)
            RowItemMenuAction.ACTION_SET_AS_RINGTONE -> onSongCallbacksActionSetAsRingtone(item)
            RowItemMenuAction.ACTION_REMOVE_FROM_QUEUE,
            RowItemMenuAction.ACTION_VIEW_ALL_SONGS,
            RowItemMenuAction.ACTION_RENAME_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_FROM_PLAYLIST -> onSongCallbacksInvalidAction(item, itemPosition, menuAction)
        }
    }

    private fun callOnSongCallbacksActionPlay(itemPosition: Int, albumName: String) =
            onSongCallbacksActionPlay(itemPosition) { songSorting ->
                songSorting.getPlayableSongsForAlbum(roomDbViewModel.songDao, albumName) as ArrayList<PlayableSong>
            }

    private val albumsAdapterObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            super.onItemRangeInserted(positionStart, itemCount)
            if (itemCount != 0 && lastScrollPositionAlbums != -1) {
                recyclerView.scrollToPosition(lastScrollPositionAlbums)
                lastScrollPositionAlbums = -1
            }
        }
    }

    private val songsAdapterObserver = object : RecyclerView.AdapterDataObserver() {

    }

    private lateinit var albumsAdapter: MainActivityRecyclerViewAdapter<Album, AlbumRowItem>
    private lateinit var songsAdapter: MainActivityRecyclerViewAdapter<DisplaySong, AbstractSongRowItem>

    private var lastScrollPositionAlbums: Int = -1

    override fun onViewStubInflated() {
        initAdapters()
        initRecyclerViewProperties()
        setAlbumsAdapter()
    }

    private fun initAdapters() {
        initAlbumsAdapter()
        initSongsAdapter()
    }

    private fun initAlbumsAdapter() {
        albumsAdapter = MainActivityRecyclerViewAdapter(AlbumRowItem.DIFF_CALLBACK, albumCallbacks)
        albumsAdapter.setLiveData(roomDbViewModel.albumRowItemsLiveData)
    }

    private fun initSongsAdapter() {
        songsAdapter = MainActivityRecyclerViewAdapter(AbstractSongRowItem.DIFF_CALLBACK, songCallbacks)
    }

    private fun initRecyclerViewProperties() {
        recyclerView.layoutManager = LinearLayoutManager(mainActivity)
        recyclerView.setHasFixedSize(true)
    }

    private fun setAlbumsAdapter() {
        albumsAdapter.registerAdapterDataObserver(albumsAdapterObserver)
        albumsAdapter.startObserving(this)
        recyclerView.adapter = albumsAdapter
    }

    private fun setSongsAdapterFromAlbums(albumName: String, lastScrollPositionAlbums: Int) {
        albumsAdapter.unregisterAdapterDataObserver(albumsAdapterObserver)
        val liveData = roomDbViewModel.getSongRowItemsLiveDataForAlbum(albumName)
        songsAdapter.setLiveData(liveData)
        songsAdapter.registerAdapterDataObserver(songsAdapterObserver)
        songsAdapter.startObserving(this)
        recyclerView.adapter = songsAdapter
        albumsAdapter.stopObserving()
        albumsAdapter.submitList(null)
        this.lastScrollPositionAlbums = lastScrollPositionAlbums
    }

    override fun onBackPressed() = if (this.lastScrollPositionAlbums != -1) {
        setAlbumsAdapterFromSongs()
        false
    } else
        true

    private fun setAlbumsAdapterFromSongs() {
        songsAdapter.unregisterAdapterDataObserver(songsAdapterObserver)
        albumsAdapter.registerAdapterDataObserver(albumsAdapterObserver)
        albumsAdapter.startObserving(this)
        recyclerView.adapter = albumsAdapter
        songsAdapter.stopObserving()
        songsAdapter.submitList(null)
    }

    override fun reset() = when {
        lastScrollPositionAlbums != -1 -> {
            lastScrollPositionAlbums = -1
            setAlbumsAdapterFromSongs()
        }
        else -> recyclerView.smoothScrollToPosition(0)
    }
}