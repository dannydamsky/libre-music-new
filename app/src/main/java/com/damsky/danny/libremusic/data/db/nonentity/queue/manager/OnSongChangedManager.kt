package com.damsky.danny.libremusic.data.db.nonentity.queue.manager

import com.damsky.danny.libremusic.data.db.nonentity.queue.SongQueueProperties
import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnSongChangedListener

/**
 * A manager for the [OnSongChangedListener]s.
 *
 * @author Danny Damsky
 * @see BaseManager
 * @see OnSongChangedListener
 */
class OnSongChangedManager : BaseManager<OnSongChangedListener>() {

    /**
     * @see OnSongChangedListener.onSongChanged
     */
    fun notifyOnSongChanged(songQueueProperties: SongQueueProperties) = forEach {
        it.onSongChanged(
                songQueueProperties.getCurrentSong()!!,
                songQueueProperties.currentSongIsPlaying(),
                songQueueProperties.getCurrentSongIndex(),
                songQueueProperties.getSongsTotal(),
                songQueueProperties.getPlayerPositionMillis()
        )
    }
}