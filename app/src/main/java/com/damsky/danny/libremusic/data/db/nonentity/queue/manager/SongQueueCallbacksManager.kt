package com.damsky.danny.libremusic.data.db.nonentity.queue.manager

import com.damsky.danny.libremusic.data.db.nonentity.queue.SongQueueProperties
import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnPlaybackInfoChangedListener
import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnQueueChangedListener
import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnSongChangedListener
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.AbstractSongRowItem

/**
 * This class manages all of the callbacks for the SongQueue.
 * Therefore its properties consist of other managers.
 *
 * @author Danny Damsky
 *
 * @param onSongChangedManager the manager for the [OnSongChangedListener] callbacks.
 * @param onQueueChangedManager the manager for the [OnQueueChangedListener] callbacks.
 * @param onPlaybackInfoChangedManager the manager for the [OnPlaybackInfoChangedListener] callbacks.
 */
data class SongQueueCallbacksManager(private val onSongChangedManager: OnSongChangedManager,
                                     private val onQueueChangedManager: OnQueueChangedManager,
                                     private val onPlaybackInfoChangedManager: OnPlaybackInfoChangedManager) {
    constructor() : this(OnSongChangedManager(), OnQueueChangedManager(), OnPlaybackInfoChangedManager())

    /**
     * @param onSongChangedListener the listener to register and provide callbacks to.
     */
    fun registerOnSongChangedListener(onSongChangedListener: OnSongChangedListener) =
            onSongChangedManager.register(onSongChangedListener)

    /**
     * @param onSongChangedListener the listener to unregister and stop providing callbacks to.
     */
    fun unregisterOnSongChangedListener(onSongChangedListener: OnSongChangedListener) =
            onSongChangedManager.unregister(onSongChangedListener)

    /**
     * @param songQueueProperties the new properties to send as a callback to all registered [OnSongChangedListener]s.
     */
    fun notifyOnSongChanged(songQueueProperties: SongQueueProperties) =
            onSongChangedManager.notifyOnSongChanged(songQueueProperties)

    /**
     * @param onQueueChangedListener the listener to register and provide callbacks to.
     */
    fun registerOnQueueChangedListener(onQueueChangedListener: OnQueueChangedListener) =
            onQueueChangedManager.register(onQueueChangedListener)

    /**
     * @param onQueueChangedListener the listener to unregister and stop providing callbacks to.
     */
    fun unregisterOnQueueChangedListener(onQueueChangedListener: OnQueueChangedListener) =
            onQueueChangedManager.unregister(onQueueChangedListener)

    /**
     * @param queue the new queue to send as a callback to all registered [OnQueueChangedListener]s.
     */
    fun notifyOnQueueChanged(queue: List<AbstractSongRowItem>) =
            onQueueChangedManager.notifyOnQueueChanged(queue)

    /**
     * @param onPlaybackInfoChangedListener the listener to register and provide callbacks to.
     */
    fun registerOnPlaybackInfoChangedListener(onPlaybackInfoChangedListener: OnPlaybackInfoChangedListener) =
            onPlaybackInfoChangedManager.register(onPlaybackInfoChangedListener)

    /**
     * @param onPlaybackInfoChangedListener the listener to unregister and stop providing callbacks to.
     */
    fun unregisterOnPlaybackInfoChangedListener(onPlaybackInfoChangedListener: OnPlaybackInfoChangedListener) =
            onPlaybackInfoChangedManager.unregister(onPlaybackInfoChangedListener)

    /**
     * Notifies all registered [OnPlaybackInfoChangedListener]s that the playback has stopped.
     */
    fun notifyOnPlaybackStopped() = onPlaybackInfoChangedManager.notifyOnPlaybackStopped()

    /**
     * Notifies all registered [OnPlaybackInfoChangedListener]s that the playback has started.
     */
    fun notifyOnPlaybackStarted() = onPlaybackInfoChangedManager.notifyOnPlaybackStarted()

    /**
     * Notifies all registered [OnPlaybackInfoChangedListener]s that the media player's seek
     * position has changed.
     *
     * @param playerPositionMillis the new seek position of the media player.
     */
    fun notifyOnPlayerPositionChanged(playerPositionMillis: Int) =
            onPlaybackInfoChangedManager.notifyOnPlayerPositionChanged(playerPositionMillis)

    /**
     * Notifies all registered [OnPlaybackInfoChangedListener]s that the queue has changed.
     *
     * @param currentSongIndex the index of the currently selected song.
     * @param maxIndex the total songs amount in the queue.
     */
    fun notifyOnQueueResized(currentSongIndex: Int, maxIndex: Int) =
            onPlaybackInfoChangedManager.notifyOnQueueResized(currentSongIndex, maxIndex)
}