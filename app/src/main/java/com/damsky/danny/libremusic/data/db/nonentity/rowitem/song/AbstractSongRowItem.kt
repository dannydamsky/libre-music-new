package com.damsky.danny.libremusic.data.db.nonentity.rowitem.song

import android.content.Context
import android.support.v7.util.DiffUtil
import android.view.View
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplaySong
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.EntityRowItem
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks
import com.damsky.danny.libremusic.util.PopupMenuBuilder
import com.damsky.danny.libremusic.util.TimeUtils

/**
 * An abstract class that can be implemented in order to represent an [EntityRowItem] for the
 * Song entity.
 *
 * @author Danny Damsky
 *
 * @param song the entity object for this row item.
 */
abstract class AbstractSongRowItem(protected val song: DisplaySong) : EntityRowItem<DisplaySong> {

    companion object {
        /**
         * An interface implementation used to compare different [AbstractSongRowItem]s.
         */
        @JvmField
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<AbstractSongRowItem>() {
            override fun areItemsTheSame(p0: AbstractSongRowItem, p1: AbstractSongRowItem) = p0 === p1
            override fun areContentsTheSame(p0: AbstractSongRowItem, p1: AbstractSongRowItem) = p0.song == p1.song
        }
    }

    private val tertiaryText = buildTertiaryText()
    private val popupMenuBuilder = this.buildPopupMenuBuilder()

    /**
     * @return a [PopupMenuBuilder] that has been prepared with the appropriate assets.
     */
    abstract fun buildPopupMenuBuilder(): PopupMenuBuilder<DisplaySong>

    private fun buildTertiaryText() = TimeUtils.getFormattedDuration(song.durationInMillis.toLong())

    final override fun getCoverData() = song.coverData

    final override fun getPlaceHolderImage() = R.drawable.ic_song_round_72dp

    final override fun getMainText() = song.name

    final override fun getSecondaryText() = "${this.song.artistName} - ${this.song.albumName}"

    final override fun getTertiaryText() = tertiaryText

    final override fun buildPopupMenu(context: Context, anchorView: View, itemPosition: Int, callbacks: MainActivityRecyclerViewAdapterCallbacks<DisplaySong>) =
            popupMenuBuilder
                    .setItemPosition(itemPosition)
                    .setRecyclerViewOnClickListener(callbacks)
                    .build(context, anchorView)

    final override fun getObject() = song

    final override fun toString() = "${javaClass.simpleName}(song=$song)"

    final override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AbstractSongRowItem

        if (song != other.song) return false

        return true
    }

    final override fun hashCode() = song.hashCode()
}