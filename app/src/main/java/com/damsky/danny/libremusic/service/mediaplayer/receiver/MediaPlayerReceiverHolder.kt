package com.damsky.danny.libremusic.service.mediaplayer.receiver

import android.content.Context

/**
 * This class holds all BroadcastReceivers that are associated with the MediaPlayerService class,
 * it contains two functions for registering and unregistering all of them: [registerReceivers] and
 * [unregisterReceivers].
 *
 * @author Danny Damsky
 */
class MediaPlayerReceiverHolder {

    private val becomingNoisyReceiver = BecomingNoisyReceiver()

    /**
     * Registers all required receivers for the MediaPlayerService class.
     *
     * @param context the context of the MediaPlayerService.
     */
    fun registerReceivers(context: Context) =
            BecomingNoisyReceiver.register(context, becomingNoisyReceiver)

    /**
     * Unregisters all required receivers for the MediaPlayerService class.
     *
     * @param context the context of the MediaPlayerService.
     */
    fun unregisterReceivers(context: Context) = context.unregisterReceiver(becomingNoisyReceiver)

    override fun toString() =
            "MediaPlayerReceiverHolder(becomingNoisyReceiver=$becomingNoisyReceiver)"
}