package com.damsky.danny.libremusic.data.db.nonentity

import android.arch.persistence.room.ColumnInfo
import com.damsky.danny.libremusic.data.db.RoomDbConstants

/**
 * This is a more compact class of the song entity, representing only the required columns from
 * the song entity for setting a song as a ringtone on a device.
 *
 * @author Danny Damsky
 *
 * @param songData the song file path.
 * @param name the name of the song.
 * @param durationInMillis the total duration of the song.
 */
data class RingtoneSong(
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_DATA)
        val songData: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_NAME)
        val name: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_DURATION_IN_MILLIS)
        val durationInMillis: Int
)