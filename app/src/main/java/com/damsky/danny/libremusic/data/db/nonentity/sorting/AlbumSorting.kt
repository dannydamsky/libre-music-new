package com.damsky.danny.libremusic.data.db.nonentity.sorting

import android.arch.lifecycle.LiveData
import com.damsky.danny.libremusic.data.db.dao.AlbumDao
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayAlbum
import com.damsky.danny.libremusic.util.CrashUtils

/**
 * The values of this enum class represent the available sorting types for lists representing
 * the album entity.
 *
 * @author Danny Damsky
 */
enum class AlbumSorting {
    NONE, NAME_ASC, NAME_DESC, YEAR_ASC, YEAR_DESC, ARTIST_NAME_ASC, ARTIST_NAME_DESC;

    /**
     * @param albumDao the DAO (Data Access Object) for working with the albums table.
     *
     * @return the appropriate [LiveData] object of a [List] of [DisplayAlbum] according to the
     * sorting enum value.
     */
    fun getDisplayAlbumsLiveData(albumDao: AlbumDao) = when (this) {
        NONE -> albumDao.getAllDisplayLive()
        NAME_ASC -> albumDao.getAllDisplayOrderedByNameAscLive()
        NAME_DESC -> albumDao.getAllDisplayOrderedByNameDescLive()
        YEAR_ASC -> albumDao.getAllDisplayOrderedByYearAscLive()
        YEAR_DESC -> albumDao.getAllDisplayOrderedByYearDescLive()
        ARTIST_NAME_ASC -> albumDao.getAllDisplayOrderedByArtistNameAscLive()
        ARTIST_NAME_DESC -> albumDao.getAllDisplayOrderedByArtistNameDescLive()
    }

    /**
     * @param albumDao the DAO (Data Access Object) for working with the albums table.
     * @param artistName the name of the artist of the albums to look for.
     *
     * @return the appropriate [LiveData] object of a [List] of [DisplayAlbum] for the given
     * [artistName] according to the sorting enum value.
     */
    fun getDisplayAlbumsForArtistLiveData(albumDao: AlbumDao, artistName: String) = when (this) {
        NONE -> albumDao.getAllDisplayForArtistLive(artistName)
        NAME_ASC -> albumDao.getAllDisplayForArtistOrderedByNameAscLive(artistName)
        NAME_DESC -> albumDao.getAllDisplayForArtistOrderedByNameDescLive(artistName)
        YEAR_ASC -> albumDao.getAllDisplayForArtistOrderedByYearAscLive(artistName)
        YEAR_DESC -> albumDao.getAllDisplayForArtistOrderedByYearDescLive(artistName)
        ARTIST_NAME_ASC, ARTIST_NAME_DESC -> {
            CrashUtils.logError("AlbumSorting.getDisplayAlbumsForArtistLiveData",
                    "artistName=$artistName, this=$this")
            albumDao.getAllDisplayForArtistLive(artistName)
        }
    }
}