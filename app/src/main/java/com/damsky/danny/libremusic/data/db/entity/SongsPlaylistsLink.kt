package com.damsky.danny.libremusic.data.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import com.damsky.danny.libremusic.data.db.RoomDbConstants

/**
 * This class represents the SongsPlaylistsLink table in the database, a table made to implement a
 * M-N relationship between songs and playlists.
 *
 * Relationships:
 * Song 1-N SongsPlaylistsLinks
 * Playlist 1-N SongsPlaylistsLinks
 *
 * @author Danny Damsky
 *
 * @param songId the ID of the song that is connected to a playlist.
 * @param playlistId the ID of the playlist that is connected to a song.
 */
@Entity(tableName = RoomDbConstants.TABLE_NAME_SONGS_PLAYLISTS_LINKS,
        primaryKeys = [RoomDbConstants.COLUMN_SONGS_PLAYLISTS_LINK_SONG_ID,
            RoomDbConstants.COLUMN_SONGS_PLAYLISTS_LINK_PLAYLIST_ID],
        foreignKeys = [ForeignKey(entity = Song::class,
                parentColumns = [RoomDbConstants.COLUMN_SONG_ID],
                childColumns = [RoomDbConstants.COLUMN_SONGS_PLAYLISTS_LINK_SONG_ID],
                onDelete = ForeignKey.CASCADE),
            ForeignKey(entity = Playlist::class,
                    parentColumns = [RoomDbConstants.COLUMN_PLAYLIST_ID],
                    childColumns = [RoomDbConstants.COLUMN_SONGS_PLAYLISTS_LINK_PLAYLIST_ID],
                    onDelete = ForeignKey.CASCADE)],
        indices = [Index(value = [RoomDbConstants.COLUMN_SONGS_PLAYLISTS_LINK_SONG_ID,
            RoomDbConstants.COLUMN_SONGS_PLAYLISTS_LINK_PLAYLIST_ID], unique = true)])
data class SongsPlaylistsLink(
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONGS_PLAYLISTS_LINK_SONG_ID)
        val songId: Long,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONGS_PLAYLISTS_LINK_PLAYLIST_ID)
        val playlistId: Long) {

    /**
     * A builder class for teh [SongsPlaylistsLink] entity.
     *
     * @author Danny Damsky
     */
    class Builder {
        private var songId = 0L
        private var playlistId = 0L

        /**
         * @param songId the ID of the song that is connected to the playlist.
         */
        fun setSongId(songId: Long) = apply {
            this.songId = songId
        }

        /**
         * @param playlistId the ID of the playlist that is connected to a song.
         */
        fun setPlaylistId(playlistId: Long) = apply {
            this.playlistId = playlistId
        }

        /**
         * @return a new [SongsPlaylistsLink] object with the properties set in this class.
         */
        fun build() = SongsPlaylistsLink(songId, playlistId)

        override fun toString() = "Builder(songId=$songId, playlistId=$playlistId)"
    }
}
