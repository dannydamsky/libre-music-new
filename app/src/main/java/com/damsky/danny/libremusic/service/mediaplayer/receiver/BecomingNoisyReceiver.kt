package com.damsky.danny.libremusic.service.mediaplayer.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.AudioManager
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService

/**
 * This receiver listens for [AudioManager.ACTION_AUDIO_BECOMING_NOISY] and calls [MediaPlayerService.pause]
 * if that action is sent in a broadcast.
 *
 * @author Danny Damsky
 */
class BecomingNoisyReceiver : BroadcastReceiver() {

    companion object {

        /**
         * Registers the given [becomingNoisyReceiver] as a [BroadcastReceiver].
         *
         * @param context the context of the application package that made this call.
         * @param becomingNoisyReceiver the receiver's instance that shall be implemented
         */
        @JvmStatic
        fun register(context: Context, becomingNoisyReceiver: BecomingNoisyReceiver) {
            context.registerReceiver(becomingNoisyReceiver, IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY))
        }
    }

    override fun onReceive(context: Context, intent: Intent?) = MediaPlayerService.pause(context)
}