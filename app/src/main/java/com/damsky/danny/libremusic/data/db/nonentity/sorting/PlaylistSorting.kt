package com.damsky.danny.libremusic.data.db.nonentity.sorting

import android.arch.lifecycle.LiveData
import com.damsky.danny.libremusic.data.db.dao.PlaylistDao
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayPlaylist

/**
 * The values of this enum class represent the available sorting types for lists representing
 * the playlist entity.
 *
 * @author Danny Damsky
 */
enum class PlaylistSorting {
    NONE, NAME_ASC, NAME_DESC;

    /**
     * @param playlistDao the DAO (Data Access Object) for working with the playlists table.
     *
     * @return the appropriate [LiveData] object of a [List] of [DisplayPlaylist] according to the
     * sorting enum value.
     */
    fun getDisplayPlaylistsLiveData(playlistDao: PlaylistDao) = when (this) {
        NONE -> playlistDao.getAllDisplayLive()
        NAME_ASC -> playlistDao.getAllDisplayOrderedByNameAscLive()
        NAME_DESC -> playlistDao.getAllDisplayOrderedByNameDescLive()
    }
}