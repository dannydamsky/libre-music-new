package com.damsky.danny.libremusic.util

import android.content.Context
import android.support.annotation.MenuRes
import android.support.v7.widget.PopupMenu
import android.view.MenuItem
import android.view.View
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks

/**
 * A builder class for [PopupMenu] that is used for certain menu items.
 * The PopupMenu is meant to be used with a RecyclerView.
 *
 * @param T a database entity
 * @author Danny Damsky
 * @see PopupMenu
 */
class PopupMenuBuilder<T> {
    @MenuRes
    private var menuId: Int = 0
    private lateinit var onMenuItemClickListener: PopupMenu.OnMenuItemClickListener
    private lateinit var recyclerViewOnClickListener: MainActivityRecyclerViewAdapterCallbacks<T>
    private var item: T? = null
    private var itemPosition: Int = 0

    /**
     * @param menuId the ID of the menu resource to set to the PopupMenu
     */
    fun inflate(@MenuRes menuId: Int) = apply {
        this.menuId = menuId
    }

    /**
     * @param listener a callback function that will run when the menu item has been clicked.
     */
    fun setOnMenuItemClickListener(listener: (menuItem: MenuItem) -> RowItemMenuAction) = apply {
        onMenuItemClickListener = PopupMenu.OnMenuItemClickListener { menuItem ->
            val action = listener(menuItem)
            recyclerViewOnClickListener.onRowMenuClick(item!!, itemPosition, action)
            true
        }
    }

    /**
     * This PopupMenu is meant to be used with a RecyclerView, therefore it needs to call callback
     * functions on the RecyclerView's listener. This function passes that listener to this builder.
     * @param recyclerViewOnClickListener the RecyclerView's callback interface
     */
    fun setRecyclerViewOnClickListener(recyclerViewOnClickListener: MainActivityRecyclerViewAdapterCallbacks<T>) = apply {
        this.recyclerViewOnClickListener = recyclerViewOnClickListener
    }

    /**
     * @param item the item that will be passed in the [PopupMenu.OnMenuItemClickListener] event to
     * the RecyclerView's listener.
     */
    fun setItem(item: T) = apply {
        this.item = item
    }

    /**
     * @param itemPosition the position of the item in the RecyclerView's list.
     */
    fun setItemPosition(itemPosition: Int) = apply {
        this.itemPosition = itemPosition
    }

    /**
     * @param context the context from where the [PopupMenu] will be used.
     * @param anchorView the view to which the [PopupMenu] will be anchored.
     * @return an inflated [PopupMenu] object with an implemented [PopupMenu.OnMenuItemClickListener].
     */
    fun build(context: Context, anchorView: View): PopupMenu {
        val popupMenu = PopupMenu(context, anchorView)
        popupMenu.inflate(menuId)
        popupMenu.setOnMenuItemClickListener(onMenuItemClickListener)
        return popupMenu
    }

    override fun toString() =
            "PopupMenuBuilder(menuId=$menuId, onMenuItemClickListener=$onMenuItemClickListener, recyclerViewOnClickListener=$recyclerViewOnClickListener, item=$item, itemPosition=$itemPosition)"
}