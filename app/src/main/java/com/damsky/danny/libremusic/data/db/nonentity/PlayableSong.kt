package com.damsky.danny.libremusic.data.db.nonentity

import android.arch.persistence.room.ColumnInfo
import com.damsky.danny.libremusic.data.db.RoomDbConstants

/**
 * This is a more compact class of the song entity, representing only the required columns
 * for playback.
 *
 * @author Danny Damsky
 *
 * @param id the ID of the song in the database.
 * @param songData the song file path.
 * @param name the name of the song.
 * @param albumName the name of the song's album.
 * @param artistName the name of the song's artist.
 * @param startTimeInMillis the start time of the song in milliseconds (this is usually 0,
 * but it won't be 0 for songs that are located in a cue sheet, except for the first song).
 * @param endTimeInMillis the end time of the song in milliseconds.
 * @param durationInMillis the duration of the song (=[endTimeInMillis] - [startTimeInMillis])
 * @param coverData the path of the song's cover art image.
 */
data class PlayableSong(
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_ID)
        val id: Long?,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_DATA)
        val songData: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_NAME)
        val name: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_ALBUM_NAME)
        val albumName: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_ARTIST_NAME)
        val artistName: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_START_TIME_IN_MILLIS)
        val startTimeInMillis: Int,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_END_TIME_IN_MILLIS)
        val endTimeInMillis: Int,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_DURATION_IN_MILLIS)
        val durationInMillis: Int,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_COVER_DATA)
        val coverData: String?)