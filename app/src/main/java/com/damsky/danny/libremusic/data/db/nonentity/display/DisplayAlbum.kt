package com.damsky.danny.libremusic.data.db.nonentity.display

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import com.damsky.danny.libremusic.data.db.RoomDbConstants
import com.damsky.danny.libremusic.data.db.entity.Album

/**
 * A class representing the [Album] entity alongside the amount of songs in it.
 *
 * @author Danny Damsky
 *
 * @param album an object representing a row in the albums table.
 * @param songsCount the amount of songs that are available to the [album].
 */
data class DisplayAlbum(
        @Embedded
        val album: Album,
        @ColumnInfo(name = RoomDbConstants.COUNT_SONGS)
        val songsCount: Int
)