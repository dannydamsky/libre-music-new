package com.damsky.danny.libremusic.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.provider.Settings
import android.support.v4.content.ContextCompat

/**
 * A utility class meant to simplify working with Android Permissions.
 *
 * @author Danny Damsky
 */
class PermissionUtils private constructor() {

    companion object {
        /**
         * This constant contains all the dangerous permissions that the application will
         * request during runtime.
         */
        @JvmField
        val APP_PERMISSIONS = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_PHONE_STATE)

        /**
         * @param activity the activity where the call is coming from.
         * @return true if all dangerous permissions have been granted, false otherwise.
         */
        @JvmStatic
        fun allGranted(activity: Activity) =
                isGranted(ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                        && isGranted(ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE))

        /**
         * @param grantResults the results of a permission request sequence, should contain an array
         * the same length as [APP_PERMISSIONS].
         * @return true if all dangerous permissions have been granted, false otherwise.
         */
        @JvmStatic
        fun allGranted(grantResults: IntArray) =
                isGranted(grantResults[0]) && isGranted(grantResults[1])

        /**
         * @param grantResult a single result of a permission request.
         * @return true if the permission is granted, false otherwise.
         */
        @JvmStatic
        private fun isGranted(grantResult: Int) = grantResult == PackageManager.PERMISSION_GRANTED

        /**
         * @param context the context where the call is coming from,
         * the function uses the applicationContext property.
         * @return true if the application has permission to modify system settings, false otherwise.
         */
        @JvmStatic
        fun canModifySystemSettings(context: Context) =
                Settings.System.canWrite(context.applicationContext)
    }

}