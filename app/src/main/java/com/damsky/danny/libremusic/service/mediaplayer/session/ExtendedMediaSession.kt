package com.damsky.danny.libremusic.service.mediaplayer.session

import android.support.v4.media.MediaSessionManager
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import com.damsky.danny.libremusic.data.db.nonentity.queue.SongQueue
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService
import com.damsky.danny.libremusic.service.mediaplayer.enumeration.PlaybackStatus
import com.damsky.danny.libremusic.service.mediaplayer.player.ExtendedMediaPlayer
import com.damsky.danny.libremusic.util.notification.MediaNotification
import com.damsky.danny.libremusic.util.notification.NotificationUtils

/**
 * This class extends the functionality of the [MediaSessionCompat] class as well as implements it.
 *
 * This class is used for obtaining the [MediaSessionCompat.Token] as well as calling function on
 * the class' inner-property [MediaControllerCompat.TransportControls].
 *
 * The class implements [MediaSessionCompat.Callback] and therefore reacts to any accessible
 * function called on its [MediaControllerCompat.TransportControls].
 *
 * @author Danny Damsky
 */
class ExtendedMediaSession(private val mediaPlayerService: MediaPlayerService,
                           private val notificationUtils: NotificationUtils,
                           private val songQueue: SongQueue) : MediaSessionCompat.Callback() {

    private val mediaSession = MediaSessionCompat(mediaPlayerService, "Audio Player")
    private val mediaSessionManager = MediaSessionManager.getSessionManager(mediaPlayerService)
    private val transportControls: MediaControllerCompat.TransportControls
    private lateinit var mediaPlayer: ExtendedMediaPlayer

    init {
        mediaSession.isActive = true
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS)
        mediaSession.setCallback(this)
        transportControls = mediaSession.controller.transportControls
    }

    /**
     * @param mediaPlayer the [ExtendedMediaPlayer] that will be paired with this class.
     */
    fun init(mediaPlayer: ExtendedMediaPlayer) {
        this.mediaPlayer = mediaPlayer
    }

    override fun onPlay() {
        super.onPlay()
        mediaPlayer.resumeMedia()
        val mediaNotification = buildMediaNotification(PlaybackStatus.PLAYING)
        notificationUtils.sendMediaNotification(mediaNotification)
    }

    override fun onPause() {
        super.onPause()
        mediaPlayer.pauseMedia()
        val mediaNotification = buildMediaNotification(PlaybackStatus.PAUSED)
        notificationUtils.sendMediaNotification(mediaNotification)
    }

    override fun onSkipToNext() {
        super.onSkipToNext()
        mediaPlayer.skipToNext()
        val mediaNotification = buildMediaNotification(PlaybackStatus.PLAYING)
        notificationUtils.sendMediaNotification(mediaNotification)
    }

    override fun onSkipToPrevious() {
        super.onSkipToPrevious()
        mediaPlayer.skipToPrevious()
        val mediaNotification = buildMediaNotification(PlaybackStatus.PLAYING)
        notificationUtils.sendMediaNotification(mediaNotification)
    }

    private fun buildMediaNotification(playbackStatus: PlaybackStatus) =
            MediaNotification.Builder()
                    .setContext(mediaPlayerService)
                    .setSong(songQueue.getCurrentSong())
                    .setSessionToken(mediaSession.sessionToken)
                    .setPlaybackStatus(playbackStatus)
                    .build()

    override fun onStop() {
        super.onStop()
        notificationUtils.removeMediaNotification()
        mediaPlayerService.stopSelf()
    }

    override fun onSeekTo(pos: Long) {
        super.onSeekTo(pos)
        mediaPlayer.seekFrom(pos.toInt())
    }

    /**
     * @see [MediaControllerCompat.TransportControls.play]
     */
    fun play() = transportControls.play()

    /**
     * @see [MediaControllerCompat.TransportControls.pause]
     */
    fun pause() = transportControls.pause()

    /**
     * @see [MediaControllerCompat.TransportControls.skipToNext]
     */
    fun skipToNext() = transportControls.skipToNext()

    /**
     * @see [MediaControllerCompat.TransportControls.skipToPrevious]
     */
    fun skipToPrevious() = transportControls.skipToPrevious()

    /**
     * @see [MediaControllerCompat.TransportControls.stop]
     */
    fun stop() = transportControls.stop()

    /**
     * @see [MediaControllerCompat.TransportControls.seekTo]
     */
    fun seekTo(newPlayerTimeMillis: Long) = transportControls.seekTo(newPlayerTimeMillis)

    /**
     * @see [MediaSessionCompat.getSessionToken]
     */
    fun getSessionToken() = mediaSession.sessionToken!!

    override fun toString() =
            "ExtendedMediaSession(mediaPlayerService=$mediaPlayerService, mediaSession=$mediaSession, mediaSessionManager=$mediaSessionManager, transportControls=$transportControls, songQueue=$songQueue, mediaPlayer=$mediaPlayer, notificationUtils=$notificationUtils)"
}