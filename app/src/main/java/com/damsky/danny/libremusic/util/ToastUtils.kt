package com.damsky.danny.libremusic.util

import android.content.Context
import android.support.annotation.StringRes
import android.widget.Toast

/**
 * A utility class for system-wide access to Toast.makeText functions.
 *
 * @author Danny Damsky
 * @see Toast
 */
class ToastUtils private constructor() {
    companion object {
        /**
         * Automatically shows the Toast with the [Toast.LENGTH_LONG] parameter used.
         *
         * @param context the context to use, this function automatically uses the applicationContext
         * property from the given context.
         * @param textId the ID of the resource String to be displayed in a toast.
         */
        @JvmStatic
        fun showLong(context: Context, @StringRes textId: Int) {
            Toast.makeText(context.applicationContext, textId, Toast.LENGTH_LONG).show()
        }

        /**
         * Automatically shows the Toast with the [Toast.LENGTH_LONG] parameter used.
         *
         * @param context the context to use, this function automatically uses the applicationContext
         * property from the given context.
         * @param text the String to be displayed in a toast.
         */
        @JvmStatic
        fun showLong(context: Context, text: String) {
            Toast.makeText(context.applicationContext, text, Toast.LENGTH_LONG).show()
        }

        /**
         * Automatically shows the Toast with the [Toast.LENGTH_SHORT] parameter used.
         *
         * @param context the context to use, this function automatically uses the applicationContext
         * property from the given context.
         * @param textId the ID of the resource String to be displayed in a toast.
         */
        @JvmStatic
        fun showShort(context: Context, @StringRes textId: Int) {
            Toast.makeText(context.applicationContext, textId, Toast.LENGTH_SHORT).show()
        }

        /**
         * Automatically shows the Toast with the [Toast.LENGTH_SHORT] parameter used.
         *
         * @param context the context to use, this function automatically uses the applicationContext
         * property from the given context.
         * @param text the String to be displayed in a toast.
         */
        @JvmStatic
        fun showShort(context: Context, text: String) {
            Toast.makeText(context.applicationContext, text, Toast.LENGTH_SHORT).show()
        }
    }
}