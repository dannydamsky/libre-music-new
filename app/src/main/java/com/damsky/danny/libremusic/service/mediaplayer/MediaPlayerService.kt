package com.damsky.danny.libremusic.service.mediaplayer

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.IBinder
import com.damsky.danny.libremusic.data.db.RoomDbRepository
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService.Companion.next
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService.Companion.pause
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService.Companion.play
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService.Companion.previous
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService.Companion.start
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService.Companion.stop
import com.damsky.danny.libremusic.service.mediaplayer.enumeration.PlaybackStatus
import com.damsky.danny.libremusic.service.mediaplayer.player.ExtendedMediaPlayer
import com.damsky.danny.libremusic.service.mediaplayer.receiver.MediaPlayerReceiverHolder
import com.damsky.danny.libremusic.service.mediaplayer.session.ExtendedMediaSession
import com.damsky.danny.libremusic.service.mediaplayer.telephony.MediaPlayerPhoneStateListener
import com.damsky.danny.libremusic.service.mediaplayer.telephony.MediaPlayerTelephony
import com.damsky.danny.libremusic.util.notification.MediaNotification
import com.damsky.danny.libremusic.util.notification.NotificationUtils

/**
 * This [Service] is in charge of the application's media playback functionality.
 *
 * To interact with it there the available static functions: [start], [play], [pause], [next],
 * [previous] and [stop].
 *
 * @author Danny Damsky
 */
class MediaPlayerService : Service(), MediaPlayerPhoneStateListener.Callbacks {

    companion object {
        const val ACTION_START: String =
                "com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService.ACTION_START"
        const val ACTION_PLAY: String =
                "com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService.ACTION_PLAY"
        const val ACTION_PAUSE: String =
                "com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService.ACTION_PAUSE"
        const val ACTION_NEXT: String =
                "com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService.ACTION_NEXT"
        const val ACTION_PREVIOUS: String =
                "com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService.ACTION_PREVIOUS"
        const val ACTION_STOP: String =
                "com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService.ACTION_STOP"
        const val ACTION_SEEK_TO: String =
                "com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService.ACTION_SEEK_TO"

        private const val EXTRA_SEEK_POSITION: String = "extra_seek_position"

        /**
         * Starts the [MediaPlayerService] if it hasn't been started yet, and proceeds to play
         * the song that is currently chosen in the queue.
         *
         * @param context a context of the application package used to call this function.
         */
        @JvmStatic
        fun start(context: Context) = startService(context, ACTION_START)

        /**
         * Plays the currently chosen song if it was paused previously.
         *
         * @param context a context of the application package used to call this function.
         */
        @JvmStatic
        fun play(context: Context) = startService(context, ACTION_PLAY)

        /**
         * Pauses the currently chosen song if it was playing previously.
         *
         * @param context a context of the application package used to call this function.
         */
        @JvmStatic
        fun pause(context: Context) = startService(context, ACTION_PAUSE)

        /**
         * Skips to the next available song in the queue and starts playing it.
         *
         * @param context a context of the application package used to call this function.
         */
        @JvmStatic
        fun next(context: Context) = startService(context, ACTION_NEXT)

        /**
         * Skips to the previous song in the queue and starts playing it.
         *
         * @param context a context of the application package used to call this function.
         */
        @JvmStatic
        fun previous(context: Context) = startService(context, ACTION_PREVIOUS)

        /**
         * Stops and releases all objects from the [MediaPlayerService].
         *
         * @param context a context of the application package used to call this function.
         */
        @JvmStatic
        fun stop(context: Context) = startService(context, ACTION_STOP)

        @JvmStatic
        private fun startService(context: Context, action: String) {
            val intent = Intent(context, MediaPlayerService::class.java)
            intent.action = action
            context.startService(intent)
        }

        @JvmStatic
        fun seekTo(context: Context, playerPositionMillis: Int) {
            val intent = Intent(context, MediaPlayerService::class.java)
            intent.action = ACTION_SEEK_TO
            intent.putExtra(EXTRA_SEEK_POSITION, playerPositionMillis)
            context.startService(intent)
        }
    }

    private lateinit var mediaPlayer: ExtendedMediaPlayer
    private lateinit var mediaPlayerTelephony: MediaPlayerTelephony
    private lateinit var mediaSession: ExtendedMediaSession
    private val mediaPlayerReceiverHolder = MediaPlayerReceiverHolder()
    private lateinit var notificationUtils: NotificationUtils
    private lateinit var roomDbRepository: RoomDbRepository

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onCreate() {
        super.onCreate()
        roomDbRepository = RoomDbRepository.getInstance(this)
        notificationUtils = NotificationUtils.getInstance(this)
        mediaPlayer = ExtendedMediaPlayer(this, notificationUtils, roomDbRepository.songQueue)
        mediaSession = ExtendedMediaSession(this, notificationUtils, roomDbRepository.songQueue)
        mediaPlayerTelephony = MediaPlayerTelephony(this, this)
        mediaPlayerReceiverHolder.registerReceivers(this)
        mediaPlayer.init(mediaSession)
        mediaSession.init(mediaPlayer)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        this.mediaPlayer.requestAudioFocus()
        when (intent?.action) {
            ACTION_START -> startPlaying()
            ACTION_PLAY -> mediaSession.play()
            ACTION_PAUSE -> mediaSession.pause()
            ACTION_NEXT -> mediaSession.skipToNext()
            ACTION_PREVIOUS -> mediaSession.skipToPrevious()
            ACTION_STOP -> mediaSession.stop()
            ACTION_SEEK_TO -> onActionSeekTo(intent)
        }
        return START_NOT_STICKY
    }

    private fun startPlaying() {
        mediaPlayer.playMedia()
        notificationUtils.sendMediaNotification(MediaNotification.Builder()
                .setContext(this)
                .setPlaybackStatus(PlaybackStatus.PLAYING)
                .setSessionToken(mediaSession.getSessionToken())
                .setSong(roomDbRepository.songQueue.getCurrentSong())
                .build()
        )
    }

    private fun onActionSeekTo(intent: Intent) {
        val seekPosition = intent.getIntExtra(EXTRA_SEEK_POSITION, mediaPlayer.currentPosition)
        mediaSession.seekTo(seekPosition.toLong())
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.stopMedia()
        mediaPlayer.release()
        mediaPlayer.removeAudioFocus()
        mediaPlayerTelephony.stopListening()
        notificationUtils.removeMediaNotification()
        mediaPlayerReceiverHolder.unregisterReceivers(this)
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        stopSelf()
        super.onTaskRemoved(rootIntent)
    }

    override fun onPhoneBusy() {
        mediaPlayerTelephony.setResumeAfterCall(mediaPlayer.isPlaying)
        mediaPlayer.pauseMedia()
    }

    override fun onPhoneIdle() {
        if (mediaPlayerTelephony.resumeAfterCall()) {
            mediaPlayerTelephony.setResumeAfterCall(false)
            mediaPlayer.resumeMedia()
        }
    }
}