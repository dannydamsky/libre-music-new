package com.damsky.danny.libremusic.service.mediaplayer.player

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.media.AudioAttributes
import android.media.AudioFocusRequest
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.os.Handler
import com.damsky.danny.libremusic.data.db.nonentity.queue.SongQueue
import com.damsky.danny.libremusic.data.prefs.SharedPreferencesHelper
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService
import com.damsky.danny.libremusic.service.mediaplayer.enumeration.PlaybackStatus
import com.damsky.danny.libremusic.service.mediaplayer.session.ExtendedMediaSession
import com.damsky.danny.libremusic.util.Constants
import com.damsky.danny.libremusic.util.CrashUtils
import com.damsky.danny.libremusic.util.notification.MediaNotification
import com.damsky.danny.libremusic.util.notification.NotificationUtils
import java.io.IOException

/**
 * This class extends the functionality of the [MediaPlayer] class as well as implements it.
 *
 * @author Danny Damsky
 */
class ExtendedMediaPlayer(private val mediaPlayerService: MediaPlayerService,
                          private val notificationUtils: NotificationUtils,
                          private val songQueue: SongQueue) : MediaPlayer(),
        MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener,
        AudioManager.OnAudioFocusChangeListener {

    companion object {
        private const val DELAY_SECONDARY_HANDLER_RUNNABLE: Long = 16L
    }

    private val audioAttributes: AudioAttributes
    private val handler = Handler()
    private val mainHandlerRunnable: Runnable
    private val secondaryHandlerRunnable: Runnable
    private val sharedPreferencesHelper = SharedPreferencesHelper.getInstance(mediaPlayerService)
    private val audioManager: AudioManager
    private lateinit var mediaSession: ExtendedMediaSession

    init {
        setOnCompletionListener(this)
        setOnPreparedListener(this)
        audioAttributes = getAudioAttributes()
        mainHandlerRunnable = getMainHandlerRunnable()
        secondaryHandlerRunnable = getSecondaryHandlerRunnable()
        audioManager = mediaPlayerService.getSystemService(Context.AUDIO_SERVICE) as AudioManager
        requestAudioFocus()
        initMediaPlayer()
    }

    /**
     * @param mediaSession the [ExtendedMediaSession] that will be paired with this class.
     */
    fun init(mediaSession: ExtendedMediaSession) {
        this.mediaSession = mediaSession
    }

    private fun getAudioAttributes() = AudioAttributes.Builder()
            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
            .setUsage(AudioAttributes.USAGE_MEDIA)
            .build()

    private fun getMainHandlerRunnable() = Runnable {
        if (sharedPreferencesHelper.repeatEnabled()) {
            val currentSong = songQueue.getCurrentSong()
            seekFromTo(currentSong.startTimeInMillis, currentSong.endTimeInMillis)
        } else {
            skipToNext()
            notificationUtils.sendMediaNotification(MediaNotification.Builder()
                    .setContext(mediaPlayerService)
                    .setPlaybackStatus(PlaybackStatus.PLAYING)
                    .setSessionToken(mediaSession.getSessionToken())
                    .setSong(songQueue.getCurrentSong())
                    .build())
        }
    }

    private fun getSecondaryHandlerRunnable() = object : Runnable {
        override fun run() {
            if (isPlaying) {
                songQueue.setPlayerPositionMillis(currentPosition)
                handler.postDelayed(this, DELAY_SECONDARY_HANDLER_RUNNABLE)
            } else
                handler.removeCallbacks(this)
        }
    }

    /**
     * Requests audio focus from the [AudioManager] class.
     */
    @Throws(IOException::class)
    fun requestAudioFocus() {
        val audioFocusRequestResult = getAudioFocusRequestResult(audioManager)
        if (audioFocusRequestResult != AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
            throw IOException("Unable to grant audio focus")
    }

    @SuppressLint("NewApi")
    private fun getAudioFocusRequestResult(audioManager: AudioManager) =
            if (Constants.IS_OREO_OR_ABOVE)
                audioManager.requestAudioFocus(getAudioFocusRequest())
            else
                audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN)

    @TargetApi(Build.VERSION_CODES.O)
    private fun getAudioFocusRequest() = AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
            .setAudioAttributes(audioAttributes)
            .build()

    private fun initMediaPlayer() {
        reset()
        setAudioAttributes(audioAttributes)
        try {
            setDataSource(songQueue.getCurrentSong().songData)
        } catch (e: IOException) {
            CrashUtils.logException(e)
        } catch (e: Exception) {
            CrashUtils.logError("ExtendedMediaPlayer.initMediaPlayer",
                    "UNEXPECTED ERROR: message=${e.message}")
        }
        prepareAsync()
    }

    private fun postHandler(songDuration: Long) {
        handler.postDelayed(mainHandlerRunnable, songDuration)
        handler.postDelayed(secondaryHandlerRunnable, DELAY_SECONDARY_HANDLER_RUNNABLE)
    }

    private fun cancelHandler() {
        handler.removeCallbacks(mainHandlerRunnable)
        handler.removeCallbacks(secondaryHandlerRunnable)
    }

    override fun onCompletion(mp: MediaPlayer?) {
        stopMedia()
        mediaPlayerService.stopSelf()
    }

    override fun onPrepared(mp: MediaPlayer?) = playMedia()

    /**
     * Plays the current song if it isn't playing yet or if another song is playing.
     */
    fun playMedia() {
        if (!isPlaying) {
            val song = songQueue.getCurrentSong()
            seekFromTo(song.startTimeInMillis, song.endTimeInMillis)
        } else {
            stopMedia()
            initMediaPlayer()
        }
        songQueue.setCurrentSongIsPlaying()
    }

    /**
     * Stops all playback.
     */
    fun stopMedia() {
        if (isPlaying) {
            stop()
            cancelHandler()
            songQueue.setCurrentSongIsNotPlaying()
        }
    }

    /**
     * Pauses the playback.
     */
    fun pauseMedia() {
        if (isPlaying) {
            pause()
            cancelHandler()
            songQueue.setCurrentSongIsNotPlaying()
        }
    }

    /**
     * Resumes from the last known position of the [MediaPlayer].
     */
    fun resumeMedia() {
        if (!isPlaying) {
            seekFromTo(currentPosition, songQueue.getCurrentSong().endTimeInMillis)
            songQueue.setCurrentSongIsPlaying()
        }
    }

    /**
     * Skips to the next track on the [SongQueue].
     */
    fun skipToNext() = skipToNextOrPrevious { songQueue.skipToNextSong() }

    /**
     * Skips to the previous track on the [SongQueue].
     */
    fun skipToPrevious() = skipToNextOrPrevious { songQueue.skipToPreviousSong() }

    private fun skipToNextOrPrevious(incrementOrDecrement: () -> Unit) {
        if (sharedPreferencesHelper.shuffleEnabled())
            songQueue.skipToRandomSong()
        else
            incrementOrDecrement()
        stopMedia()
        initMediaPlayer()
    }


    private fun seekFromTo(from: Int, to: Int) {
        cancelHandler()
        val duration: Int = to - from
        seekTo(from)
        start()
        postHandler(duration.toLong())
    }

    /**
     * Sets [MediaPlayer.seekTo] to [from] and resumes playback if it was paused.
     */
    fun seekFrom(from: Int) {
        if (!isPlaying)
            songQueue.setCurrentSongIsPlaying()
        seekFromTo(from, songQueue.getCurrentSong().endTimeInMillis)
    }

    override fun onAudioFocusChange(focusChange: Int) {
        when (focusChange) {
            AudioManager.AUDIOFOCUS_GAIN -> onAudioFocusGain()
            AudioManager.AUDIOFOCUS_LOSS -> onAudioFocusLoss()
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> onAudioFocusLossTransient()
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> onAudioFocusLossTransientCanDuck()
        }
    }

    private fun onAudioFocusGain() {
        if (!isPlaying) {
            start()
            songQueue.setCurrentSongIsPlaying()
        }
        setVolume(1f, 1f)
    }

    private fun onAudioFocusLoss() {
        if (isPlaying) {
            stop()
            songQueue.setCurrentSongIsNotPlaying()
        }
        release()
    }

    private fun onAudioFocusLossTransient() {
        if (isPlaying) {
            pause()
            songQueue.setCurrentSongIsNotPlaying()
        }
    }

    private fun onAudioFocusLossTransientCanDuck() {
        if (isPlaying)
            setVolume(0.1f, 0.1f)
    }

    @SuppressLint("NewApi")
    @Throws(IOException::class)
    fun removeAudioFocus() {
        val requestResult = if (Constants.IS_OREO_OR_ABOVE)
            audioManager.abandonAudioFocusRequest(getAudioFocusRequest())
        else
            audioManager.abandonAudioFocus(this)

        if (requestResult != AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
            throw IOException("Unable to grant audio focus")
    }

    override fun toString() =
            "ExtendedMediaPlayer(mediaPlayerService=$mediaPlayerService, audioAttributes=$audioAttributes, handler=$handler, mainHandlerRunnable=$mainHandlerRunnable, sharedPreferencesHelper=$sharedPreferencesHelper, songQueue=$songQueue, audioManager=$audioManager, notificationUtils=$notificationUtils, mediaSession=$mediaSession)"
}
