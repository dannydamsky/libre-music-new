package com.damsky.danny.libremusic.data.db

import android.arch.lifecycle.LiveData
import android.content.res.Resources
import com.damsky.danny.libremusic.data.db.dao.*
import com.damsky.danny.libremusic.data.db.nonentity.queue.SongQueue
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.AlbumRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.ArtistRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.GenreRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.PlaylistRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.AbstractSongRowItem

/**
 * This interface contains the functions that the [RoomDbRepository] and [RoomDbViewModel] classes
 * implement.
 *
 * @author Danny Damsky
 */
interface IRoomDbRepository {

    /**
     * The fully-implemented instance of the [RoomDb] class.
     */
    val roomDb: RoomDb

    /**
     * The fully-implemented instance of the DAO (Data Access Object) for the artists table.
     */
    val artistDao: ArtistDao

    /**
     * The fully-implemented instance of the DAO (Data Access Object) for the albums table.
     */
    val albumDao: AlbumDao

    /**
     * The fully-implemented instance of the DAO (Data Access Object) for the songs table.
     */
    val songDao: SongDao

    /**
     * The fully-implemented instance of the DAO (Data Access Object) for the genres table.
     */
    val genreDao: GenreDao

    /**
     * The fully-implemented instance of the DAO (Data Access Object) for the playlists table.
     */
    val playlistDao: PlaylistDao

    /**
     * The fully-implemented instance of the DAO (Data Access Object) for the table linking the songs to the playlists.
     */
    val songsPlaylistsLinkDao: SongsPlaylistsLinkDao

    /**
     * The current queue consisting of songs to be played and an index representing the
     * current song to be played, as well as methods for modifying these properties.
     */
    val songQueue: SongQueue

    /**
     * A [LiveData] of a [List] of [ArtistRowItem]s representing the artists in the database.
     */
    val artistRowItemsLiveData: LiveData<List<ArtistRowItem>>

    /**
     * A [LiveData] of a [List] of [AlbumRowItem]s representing the albums in the database.
     */
    val albumRowItemsLiveData: LiveData<List<AlbumRowItem>>

    /**
     * A [LiveData] of a [List] of [AbstractSongRowItem]s representing the songs in the database.
     */
    val songRowItemsLiveData: LiveData<List<AbstractSongRowItem>>

    /**
     * A [LiveData] of a [List] of [PlaylistRowItem]s representing the playlists in the database.
     */
    val playlistRowItemsLiveData: LiveData<List<PlaylistRowItem>>

    /**
     * A [LiveData] of a [List] of [GenreRowItem]s representing the genres in the database.
     */
    val genreRowItemsLiveData: LiveData<List<GenreRowItem>>

    /**
     * Deletes all rows in all of the tables in the database.
     */
    fun deleteAll()

    /**
     * @param resources used to get a resource string for [AlbumRowItem]s.
     * @param artistName the artist to get the [List] of [AlbumRowItem]s for.
     * @return a [LiveData] of a [List] of [AlbumRowItem]s representing the albums in the database
     * for the given [artistName].
     */
    fun getAlbumRowItemsLiveDataForArtist(resources: Resources, artistName: String): LiveData<List<AlbumRowItem>>

    /**
     * @param artistName the name of the artist to get the songs for.
     * @return a [LiveData] of a [List] of [AbstractSongRowItem]s representing the songs in the database
     * for the given [artistName].
     */
    fun getSongRowItemsLiveDataForArtist(artistName: String): LiveData<List<AbstractSongRowItem>>

    /**
     * @param albumName the name of the album to get the songs for.
     * @return a [LiveData] of a [List] of [AbstractSongRowItem]s representing the songs in the database
     * for the given [albumName].
     */
    fun getSongRowItemsLiveDataForAlbum(albumName: String): LiveData<List<AbstractSongRowItem>>

    /**
     * @param genreName the name of the genre to get the songs for.
     * @return a [LiveData] of a [List] of [AbstractSongRowItem]s representing the songs in the database
     * for the given [genreName].
     */
    fun getSongRowItemsLiveDataForGenre(genreName: String): LiveData<List<AbstractSongRowItem>>

    /**
     * @param playlistId the ID of the playlist to get the songs for.
     * @return a [LiveData] of a [List] of [AbstractSongRowItem]s representing the songs in the database
     * for the given [playlistId].
     */
    fun getSongRowItemsLiveDataForPlaylist(playlistId: Long): LiveData<List<AbstractSongRowItem>>
}