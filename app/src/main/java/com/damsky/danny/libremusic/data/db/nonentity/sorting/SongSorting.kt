package com.damsky.danny.libremusic.data.db.nonentity.sorting

import android.arch.lifecycle.LiveData
import com.damsky.danny.libremusic.data.db.dao.SongDao
import com.damsky.danny.libremusic.data.db.dao.SongsPlaylistsLinkDao
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplaySong
import com.damsky.danny.libremusic.util.CrashUtils

/**
 * The values of this enum class represent the available sorting types for lists representing
 * the song entity.
 *
 * @author Danny Damsky
 */
enum class SongSorting {
    NONE, NAME_ASC, NAME_DESC, YEAR_ASC, YEAR_DESC, ARTIST_NAME_ASC, ARTIST_NAME_DESC,
    ALBUM_NAME_ASC, ALBUM_NAME_DESC, GENRE_NAME_ASC, GENRE_NAME_DESC, DURATION_ASC, DURATION_DESC,
    TRACK_NUMBER_ASC, TRACK_NUMBER_DESC;

    /**
     * @param songDao the DAO (Data Access Object) for working with the genres table.
     *
     * @return the appropriate [LiveData] object of a [List] of [DisplaySong]s according to the
     * sorting enum value.
     */
    fun getDisplaySongsLiveData(songDao: SongDao) = when (this) {
        NONE -> songDao.getDisplaySongsLive()
        NAME_ASC -> songDao.getDisplaySongsOrderedByNameAscLive()
        NAME_DESC -> songDao.getDisplaySongsOrderedByNameDescLive()
        YEAR_ASC -> songDao.getDisplaySongsOrderedByYearAscLive()
        YEAR_DESC -> songDao.getDisplaySongsOrderedByYearDescLive()
        ARTIST_NAME_ASC -> songDao.getDisplaySongsOrderedByArtistNameAscLive()
        ARTIST_NAME_DESC -> songDao.getDisplaySongsOrderedByArtistNameDescLive()
        ALBUM_NAME_ASC -> songDao.getDisplaySongsOrderedByAlbumNameAscLive()
        ALBUM_NAME_DESC -> songDao.getDisplaySongsOrderedByAlbumNameDescLive()
        GENRE_NAME_ASC -> songDao.getDisplaySongsOrderedByGenreNameAscLive()
        GENRE_NAME_DESC -> songDao.getDisplaySongsOrderedByGenreNameDescLive()
        DURATION_ASC -> songDao.getDisplaySongsOrderedByDurationAscLive()
        DURATION_DESC -> songDao.getDisplaySongsOrderedByDurationDescLive()
        TRACK_NUMBER_ASC -> songDao.getDisplaySongsOrderedByTrackNumberAscLive()
        TRACK_NUMBER_DESC -> songDao.getDisplaySongsOrderedByTrackNumberDescLive()
    }

    /**
     * @param songDao the DAO (Data Access Object) for working with the genres table.
     * @param artistName the name of the artist of the songs to look for.
     *
     * @return the appropriate [LiveData] object of a [List] of [DisplaySong]s for the given [artistName],
     * according to the sorting enum value.
     */
    fun getDisplaySongsForArtistLiveData(songDao: SongDao, artistName: String) = when (this) {
        NONE -> songDao.getDisplaySongsForArtistLive(artistName)
        NAME_ASC -> songDao.getDisplaySongsForArtistOrderedByNameAscLive(artistName)
        NAME_DESC -> songDao.getDisplaySongsForArtistOrderedByNameDescLive(artistName)
        YEAR_ASC -> songDao.getDisplaySongsForArtistOrderedByYearAscLive(artistName)
        YEAR_DESC -> songDao.getDisplaySongsForArtistOrderedByYearDescLive(artistName)
        ALBUM_NAME_ASC -> songDao.getDisplaySongsForArtistOrderedByAlbumNameAscLive(artistName)
        ALBUM_NAME_DESC -> songDao.getDisplaySongsForArtistOrderedByAlbumNameDescLive(artistName)
        GENRE_NAME_ASC -> songDao.getDisplaySongsForArtistOrderedByGenreNameAscLive(artistName)
        GENRE_NAME_DESC -> songDao.getDisplaySongsForArtistOrderedByGenreNameDescLive(artistName)
        DURATION_ASC -> songDao.getDisplaySongsForArtistOrderedByDurationAscLive(artistName)
        DURATION_DESC -> songDao.getDisplaySongsForArtistOrderedByDurationDescLive(artistName)
        TRACK_NUMBER_ASC -> songDao.getDisplaySongsForArtistOrderedByTrackNumberAscLive(artistName)
        TRACK_NUMBER_DESC -> songDao.getDisplaySongsForArtistOrderedByTrackNumberDescLive(artistName)
        ARTIST_NAME_ASC, ARTIST_NAME_DESC -> {
            CrashUtils.logError("SongSorting.getDisplaySongsForArtistLiveData",
                    "artistName=$artistName, this=$this")
            songDao.getDisplaySongsForArtistLive(artistName)
        }
    }

    /**
     * @param songDao the DAO (Data Access Object) for working with the genres table.
     * @param albumName the name of the album of the songs to look for.
     *
     * @return the appropriate [LiveData] object of a [List] of [DisplaySong]s for the given [albumName],
     * according to the sorting enum value.
     */
    fun getDisplaySongsForAlbumLiveData(songDao: SongDao, albumName: String) = when (this) {
        NONE -> songDao.getDisplaySongsForAlbumLive(albumName)
        NAME_ASC -> songDao.getDisplaySongsForAlbumOrderedByNameAscLive(albumName)
        NAME_DESC -> songDao.getDisplaySongsForAlbumOrderedByNameDescLive(albumName)
        TRACK_NUMBER_ASC -> songDao.getDisplaySongsForAlbumOrderedByTrackNumberAscLive(albumName)
        TRACK_NUMBER_DESC -> songDao.getDisplaySongsForAlbumOrderedByTrackNumberDescLive(albumName)
        DURATION_ASC -> songDao.getDisplaySongsForAlbumOrderedByDurationAscLive(albumName)
        DURATION_DESC -> songDao.getDisplaySongsForAlbumOrderedByDurationDescLive(albumName)
        else -> {
            CrashUtils.logError("SongSorting.getDisplaySongsForAlbumLiveData",
                    "albumName=$albumName, this=$this")
            songDao.getDisplaySongsForAlbumLive(albumName)
        }
    }

    /**
     * @param songDao the DAO (Data Access Object) for working with the genres table.
     * @param genreName the name of the genre of the songs to look for.
     *
     * @return the appropriate [LiveData] object of a [List] of [DisplaySong]s for the given [genreName],
     * according to the sorting enum value.
     */
    fun getDisplaySongsForGenreLiveData(songDao: SongDao, genreName: String) = when (this) {
        NONE -> songDao.getDisplaySongsForGenreLive(genreName)
        NAME_ASC -> songDao.getDisplaySongsForGenreOrderedByNameAscLive(genreName)
        NAME_DESC -> songDao.getDisplaySongsForGenreOrderedByNameDescLive(genreName)
        YEAR_ASC -> songDao.getDisplaySongsForGenreOrderedByYearAscLive(genreName)
        YEAR_DESC -> songDao.getDisplaySongsForGenreOrderedByYearDescLive(genreName)
        ALBUM_NAME_ASC -> songDao.getDisplaySongsForGenreOrderedByAlbumNameAscLive(genreName)
        ALBUM_NAME_DESC -> songDao.getDisplaySongsForGenreOrderedByAlbumNameDescLive(genreName)
        ARTIST_NAME_ASC -> songDao.getDisplaySongsForGenreOrderedByArtistNameAscLive(genreName)
        ARTIST_NAME_DESC -> songDao.getDisplaySongsForGenreOrderedByArtistNameDescLive(genreName)
        DURATION_ASC -> songDao.getDisplaySongsForGenreOrderedByDurationAscLive(genreName)
        DURATION_DESC -> songDao.getDisplaySongsForGenreOrderedByDurationDescLive(genreName)
        TRACK_NUMBER_ASC -> songDao.getDisplaySongsForGenreOrderedByTrackNumberAscLive(genreName)
        TRACK_NUMBER_DESC -> songDao.getDisplaySongsForGenreOrderedByTrackNumberDescLive(genreName)
        GENRE_NAME_ASC, GENRE_NAME_DESC -> {
            CrashUtils.logError("SongSorting.getDisplaySongsForGenreLiveData",
                    "genreName=$genreName, this=$this")
            songDao.getDisplaySongsForGenreLive(genreName)
        }
    }

    /**
     * @param songDao the DAO (Data Access Object) for working with the genres table.
     * @param playlistId the ID of the playlist of the songs to look for.
     *
     * @return the appropriate [LiveData] object of a [List] of [DisplaySong]s for the given [playlistId],
     * according to the sorting enum value.
     */
    fun getDisplaySongsForPlaylistLiveData(songDao: SongsPlaylistsLinkDao, playlistId: Long) = when (this) {
        NONE -> songDao.getDisplaySongsForPlaylistLive(playlistId)
        NAME_ASC -> songDao.getDisplaySongsForPlaylistOrderedBySongNameAscLive(playlistId)
        NAME_DESC -> songDao.getDisplaySongsForPlaylistOrderedBySongNameDescLive(playlistId)
        YEAR_ASC -> songDao.getDisplaySongsForPlaylistOrderedByYearPublishedAscLive(playlistId)
        YEAR_DESC -> songDao.getDisplaySongsForPlaylistOrderedByYearPublishedDescLive(playlistId)
        ALBUM_NAME_ASC -> songDao.getDisplaySongsForPlaylistOrderedByAlbumNameAscLive(playlistId)
        ALBUM_NAME_DESC -> songDao.getDisplaySongsForPlaylistOrderedByAlbumNameDescLive(playlistId)
        ARTIST_NAME_ASC -> songDao.getDisplaySongsForPlaylistOrderedByArtistNameAscLive(playlistId)
        ARTIST_NAME_DESC -> songDao.getDisplaySongsForPlaylistOrderedByArtistNameDescLive(playlistId)
        GENRE_NAME_ASC -> songDao.getDisplaySongsForPlaylistOrderedByGenreNameAscLive(playlistId)
        GENRE_NAME_DESC -> songDao.getDisplaySongsForPlaylistOrderedByGenreNameDescLive(playlistId)
        DURATION_ASC -> songDao.getDisplaySongsForPlaylistOrderedByDurationAscLive(playlistId)
        DURATION_DESC -> songDao.getDisplaySongsForPlaylistOrderedByDurationDescLive(playlistId)
        TRACK_NUMBER_ASC -> songDao.getDisplaySongsForPlaylistOrderedByTrackNumberAscLive(playlistId)
        TRACK_NUMBER_DESC -> songDao.getDisplaySongsForPlaylistOrderedByTrackNumberDescLive(playlistId)
    }

    /**
     * @param songDao the DAO (Data Access Object) for working with the genres table.
     *
     * @return the appropriate [List] of PlayableSongs according to the
     * sorting enum value.
     */
    fun getPlayableSongs(songDao: SongDao) = when (this) {
        NONE -> songDao.getPlayableSongs()
        NAME_ASC -> songDao.getPlayableSongsOrderedByNameAsc()
        NAME_DESC -> songDao.getPlayableSongsOrderedByNameDesc()
        YEAR_ASC -> songDao.getPlayableSongsOrderedByYearAsc()
        YEAR_DESC -> songDao.getPlayableSongsOrderedByYearDesc()
        ARTIST_NAME_ASC -> songDao.getPlayableSongsOrderedByArtistNameAsc()
        ARTIST_NAME_DESC -> songDao.getPlayableSongsOrderedByArtistNameDesc()
        ALBUM_NAME_ASC -> songDao.getPlayableSongsOrderedByAlbumNameAsc()
        ALBUM_NAME_DESC -> songDao.getPlayableSongsOrderedByAlbumNameDesc()
        GENRE_NAME_ASC -> songDao.getPlayableSongsOrderedByGenreNameAsc()
        GENRE_NAME_DESC -> songDao.getPlayableSongsOrderedByGenreNameDesc()
        DURATION_ASC -> songDao.getPlayableSongsOrderedByDurationAsc()
        DURATION_DESC -> songDao.getPlayableSongsOrderedByDurationDesc()
        TRACK_NUMBER_ASC -> songDao.getPlayableSongsOrderedByTrackNumberAsc()
        TRACK_NUMBER_DESC -> songDao.getPlayableSongsOrderedByTrackNumberDesc()
    }

    /**
     * @param songDao the DAO (Data Access Object) for working with the genres table.
     * @param artistName the name of the artist of the songs to look for.
     *
     * @return the appropriate [List] of PlayableSongs for the given [artistName],
     * according to the sorting enum value.
     */
    fun getPlayableSongsForArtist(songDao: SongDao, artistName: String) = when (this) {
        NONE -> songDao.getPlayableSongsForArtist(artistName)
        NAME_ASC -> songDao.getPlayableSongsForArtistOrderedByNameAsc(artistName)
        NAME_DESC -> songDao.getPlayableSongsForArtistOrderedByNameDesc(artistName)
        YEAR_ASC -> songDao.getPlayableSongsForArtistOrderedByYearAsc(artistName)
        YEAR_DESC -> songDao.getPlayableSongsForArtistOrderedByYearDesc(artistName)
        ALBUM_NAME_ASC -> songDao.getPlayableSongsForArtistOrderedByAlbumNameAsc(artistName)
        ALBUM_NAME_DESC -> songDao.getPlayableSongsForArtistOrderedByAlbumNameDesc(artistName)
        GENRE_NAME_ASC -> songDao.getPlayableSongsForArtistOrderedByGenreNameAsc(artistName)
        GENRE_NAME_DESC -> songDao.getPlayableSongsForArtistOrderedByGenreNameDesc(artistName)
        DURATION_ASC -> songDao.getPlayableSongsForArtistOrderedByDurationAsc(artistName)
        DURATION_DESC -> songDao.getPlayableSongsForArtistOrderedByDurationDesc(artistName)
        TRACK_NUMBER_ASC -> songDao.getPlayableSongsForArtistOrderedByTrackNumberAsc(artistName)
        TRACK_NUMBER_DESC -> songDao.getPlayableSongsForArtistOrderedByTrackNumberDesc(artistName)
        ARTIST_NAME_ASC, ARTIST_NAME_DESC -> {
            CrashUtils.logError("SongSorting.getPlayableSongsForArtist",
                    "artistName=$artistName, this=$this")
            songDao.getPlayableSongsForArtist(artistName)
        }
    }

    /**
     * @param songDao the DAO (Data Access Object) for working with the genres table.
     * @param albumName the name of the album of the songs to look for.
     *
     * @return the appropriate [List] of PlayableSongs for the given [albumName],
     * according to the sorting enum value.
     */
    fun getPlayableSongsForAlbum(songDao: SongDao, albumName: String) = when (this) {
        NONE -> songDao.getPlayableSongsForAlbum(albumName)
        NAME_ASC -> songDao.getPlayableSongsForAlbumOrderedByNameAsc(albumName)
        NAME_DESC -> songDao.getPlayableSongsForAlbumOrderedByNameDesc(albumName)
        TRACK_NUMBER_ASC -> songDao.getPlayableSongsForAlbumOrderedByTrackNumberAsc(albumName)
        TRACK_NUMBER_DESC -> songDao.getPlayableSongsForAlbumOrderedByTrackNumberDesc(albumName)
        DURATION_ASC -> songDao.getPlayableSongsForAlbumOrderedByDurationAsc(albumName)
        DURATION_DESC -> songDao.getPlayableSongsForAlbumOrderedByDurationDesc(albumName)
        else -> {
            CrashUtils.logError("SongSorting.getPlayableSongsForAlbum",
                    "albumName=$albumName, this=$this")
            songDao.getPlayableSongsForAlbum(albumName)
        }
    }

    /**
     * @param songDao the DAO (Data Access Object) for working with the genres table.
     * @param genreName the name of the genre of the songs to look for.
     *
     * @return the appropriate [List] of PlayableSongs for the given [genreName],
     * according to the sorting enum value.
     */
    fun getPlayableSongsForGenre(songDao: SongDao, genreName: String) = when (this) {
        NONE -> songDao.getPlayableSongsForGenre(genreName)
        NAME_ASC -> songDao.getPlayableSongsForGenreOrderedByNameAsc(genreName)
        NAME_DESC -> songDao.getPlayableSongsForGenreOrderedByNameDesc(genreName)
        YEAR_ASC -> songDao.getPlayableSongsForGenreOrderedByYearAsc(genreName)
        YEAR_DESC -> songDao.getPlayableSongsForGenreOrderedByYearDesc(genreName)
        ALBUM_NAME_ASC -> songDao.getPlayableSongsForGenreOrderedByAlbumNameAsc(genreName)
        ALBUM_NAME_DESC -> songDao.getPlayableSongsForGenreOrderedByAlbumNameDesc(genreName)
        ARTIST_NAME_ASC -> songDao.getPlayableSongsForGenreOrderedByArtistNameAsc(genreName)
        ARTIST_NAME_DESC -> songDao.getPlayableSongsForGenreOrderedByArtistNameDesc(genreName)
        DURATION_ASC -> songDao.getPlayableSongsForGenreOrderedByDurationAsc(genreName)
        DURATION_DESC -> songDao.getPlayableSongsForGenreOrderedByDurationDesc(genreName)
        TRACK_NUMBER_ASC -> songDao.getPlayableSongsForGenreOrderedByTrackNumberAsc(genreName)
        TRACK_NUMBER_DESC -> songDao.getPlayableSongsForGenreOrderedByTrackNumberDesc(genreName)
        GENRE_NAME_ASC, GENRE_NAME_DESC -> {
            CrashUtils.logError("SongSorting.getPlayableSongsForGenre",
                    "genreName=$genreName, this=$this")
            songDao.getPlayableSongsForGenre(genreName)
        }
    }

    /**
     * @param songDao the DAO (Data Access Object) for working with the genres table.
     * @param playlistId the ID of the playlist of the songs to look for.
     *
     * @return the appropriate [List] of PlayableSongs for the given [playlistId],
     * according to the sorting enum value.
     */
    fun getPlayableSongsForPlaylist(songDao: SongsPlaylistsLinkDao, playlistId: Long) = when (this) {
        NONE -> songDao.getPlayableSongsForPlaylist(playlistId)
        NAME_ASC -> songDao.getPlayableSongsForPlaylistOrderedBySongNameAsc(playlistId)
        NAME_DESC -> songDao.getPlayableSongsForPlaylistOrderedBySongNameDesc(playlistId)
        YEAR_ASC -> songDao.getPlayableSongsForPlaylistOrderedByYearPublishedAsc(playlistId)
        YEAR_DESC -> songDao.getPlayableSongsForPlaylistOrderedByYearPublishedDesc(playlistId)
        ALBUM_NAME_ASC -> songDao.getPlayableSongsForPlaylistOrderedByAlbumNameAsc(playlistId)
        ALBUM_NAME_DESC -> songDao.getPlayableSongsForPlaylistOrderedByAlbumNameDesc(playlistId)
        ARTIST_NAME_ASC -> songDao.getPlayableSongsForPlaylistOrderedByArtistNameAsc(playlistId)
        ARTIST_NAME_DESC -> songDao.getPlayableSongsForPlaylistOrderedByArtistNameDesc(playlistId)
        GENRE_NAME_ASC -> songDao.getPlayableSongsForPlaylistOrderedByGenreNameAsc(playlistId)
        GENRE_NAME_DESC -> songDao.getPlayableSongsForPlaylistOrderedByGenreNameDesc(playlistId)
        DURATION_ASC -> songDao.getPlayableSongsForPlaylistOrderedByDurationAsc(playlistId)
        DURATION_DESC -> songDao.getPlayableSongsForPlaylistOrderedByDurationDesc(playlistId)
        TRACK_NUMBER_ASC -> songDao.getPlayableSongsForPlaylistOrderedByTrackNumberAsc(playlistId)
        TRACK_NUMBER_DESC -> songDao.getPlayableSongsForPlaylistOrderedByTrackNumberDesc(playlistId)
    }
}