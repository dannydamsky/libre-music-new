package com.damsky.danny.libremusic.data.db.nonentity.queue.callback

import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong

/**
 * This callback is used for when the selected song in the queue has changed.
 *
 * @author Danny Damsky
 */
interface OnSongChangedListener : BaseCallback {

    /**
     * @param song the currently selected song in the queue.
     * @param isPlaying true if the song is already in playback, false otherwise.
     * @param currentSongIndex the index of the given [song] in the queue.
     * @param maxIndex the total amount of songs in the queue.
     * @param playerPositionMillis the current seek position of the media player.
     */
    fun onSongChanged(song: PlayableSong, isPlaying: Boolean, currentSongIndex: Int, maxIndex: Int, playerPositionMillis: Int)
}