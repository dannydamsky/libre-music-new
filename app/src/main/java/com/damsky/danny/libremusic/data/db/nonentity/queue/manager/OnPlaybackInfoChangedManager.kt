package com.damsky.danny.libremusic.data.db.nonentity.queue.manager

import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnPlaybackInfoChangedListener

/**
 * A manager for the [OnPlaybackInfoChangedListener]s.
 *
 * @author Danny Damsky
 * @see BaseManager
 * @see OnPlaybackInfoChangedListener
 */
class OnPlaybackInfoChangedManager : BaseManager<OnPlaybackInfoChangedListener>() {

    /**
     * @see OnPlaybackInfoChangedListener.onPlaybackStopped
     */
    fun notifyOnPlaybackStopped() = forEach { it.onPlaybackStopped() }

    /**
     * @see OnPlaybackInfoChangedListener.onPlaybackStarted
     */
    fun notifyOnPlaybackStarted() = forEach { it.onPlaybackStarted() }

    /**
     * @see OnPlaybackInfoChangedListener.onQueueResized
     */
    fun notifyOnQueueResized(currentSongIndex: Int, maxIndex: Int) =
            forEach { it.onQueueResized(currentSongIndex, maxIndex) }

    /**
     * @see OnPlaybackInfoChangedListener.onPlayerPositionChanged
     */
    fun notifyOnPlayerPositionChanged(playerPositionMillis: Int) =
            forEach { it.onPlayerPositionChanged(playerPositionMillis) }

}