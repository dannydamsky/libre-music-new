package com.damsky.danny.libremusic.data.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.damsky.danny.libremusic.data.db.RoomDbConstants
import com.damsky.danny.libremusic.data.db.entity.Playlist
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayPlaylist

/**
 * An interface that contains all the functions to be implemented by Room for working with
 * the playlists table in the database.
 *
 * @author Danny Damsky
 * @see Playlist
 * @see BaseDao
 */
@Dao
interface PlaylistDao : BaseDao<Playlist> {

    //******************************************************************//
    //******************GENERAL**DAO**FUNCTIONS************************//
    //****************************************************************//

    /**
     * @param playlistId the ID of the playlist whose name should be updated.
     * @param playlistName the new name of the playlist.
     */
    @Query("UPDATE PLAYLISTS SET PLAYLIST_NAME=:playlistName WHERE PLAYLIST_ID=:playlistId;")
    fun updateNameById(playlistId: Long, playlistName: String)

    /**
     * @param playlistId the ID of the playlist to remove from the database.
     */
    @Query("DELETE FROM PLAYLISTS WHERE PLAYLIST_ID=:playlistId;")
    fun deleteById(playlistId: Long)

    /**
     * @return the total amount of rows in the playlists table.
     */
    @Query("SELECT COUNT(PLAYLIST_ID) FROM PLAYLISTS;")
    fun getCount(): Int

    /**
     * Empties the playlists table in the database.
     */
    @Query("DELETE FROM PLAYLISTS")
    fun deleteAll()

    /**
     * @return a [LiveData] of a [List] of [DisplayPlaylist]s. This LiveData represents all playlists in
     * the database, it includes all of [Playlist]'s columns as well as another column representing the
     * [Playlist]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONGS_PLAYLISTS_LINK_PLAYLIST_ID) FROM SONGS_PLAYLISTS_LINKS WHERE SONGS_PLAYLISTS_LINK_PLAYLIST_ID=PLAYLIST_ID) as ${RoomDbConstants.COUNT_SONGS} FROM PLAYLISTS;")
    fun getAllDisplayLive(): LiveData<List<DisplayPlaylist>>

    /**
     * @return a [LiveData] of a [List] of [DisplayPlaylist]s ordered by the name of the playlist in ascending order.
     * This LiveData represents all playlists in the database,
     * it includes all of [Playlist]'s columns as well as another column representing the [Playlist]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONGS_PLAYLISTS_LINK_PLAYLIST_ID) FROM SONGS_PLAYLISTS_LINKS WHERE SONGS_PLAYLISTS_LINK_PLAYLIST_ID=PLAYLIST_ID) as ${RoomDbConstants.COUNT_SONGS} FROM PLAYLISTS ORDER BY PLAYLIST_NAME ASC;")
    fun getAllDisplayOrderedByNameAscLive(): LiveData<List<DisplayPlaylist>>

    /**
     * @return a [LiveData] of a [List] of [DisplayPlaylist]s ordered by the name of the playlist in descending order.
     * This LiveData represents all playlists in the database,
     * it includes all of [Playlist]'s columns as well as another column representing the [Playlist]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONGS_PLAYLISTS_LINK_PLAYLIST_ID) FROM SONGS_PLAYLISTS_LINKS WHERE SONGS_PLAYLISTS_LINK_PLAYLIST_ID=PLAYLIST_ID) as ${RoomDbConstants.COUNT_SONGS} FROM PLAYLISTS ORDER BY PLAYLIST_NAME DESC;")
    fun getAllDisplayOrderedByNameDescLive(): LiveData<List<DisplayPlaylist>>

    /**
     * @param songId the ID of the song to display if it already exists in each playlist or not.
     * @return a [List] of [DisplayPlaylist]s. This LiveData represents all playlists in the database,
     * it includes all of [Playlist]'s columns as well as another column representing the [Playlist]'s
     * songs count for the given [songId] (Using this function that number can only be 1 or 0 indicating
     * whether or not the song exists in the playlist already).
     */
    @Query("SELECT *, (SELECT COUNT(SONGS_PLAYLISTS_LINK_PLAYLIST_ID) FROM SONGS_PLAYLISTS_LINKS WHERE SONGS_PLAYLISTS_LINK_PLAYLIST_ID=PLAYLIST_ID AND SONGS_PLAYLISTS_LINK_SONG_ID=:songId) as ${RoomDbConstants.COUNT_SONGS} FROM PLAYLISTS;")
    fun getAllDisplayForSongById(songId: Long): List<DisplayPlaylist>

    /**
     * @param songIds the IDs of the songs to display if they already exist in each playlist or not.
     * @return a [List] of [DisplayPlaylist]s. This LiveData represents all playlists in the database,
     * it includes all of [Playlist]'s columns as well as another column representing the [Playlist]'s
     * songs count for the given [songIds] (Using this function that number can be anywhere between
     * 0 and the total size of [songIds], if it equals the size of [songIds] it means that all of
     * the given songs are present in the playlist).
     */
    @Query("SELECT *, (SELECT COUNT(SONGS_PLAYLISTS_LINK_PLAYLIST_ID) FROM SONGS_PLAYLISTS_LINKS WHERE SONGS_PLAYLISTS_LINK_PLAYLIST_ID=PLAYLIST_ID AND SONGS_PLAYLISTS_LINK_SONG_ID IN (:songIds)) as ${RoomDbConstants.COUNT_SONGS} FROM PLAYLISTS;")
    fun getAllDisplayForSongsByIds(songIds: List<Long>): List<DisplayPlaylist>
}