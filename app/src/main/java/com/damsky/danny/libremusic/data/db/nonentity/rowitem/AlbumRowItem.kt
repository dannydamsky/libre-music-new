package com.damsky.danny.libremusic.data.db.nonentity.rowitem

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.content.Context
import android.content.res.Resources
import android.support.v7.util.DiffUtil
import android.view.View
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.RoomDbConstants
import com.damsky.danny.libremusic.data.db.entity.Album
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayAlbum
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks
import com.damsky.danny.libremusic.util.PopupMenuBuilder
import com.damsky.danny.libremusic.util.RowItemMenuAction

/**
 * An [EntityRowItem] for the [Album] entity.
 *
 * @author Danny Damsky
 *
 * @param resources are used to retrieve string resources for the [AlbumRowItem].
 * @param displayAlbum the entity object for this row item.
 */
class AlbumRowItem private constructor(resources: Resources, private val displayAlbum: DisplayAlbum) : EntityRowItem<Album> {

    companion object {
        /**
         * An interface implementation used to compare different [AlbumRowItem]s.
         */
        @JvmField
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<AlbumRowItem>() {
            override fun areItemsTheSame(p0: AlbumRowItem, p1: AlbumRowItem) = p0 === p1
            override fun areContentsTheSame(p0: AlbumRowItem, p1: AlbumRowItem) = p0.displayAlbum == p1.displayAlbum
        }

        /**
         * @param resources are used to retrieve string resources for the [AlbumRowItem].
         * @param dbLiveData the [LiveData] object to transform into a [LiveData] of a [List] of
         * [AlbumRowItem]s.
         * @return a [LiveData] of a [List] of [AlbumRowItem]s which are related to the given
         * [dbLiveData].
         */
        @JvmStatic
        fun getLiveData(resources: Resources, dbLiveData: LiveData<List<DisplayAlbum>>) = Transformations.switchMap(dbLiveData) { input ->
            val albumModelList = input.map { AlbumRowItem(resources, it) }
            val liveData = MutableLiveData<List<AlbumRowItem>>()
            liveData.postValue(albumModelList)
            liveData
        }!!

    }

    private val secondaryText = buildSecondaryText(resources, displayAlbum.songsCount)
    private val tertiaryText = buildTertiaryText()
    private val popupMenuBuilder = buildPopupMenuBuilder()

    private fun buildSecondaryText(resources: Resources, albumSongsCount: Int): String {
        val firstString = this.displayAlbum.album.artistName
        val secondString = resources.getQuantityString(R.plurals.songs, albumSongsCount, albumSongsCount)
        return "$firstString | $secondString"
    }

    private fun buildTertiaryText() =
            if (displayAlbum.album.yearPublished == RoomDbConstants.YEAR_PUBLISHED_NONE) ""
            else "${displayAlbum.album.yearPublished}"

    private fun buildPopupMenuBuilder() = PopupMenuBuilder<Album>()
            .inflate(R.menu.menu_row_item_album)
            .setItem(this.displayAlbum.album)
            .setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.playSongs -> RowItemMenuAction.ACTION_PLAY
                    R.id.addToQueue -> RowItemMenuAction.ACTION_ADD_TO_QUEUE
                    R.id.addSongsToPlaylist -> RowItemMenuAction.ACTION_ADD_TO_PLAYLIST
                    R.id.shareSongs -> RowItemMenuAction.ACTION_SHARE
                    R.id.viewSongs -> RowItemMenuAction.ACTION_VIEW_ALL_SONGS
                    else -> RowItemMenuAction.ACTION_PLAY
                }
            }

    override fun getCoverData() = displayAlbum.album.coverData

    override fun getPlaceHolderImage() = R.drawable.ic_album_round_72dp

    override fun getMainText() = displayAlbum.album.name

    override fun getSecondaryText() = secondaryText

    override fun getTertiaryText() = tertiaryText

    override fun buildPopupMenu(context: Context, anchorView: View, itemPosition: Int, callbacks: MainActivityRecyclerViewAdapterCallbacks<Album>) =
            popupMenuBuilder
                    .setItemPosition(itemPosition)
                    .setRecyclerViewOnClickListener(callbacks)
                    .build(context, anchorView)

    override fun getObject() = displayAlbum.album

    override fun toString() = "AlbumRowItem(displayAlbum=$displayAlbum)"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AlbumRowItem

        if (displayAlbum != other.displayAlbum) return false

        return true
    }

    override fun hashCode() = displayAlbum.hashCode()

}