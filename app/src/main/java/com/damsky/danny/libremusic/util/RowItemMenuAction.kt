package com.damsky.danny.libremusic.util

/**
 * These enum values are used for passing information in the menus where these are used.
 *
 * @author Danny Damsky
 */
enum class RowItemMenuAction {
    ACTION_PLAY,
    ACTION_ADD_TO_QUEUE,
    ACTION_REMOVE_FROM_QUEUE,
    ACTION_ADD_TO_PLAYLIST,
    ACTION_VIEW_ALL_SONGS,
    ACTION_SHARE,
    ACTION_SET_AS_RINGTONE,
    ACTION_RENAME_PLAYLIST,
    ACTION_REMOVE_PLAYLIST,
    ACTION_REMOVE_FROM_PLAYLIST
}