package com.damsky.danny.libremusic.service

import android.app.IntentService
import android.content.Context
import android.content.Intent
import com.damsky.danny.libremusic.data.db.RoomDbRepository
import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import com.damsky.danny.libremusic.data.db.nonentity.queue.SongQueue
import com.damsky.danny.libremusic.data.prefs.SharedPreferencesHelper
import com.damsky.danny.libremusic.service.QueueInitService.Companion.start

/**
 * This service is a background service used to initialize the application's [SongQueue] object.
 * This service should only be called after [LibraryScanService] has finished its work.
 *
 * This service can be started by calling [start].
 *
 * @author Danny Damsky
 */
class QueueInitService : IntentService("com.damsky.danny.libremusic.service.QueueInitService") {

    companion object {

        /**
         * Calling this function will start the [QueueInitService].
         * @param context a context of the application package used to call this function.
         */
        @JvmStatic
        fun start(context: Context) {
            val serviceIntent = Intent(context, QueueInitService::class.java)
            context.startService(serviceIntent)
        }
    }

    private lateinit var roomDbRepository: RoomDbRepository
    private lateinit var sharedPreferencesHelper: SharedPreferencesHelper

    override fun onHandleIntent(intent: Intent?) {
        roomDbRepository = RoomDbRepository.getInstance(this)
        startWorking()
    }

    private fun startWorking() {
        if (songsAreAvailable()) {
            sharedPreferencesHelper = SharedPreferencesHelper.getInstance(this)
            initSongQueue()
        }
    }

    private fun songsAreAvailable() = roomDbRepository.songDao.getCount() > 0

    private fun initSongQueue() {
        val queue = sharedPreferencesHelper.getQueue()
        if (!queue.isNullOrEmpty())
            buildQueueFromSharedPrefs(queue)
        else
            generateNewQueue()

    }

    private fun buildQueueFromSharedPrefs(queue: MutableMap<String, *>) {
        val queueKeys = queue.keys.asSequence().sortedBy { it.toInt() }.toList()
        val queueValues = queueKeys.map { queue[it] as Long }
        val queueSongs = getQueueSongsFromTransaction(queueValues)
        val lastKnownIndex = sharedPreferencesHelper.getLastKnownIndex()
        roomDbRepository.songQueue.setSongs(queueSongs, lastKnownIndex)
    }

    private fun getQueueSongsFromTransaction(queueValues: List<Long>): ArrayList<PlayableSong> {
        roomDbRepository.roomDb.beginTransaction()
        val queueSongs = queueValues.map { roomDbRepository.songDao.getPlayableSongById(it) }
        roomDbRepository.roomDb.setTransactionSuccessful()
        roomDbRepository.roomDb.endTransaction()
        return queueSongs as ArrayList<PlayableSong>
    }

    private fun generateNewQueue() {
        val songSorting = this.sharedPreferencesHelper.getSongSorting()
        val queueSongs = songSorting.getPlayableSongs(roomDbRepository.songDao)
        roomDbRepository.songQueue.setSongs(queueSongs as ArrayList<PlayableSong>)
    }
}
