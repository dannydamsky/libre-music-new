package com.damsky.danny.libremusic.ui.activity.main

import android.support.annotation.StringRes
import com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.WelcomeDialogFragment

/**
 * This interface contains all functions that [MainActivity] implements.
 * This interface is implemented by [IMainActivity], allowing implementations of all [MainActivity]
 * functions in other classes.
 *
 * @author Danny Damsky
 */
interface IMainActivityExtras : WelcomeDialogFragment.Listener {

    /**
     * Shows a simple SnackBar with text for a short time period.
     *
     * @param textId the id of the resource that contains the string to be displayed on the snackbar.
     */
    fun showSnackBarShort(@StringRes textId: Int)

    /**
     * Shows a simple SnackBar with text for a long time period.
     *
     * @param textId the id of the resource that contains the string to be displayed on the snackbar.
     */
    fun showSnackBarLong(@StringRes textId: Int)

}