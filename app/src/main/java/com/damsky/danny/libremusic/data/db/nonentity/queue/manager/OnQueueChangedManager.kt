package com.damsky.danny.libremusic.data.db.nonentity.queue.manager

import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnQueueChangedListener
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.AbstractSongRowItem

/**
 * A manager for the [OnQueueChangedListener]s.
 *
 * @author Danny Damsky
 * @see BaseManager
 * @see OnQueueChangedListener
 */
class OnQueueChangedManager : BaseManager<OnQueueChangedListener>() {

    /**
     * @see OnQueueChangedListener.onQueueChanged
     */
    fun notifyOnQueueChanged(queue: List<AbstractSongRowItem>) =
            forEach { it.onQueueChanged(queue) }
}