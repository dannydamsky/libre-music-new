package com.damsky.danny.libremusic.data.db.nonentity.queue.callback

/**
 * This interface should be implemented by other interfaces in order to produce proper callbacks
 * from the SongQueue to other classes.
 *
 * @author Danny Damsky
 */
interface BaseCallback {

    /**
     * Indicates whether or not the callback has been registered.
     * This variable should be instantiated to false, and never modified from inside the
     * implementing class anywhere else.
     */
    var isRegistered: Boolean
}