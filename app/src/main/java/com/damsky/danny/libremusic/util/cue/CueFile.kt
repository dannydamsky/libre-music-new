package com.damsky.danny.libremusic.util.cue

import com.damsky.danny.libremusic.util.loader.SmallMediaObject
import java.io.File

/**
 * This class is used mainly by the [CueFileParser] for parsing the cue sheet stored in [file].
 *
 * @param file the file that contains the cue sheet.
 * @param  songFilePath the path to the audio file.
 * @param  coverArtPath the path to the audio file's cover art image.
 * @param durationInMillis the total duration of the audio file in milliseconds.
 *
 * @author Danny Damsky
 * @see CueFileParser
 *
 * @property file the file that contains the cue sheet.
 * @property songFilePath the path to the audio file.
 * @property coverArtPath the path to the audio file's cover art image.
 * @property durationInMillis the total duration of the audio file in milliseconds.
 */
data class CueFile(val file: File,
                   val songFilePath: String,
                   val coverArtPath: String?,
                   val durationInMillis: Int) {

    companion object {

        /**
         * Builds a new [CueFile] object using the properties of [smallMediaObject].
         *
         * @param smallMediaObject a media object containing enough information for cue parsing.
         * @return a new [CueFile] object, or null if the audio file is too short to contain a cue sheet.
         */
        @JvmStatic
        fun newInstance(smallMediaObject: SmallMediaObject): CueFile? {
            if (smallMediaObject.durationInMillis >= 600_000) {
                val fileWithCueSheet = getReadyFileWithCueSheet(smallMediaObject.songData)
                if (fileWithCueSheet != null) {
                    return CueFile.Builder()
                            .setFile(fileWithCueSheet)
                            .setSongFilePath(smallMediaObject.songData)
                            .setCoverArtPath(smallMediaObject.coverArtPath)
                            .setDurationInMillis(smallMediaObject.durationInMillis)
                            .build()
                }
            }
            return null
        }

        @JvmStatic
        private fun getReadyFileWithCueSheet(songData: String): File? {
            val fileData = File(songData)
            val listFiles: Array<File>? = fileData.parentFile.listFiles()
            var cueFile: File? = null
            if (listFiles != null)
                for (file in listFiles) {
                    if (file.absolutePath != songData && file.extension == fileData.extension)
                        return null
                    if (file.extension == "cue")
                        cueFile = file
                }
            if (cueFile == null)
                cueFile = File(songData)
            return cueFile
        }
    }

    /**
     * @return true if [file]'s [File.getAbsolutePath] method equals [songFilePath].
     */
    fun cueFileEqualsSongFilePath() = file.absolutePath == songFilePath

    /**
     * A builder class for the [CueFile] class.
     *
     * @author Danny Damsky
     */
    class Builder {
        private lateinit var file: File
        private lateinit var songFilePath: String
        private var coverArtPath: String? = null
        private var durationInMillis: Int = 0

        /**
         * @param file the file that contains the cue sheet.
         */
        fun setFile(file: File) = apply {
            this.file = file
        }

        /**
         * @param songFilePath the path to the audio file.
         */
        fun setSongFilePath(songFilePath: String) = apply {
            this.songFilePath = songFilePath
        }

        /**
         * @param coverArtPath the path to the audio file's cover art image.
         */
        fun setCoverArtPath(coverArtPath: String?) = apply {
            this.coverArtPath = coverArtPath
        }

        /**
         * @param durationInMillis the total duration of the audio file in milliseconds.
         */
        fun setDurationInMillis(durationInMillis: Int) = apply {
            this.durationInMillis = durationInMillis
        }

        /**
         * @return a complete [CueFile] object.
         */
        fun build() = CueFile(file, songFilePath, coverArtPath, durationInMillis)

        override fun toString() =
                "Builder(file=$file, songFilePath='$songFilePath', coverArtPath='$coverArtPath', durationInMillis=$durationInMillis)"

    }
}