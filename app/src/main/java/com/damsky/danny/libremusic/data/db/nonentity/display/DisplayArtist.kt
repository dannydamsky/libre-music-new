package com.damsky.danny.libremusic.data.db.nonentity.display

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import com.damsky.danny.libremusic.data.db.RoomDbConstants
import com.damsky.danny.libremusic.data.db.entity.Artist

/**
 * A class representing the [Artist] entity alongside the amount of songs and albums in it.
 *
 * @author Danny Damsky
 *
 * @param artist an object representing a row in the artists table.
 * @param albumsCount the amount of albums that are available to the [artist].
 * @param songsCount the amount of songs that are available to the [artist].
 */
data class DisplayArtist(
        @Embedded
        val artist: Artist,
        @ColumnInfo(name = RoomDbConstants.COUNT_ALBUMS)
        val albumsCount: Int,
        @ColumnInfo(name = RoomDbConstants.COUNT_SONGS)
        val songsCount: Int
)
