package com.damsky.danny.libremusic.util.loader

import com.damsky.danny.libremusic.data.db.entity.Album
import com.damsky.danny.libremusic.data.db.entity.Artist
import com.damsky.danny.libremusic.data.db.entity.Genre
import com.damsky.danny.libremusic.data.db.entity.Song

/**
 * A more complete media object than [SmallMediaObject].
 *
 * This class contains enough information to build any of the following database entities:
 * [Artist], [Album], [Song] and [Genre].
 *
 * @author Danny Damsky
 */
data class BigMediaObject(
        private val artistName: String,
        private val albumName: String,
        private val yearPublished: Int,
        private val genreName: String,
        private val songName: String,
        private val songTrackNumber: Int,
        private val smallMediaObject: SmallMediaObject
) {
    constructor(songLoader: SongLoader, smallMediaObject: SmallMediaObject) :
            this(songLoader.getArtistName(), songLoader.getAlbumName(),
                    songLoader.getYearPublished(), songLoader.getGenreName(),
                    songLoader.getSongName(), songLoader.getTrackNumber(), smallMediaObject)

    /**
     * Builds an [Artist] database entity.
     * @return A complete [Artist] object that can be inserted to the database.
     */
    fun buildArtist() = Artist.Builder()
            .setName(artistName)
            .build()

    /**
     * Builds an [Album] database entity.
     * @return A complete [Album] object that can be inserted to the database.
     */
    fun buildAlbum() = Album.Builder()
            .setName(albumName)
            .setArtistName(artistName)
            .setCoverData(smallMediaObject.coverArtPath)
            .setYearPublished(yearPublished)
            .build()

    /**
     * Builds an [Genre] database entity.
     * @return A complete [Genre] object that can be inserted to the database.
     */
    fun buildGenre() = Genre.Builder()
            .setName(genreName)
            .build()

    /**
     * Builds an [Song] database entity.
     * @return A complete [Song] object that can be inserted to the database.
     */
    fun buildSong() = Song.Builder()
            .setSongData(smallMediaObject.songData)
            .setName(songName)
            .setAlbumName(albumName)
            .setArtistName(artistName)
            .setGenreName(genreName)
            .setTrackNumber(songTrackNumber)
            .setYearPublished(yearPublished)
            .setEndTimeInMillis(smallMediaObject.durationInMillis)
            .setDurationInMillis(smallMediaObject.durationInMillis)
            .setCoverData(smallMediaObject.coverArtPath)
            .build()
}