package com.damsky.danny.libremusic.util

import android.net.Uri
import android.os.Build

/**
 * The application-wide constants are stored in this class.
 *
 * @author Danny Damsky
 */
class Constants private constructor() {
    companion object {

        //******************************************************************//
        //**********************STRING**FUNCTIONS**************************//
        //****************************************************************//

        /**
         * This is the default encoding charset used for the cue file parser class.
         * It is used when the user hasn't declared a preference for a different encoding charset.
         */
        const val DEFAULT_ENCODING = "Cp1251"

        //******************************************************************//
        //**********************INTENT**ACTIONS****************************//
        //****************************************************************//

        /**
         * This action opens the system equalizer on devices that support it.
         */
        const val ACTION_OPEN_EQUALIZER = "android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL"

        //******************************************************************//
        //**********************RUNTIME**PARAMETERS************************//
        //****************************************************************//

        /**
         * This runtime permission indicates whether the device is running Oreo or above or not
         * (API > [Build.VERSION_CODES.O])
         */
        @JvmField
        val IS_OREO_OR_ABOVE = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O


        //******************************************************************//
        //****************PERFORMANCE**TUNING**PARAMETERS******************//
        //****************************************************************//

        /**
         * Devices from this year onward are still fast.
         */
        const val DEVICE_YEAR_OPTIMAL = 2015

        /**
         * Devices from this year are showing their age but are still capable of running modern software.
         */
        private const val DEVICE_YEAR_MINIMAL = 2011

        /**
         * Defaulted to DEVICE_YEAR_MINIMAL, gets updated from the Application class.
         */
        @JvmField
        var DEVICE_YEAR = DEVICE_YEAR_MINIMAL

        //******************************************************************//
        //***************************URIs**********************************//
        //****************************************************************//

        /**
         * This [Uri] is used for opening the source code for this application.
         */
        @JvmField
        val URI_SOURCE_CODE = Uri.parse("https://bitbucket.org/dannydamsky/libre-music/src")

        /**
         * This [Uri] is used for mailing the author of this application.
         */
        @JvmField
        val URI_EMAIL = Uri.parse("mailto:dannydamsky99@gmail.com")

        /**
         * This [Uri] is used to access the Google Play Store page of this application through
         * the Google Play Store application.
         */
        @JvmField
        val URI_STORE = Uri.parse("market://details?id=com.damsky.danny.libremusic")

        /**
         * This [Uri] is used to access the Google Play Store page on the web, it is only used
         * when the Google Play Store application is not available on the device.
         */
        @JvmField
        val URI_STORE_BACKUP = Uri.parse("https://play.google.com/store/apps/details?id=com.damsky.danny.libremusic")

        //******************************************************************//
        //**********************REQUEST**CODES*****************************//
        //****************************************************************//

        /**
         * This request code gets returned when an Intent is launched requesting access to permissions.
         */
        const val REQUEST_CODE_PERMISSIONS = 422

        /**
         * This request code gets returned when an Intent is launched requesting permission to
         * modify system settings.
         */
        const val REQUEST_CODE_MODIFY_SYSTEM_PERMISSIONS = 383

        //******************************************************************//
        //**********************INTENT**EXTRAS*****************************//
        //****************************************************************//

        /**
         * This extra is passed to an activity's Intent to save the ID of the song that will
         * be set as a ringtone.
         */
        const val EXTRA_RINGTONE_SONG_ID = "extra_ringtone_song_id"
    }
}
