package com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.addtoplaylist

import android.arch.lifecycle.ViewModelProviders
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.crashlytics.android.Crashlytics
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.RoomDbViewModel
import com.damsky.danny.libremusic.data.db.dao.SongsPlaylistsLinkDao
import com.damsky.danny.libremusic.data.db.entity.SongsPlaylistsLink
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayPlaylist
import com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.CreatePlaylistDialogFragment
import com.damsky.danny.libremusic.util.CrashUtils
import kotlinx.android.synthetic.main.dialog_add_to_playlist.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

/**
 * This [DialogFragment] class inflates [R.layout.dialog_add_to_playlist].
 *
 * To show it, use the [AddToPlaylistDialogFragment.Companion.show] function.
 * The dialog handles the adding of the songs to the playlists independently
 * and doesn't have any callbacks to the activity.
 *
 * @author Danny Damsky
 */
class AddToPlaylistDialogFragment : DialogFragment() {

    companion object {
        private const val TAG =
                "com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.addtoplaylist.AddToPlaylistDialogFragment.TAG"

        private const val BUNDLE_PROPERTY_SONG_ID = "SONG_ID"
        private const val BUNDLE_VALUE_DEFAULT = -1L

        /**
         * Shows the [AddToPlaylistDialogFragment] using the activity's [FragmentManager].
         * This function will add/remove a single song that belongs to the given [songId] from
         * the available playlists.
         */
        @JvmStatic
        fun show(fragmentManager: FragmentManager, songId: Long) {
            val bundle = Bundle()
            bundle.putLong(BUNDLE_PROPERTY_SONG_ID, songId)
            showFragment(fragmentManager, bundle)
        }

        /**
         * Shows the [AddToPlaylistDialogFragment] using the activity's [FragmentManager].
         * This function will add/remove multiple songs that belong to the given [songIds] from
         * the available playlists.
         */
        @JvmStatic
        fun show(fragmentManager: FragmentManager, songIds: List<Long>) {
            val bundle = Bundle()
            bundle.putLongArray(BUNDLE_PROPERTY_SONG_ID, songIds.toLongArray())
            showFragment(fragmentManager, bundle)
        }

        @JvmStatic
        private fun showFragment(fragmentManager: FragmentManager, bundle: Bundle) {
            val fragment = AddToPlaylistDialogFragment()
            fragment.arguments = bundle
            fragment.show(fragmentManager, TAG)
        }
    }

    private lateinit var roomDbViewModel: RoomDbViewModel
    private lateinit var recyclerViewAdapter: AddToPlaylistDialogFragmentRecyclerViewAdapter

    private var songId: Long? = null
    private var songIds: List<Long>? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        try {
            setWindowProperties(dialog.window!!)
        } catch (e: NullPointerException) {
            CrashUtils.logException(e)
        } catch (e: Exception) {
            CrashUtils.logError(TAG, "Unexpected exception: ${e.message}")
        }
    }

    private fun setWindowProperties(window: Window) {
        window.setLayout((360 * resources.displayMetrics.density).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        window.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.transparent, null)))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        roomDbViewModel = ViewModelProviders.of(this).get(RoomDbViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dialog = inflater.inflate(R.layout.dialog_add_to_playlist, container, false)
        configureRecyclerView(dialog)
        setOnClickListeners(dialog)
        return dialog
    }

    private fun configureRecyclerView(dialog: View) {
        val songId = arguments!!.getLong(BUNDLE_PROPERTY_SONG_ID, BUNDLE_VALUE_DEFAULT)
        if (songId != BUNDLE_VALUE_DEFAULT) {
            this.songId = songId
            setRecyclerViewAdapterForSingleSong(dialog, songId)
        } else
            setRecyclerViewAdapterForMultipleSongs(dialog, arguments!!.getLongArray(BUNDLE_PROPERTY_SONG_ID)!!)
    }

    private fun setRecyclerViewAdapterForSingleSong(dialog: View, songId: Long) =
            setRecyclerViewAdapter(dialog, 1) {
                roomDbViewModel.playlistDao.getAllDisplayForSongById(songId)
            }

    private fun setRecyclerViewAdapterForMultipleSongs(dialog: View, songIds: LongArray) =
            setRecyclerViewAdapter(dialog, songIds.size) {
                this.songIds = songIds.toList()
                roomDbViewModel.playlistDao.getAllDisplayForSongsByIds(this.songIds!!)
            }

    private fun setRecyclerViewAdapter(dialog: View, songsCount: Int, function: () -> List<DisplayPlaylist>) {
        GlobalScope.launch(Dispatchers.Main) {
            val playlists = async(Dispatchers.IO) { function() }
            setRecyclerViewProperties(dialog)
            recyclerViewAdapter = AddToPlaylistDialogFragmentRecyclerViewAdapter(playlists.await(), songsCount)
            dialog.recyclerView.adapter = recyclerViewAdapter
        }
    }

    private fun setRecyclerViewProperties(dialog: View) {
        dialog.recyclerView.setHasFixedSize(true)
        dialog.recyclerView.itemAnimator = null
        dialog.recyclerView.layoutManager = LinearLayoutManager(dialog.context)
    }

    private fun setOnClickListeners(dialog: View) {
        setPositiveButtonOnClickListener(dialog)
        setNegativeButtonOnClickListener(dialog)
        setNeutralButtonOnClickListener(dialog)
    }

    private fun setPositiveButtonOnClickListener(dialog: View) =
            dialog.positiveButton.setOnClickListener {
                onPositiveButtonClicked(songId, songIds, recyclerViewAdapter.getSelectedIds(),
                        recyclerViewAdapter.getUnselectedIds())
                dismiss()
            }

    private fun onPositiveButtonClicked(songId: Long?, songIds: List<Long>?, selectedIds: Set<Long>, unselectedIds: Set<Long>) {
        GlobalScope.launch(Dispatchers.IO) {
            when {
                songId != null -> selectSingleSongWithPlaylists(songId, selectedIds, unselectedIds)
                songIds != null -> selectMultipleSongWithPlaylists(songIds, selectedIds, unselectedIds)
                else -> Crashlytics.log(Log.ERROR,
                        "AddToPlaylistDialogFragment.onPositiveButtonClicked",
                        "Both songId and songIds are null")
            }
        }
    }

    private fun selectSingleSongWithPlaylists(songId: Long, selectedIds: Set<Long>, unselectedIds: Set<Long>) {
        roomDbViewModel.songsPlaylistsLinkDao.deletePlaylistsFromSong(songId, unselectedIds)
        roomDbViewModel.roomDb.runInTransaction {
            selectedIds.forEach {
                insertSongPlaylistLink(roomDbViewModel.songsPlaylistsLinkDao, songId, it)
            }
        }
    }

    private fun selectMultipleSongWithPlaylists(songIds: List<Long>, selectedIds: Set<Long>, unselectedIds: Set<Long>) {
        roomDbViewModel.songsPlaylistsLinkDao.deletePlaylistsFromSongs(songIds, unselectedIds)
        roomDbViewModel.roomDb.runInTransaction {
            songIds.forEach { songId ->
                selectedIds.forEach { selectedId ->
                    insertSongPlaylistLink(roomDbViewModel.songsPlaylistsLinkDao, songId, selectedId)
                }
            }
        }
    }

    private fun insertSongPlaylistLink(songPlaylistLinkDao: SongsPlaylistsLinkDao, songId: Long, playlistId: Long) =
            songPlaylistLinkDao.insert(
                    SongsPlaylistsLink.Builder()
                            .setSongId(songId)
                            .setPlaylistId(playlistId)
                            .build())

    private fun setNegativeButtonOnClickListener(dialog: View) =
            dialog.negativeButton.setOnClickListener { dismiss() }

    private fun setNeutralButtonOnClickListener(dialog: View) =
            dialog.neutralButton.setOnClickListener {
                CreatePlaylistDialogFragment.show(fragmentManager!!)
                dismiss()
            }
}