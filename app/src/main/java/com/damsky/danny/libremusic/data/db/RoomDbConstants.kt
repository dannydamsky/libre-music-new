package com.damsky.danny.libremusic.data.db

/**
 * This is a utility class containing all of the constants that are used throughout the database.
 *
 * @author Danny Damsky
 */
class RoomDbConstants private constructor() {
    companion object {
        /**
         * The name of the database file.
         */
        const val DB_NAME = "libre-music.db"

        /**
         * The current version of the database.
         */
        const val DB_VERSION = 1

        /**
         * The name of the table representing songs in the database.
         */
        const val TABLE_NAME_SONGS = "SONGS"

        /**
         * A column from the songs table representing the ID of the song.
         */
        const val COLUMN_SONG_ID = "SONG_ID"

        /**
         * A column from the songs table representing the data of the song.
         */
        const val COLUMN_SONG_DATA = "SONG_DATA"

        /**
         * A column from the songs table representing the name of the song.
         */
        const val COLUMN_SONG_NAME = "SONG_NAME"

        /**
         * A column from the songs table representing the name of the song's album.
         */
        const val COLUMN_SONG_ALBUM_NAME = "SONG_ALBUM_NAME"

        /**
         * A column from the songs table representing the name of the song's artist.
         */
        const val COLUMN_SONG_ARTIST_NAME = "SONG_ARTIST_NAME"

        /**
         * A column from the songs table representing the name of the song's genre.
         */
        const val COLUMN_SONG_GENRE_NAME = "SONG_GENRE_NAME"

        /**
         * A column from the songs table representing the song's track number.
         */
        const val COLUMN_SONG_TRACK_NUMBER = "SONG_TRACK_NUMBER"

        /**
         * A column from the songs table representing the song's release date.
         */
        const val COLUMN_SONG_YEAR_PUBLISHED = "SONG_YEAR_PUBLISHED"

        /**
         * A column from the songs table representing the song's start time in milliseconds with respect
         * to the file that it's stored in.
         */
        const val COLUMN_SONG_START_TIME_IN_MILLIS = "SONG_START_TIME_IN_MILLIS"

        /**
         * A column from the songs table representing the song's end time in milliseconds with respect
         * to the file that it's stored in.
         */
        const val COLUMN_SONG_END_TIME_IN_MILLIS = "SONG_END_TIME_IN_MILLIS"

        /**
         * A column from the songs table representing the song's duration in milliseconds.
         */
        const val COLUMN_SONG_DURATION_IN_MILLIS = "SONG_DURATION_IN_MILLIS"

        /**
         * A column from the songs table representing the song's cover art image path.
         */
        const val COLUMN_SONG_COVER_DATA = "SONG_COVER_DATA"

        /**
         * The name of the table representing albums in the database.
         */
        const val TABLE_NAME_ALBUMS = "ALBUMS"

        /**
         * A column from the albums table representing the ID of the album.
         */
        const val COLUMN_ALBUM_ID = "ALBUM_ID"

        /**
         * A column from the albums table representing the name of the album.
         */
        const val COLUMN_ALBUM_NAME = "ALBUM_NAME"

        /**
         * A column from the albums table representing the album's artist's name.
         */
        const val COLUMN_ALBUM_ARTIST_NAME = "ALBUM_ARTIST_NAME"

        /**
         * A column from the albums table representing the album's release date.
         */
        const val COLUMN_ALBUM_YEAR_PUBLISHED = "ALBUM_YEAR_PUBLISHED"

        /**
         * A column from the albums table representing the album's cover art image path.
         */
        const val COLUMN_ALBUM_COVER_DATA = "ALBUM_COVER_DATA"

        /**
         * The name of the table representing artists in the database.
         */
        const val TABLE_NAME_ARTISTS = "ARTISTS"

        /**
         * A column from the artists table representing the ID of the artist.
         */
        const val COLUMN_ARTIST_ID = "ARTIST_ID"

        /**
         * A column from the artists table representing the name of the artist.
         */
        const val COLUMN_ARTIST_NAME = "ARTIST_NAME"

        /**
         * A column from the artists table representing the artist's cover art image path.
         */
        const val COLUMN_ARTIST_COVER_DATA = "ARTIST_COVER_DATA"

        /**
         * The name of the table representing playlists in the database.
         */
        const val TABLE_NAME_PLAYLISTS = "PLAYLISTS"

        /**
         * A column from the playlists table representing the ID of the playlist.
         */
        const val COLUMN_PLAYLIST_ID = "PLAYLIST_ID"

        /**
         * A column from the playlists table representing the name of the playlist.
         */
        const val COLUMN_PLAYLIST_NAME = "PLAYLIST_NAME"

        /**
         * A column from the playlists table representing the playlist's cover art image path.
         */
        const val COLUMN_PLAYLIST_COVER_DATA = "PLAYLIST_COVER_DATA"

        /**
         * The name of the table representing genres in the database.
         */
        const val TABLE_NAME_GENRES = "GENRES"

        /**
         * A column from the genres table representing the ID of the genre.
         */
        const val COLUMN_GENRE_ID = "GENRE_ID"

        /**
         * A column from the genres table representing the name of the genre.
         */
        const val COLUMN_GENRE_NAME = "GENRE_NAME"

        /**
         * A column from the genres table representing the genre's cover art image path.
         */
        const val COLUMN_GENRE_COVER_DATA = "GENRE_COVER_DATA"

        /**
         * The name of the table representing the link between songs and playlists in the database.
         */
        const val TABLE_NAME_SONGS_PLAYLISTS_LINKS = "SONGS_PLAYLISTS_LINKS"

        /**
         * A column from the table linking playlists to songs representing the song's ID.
         */
        const val COLUMN_SONGS_PLAYLISTS_LINK_SONG_ID = "SONGS_PLAYLISTS_LINK_SONG_ID"

        /**
         * A column from the table linking playlists to songs representing the playlist's ID.
         */
        const val COLUMN_SONGS_PLAYLISTS_LINK_PLAYLIST_ID = "SONGS_PLAYLISTS_LINK_PLAYLIST_ID"

        /**
         * The default value for a non-existing year entry in the database.
         */
        const val YEAR_PUBLISHED_NONE = 0

        /**
         * The default value for a non-existing track number entry in the database. (Should never happen)
         */
        const val TRACK_NUMBER_DEFAULT = 0

        /**
         * The default value for an unknown name in the database.
         */
        const val DEFAULT_NAME = "<unknown>"

        /**
         * The default name given for a custom value in the database that represents the amount of albums.
         */
        const val COUNT_ALBUMS = "ALBUMS_COUNT"

        /**
         * The default name given for a custom value in the database that represents the amount of songs.
         */
        const val COUNT_SONGS = "SONGS_COUNT"
    }
}