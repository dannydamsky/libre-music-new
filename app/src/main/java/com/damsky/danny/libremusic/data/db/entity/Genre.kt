package com.damsky.danny.libremusic.data.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import com.damsky.danny.libremusic.data.db.RoomDbConstants

/**
 * This class represents the genres table in the database.
 *
 * Relationships:
 * Genre 1-N Songs
 *
 * @author Danny Damsky
 *
 * @param id the ID of the genre (auto-increment).
 * @param name the name of the genre.
 * @param coverData the path to the genre's cover art image.
 */
@Entity(tableName = RoomDbConstants.TABLE_NAME_GENRES,
        indices = [Index(value = [RoomDbConstants.COLUMN_GENRE_NAME], unique = true)])
data class Genre(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = RoomDbConstants.COLUMN_GENRE_ID)
        val id: Long?,
        @ColumnInfo(name = RoomDbConstants.COLUMN_GENRE_NAME)
        val name: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_GENRE_COVER_DATA)
        val coverData: String?) {

    /**
     * A builder class for the [Genre] entity.
     *
     * @author Danny Damsky
     */
    class Builder {
        private var id: Long? = null
        private lateinit var name: String
        private var coverData: String? = null

        /**
         * @param id the ID of the genre.
         */
        fun setId(id: Long?) = apply {
            this.id = id
        }

        /**
         * @param name the name of the genre.
         */
        fun setName(name: String) = apply {
            this.name = name
        }

        /**
         * @param coverData the path to the genre's cover art image.
         */
        fun setCoverData(coverData: String?) = apply {
            this.coverData = coverData
        }

        /**
         * @return a new [Genre] object with the properties set in this class.
         */
        fun build() = Genre(id, name, coverData)

        override fun toString() = "Builder(id=$id, name='$name', coverData=$coverData)"
    }
}
