package com.damsky.danny.libremusic.data.db.nonentity.display

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import com.damsky.danny.libremusic.data.db.RoomDbConstants
import com.damsky.danny.libremusic.data.db.entity.Genre

/**
 * A class representing the [Genre] entity alongside the amount of songs in it.
 *
 * @author Danny Damsky
 *
 * @param genre an object representing a row in the genres table.
 * @param songsCount the amount of songs that are available to the [genre].
 */
data class DisplayGenre(
        @Embedded
        val genre: Genre,
        @ColumnInfo(name = RoomDbConstants.COUNT_SONGS)
        val songsCount: Int
)