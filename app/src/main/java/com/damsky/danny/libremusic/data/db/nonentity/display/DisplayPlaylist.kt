package com.damsky.danny.libremusic.data.db.nonentity.display

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import com.damsky.danny.libremusic.data.db.RoomDbConstants
import com.damsky.danny.libremusic.data.db.entity.Playlist

/**
 * A class representing the [Playlist] entity alongside the amount of songs in it.
 *
 * @author Danny Damsky
 *
 * @param playlist an object representing a row in the playlists table.
 * @param songsCount the amount of songs that are available to the [playlist].
 */
data class DisplayPlaylist(
        @Embedded
        val playlist: Playlist,
        @ColumnInfo(name = RoomDbConstants.COUNT_SONGS)
        val songsCount: Int
)