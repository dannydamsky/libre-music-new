package com.damsky.danny.libremusic

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.damsky.danny.libremusic.util.Constants
import com.facebook.device.yearclass.YearClass
import io.fabric.sdk.android.Fabric

/**
 * This is the application class, the use of this class in Libre Music is to initialise
 * all required application-level constants.
 *
 * @author Danny Damsky
 */
class LibreMusicApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Fabric.with(this, Crashlytics())
        Constants.DEVICE_YEAR = YearClass.get(this)
    }
}
