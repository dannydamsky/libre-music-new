package com.damsky.danny.libremusic.data.db.nonentity.sorting

import android.arch.lifecycle.LiveData
import com.damsky.danny.libremusic.data.db.dao.GenreDao
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayGenre

/**
 * The values of this enum class represent the available sorting types for lists representing
 * the genre entity.
 *
 * @author Danny Damsky
 */
enum class GenreSorting {
    NONE, NAME_ASC, NAME_DESC;

    /**
     * @param genreDao the DAO (Data Access Object) for working with the genres table.
     *
     * @return the appropriate [LiveData] object of a [List] of [DisplayGenre] according to the
     * sorting enum value.
     */
    fun getDisplayGenresLiveData(genreDao: GenreDao) = when (this) {
        NONE -> genreDao.getAllDisplayLive()
        NAME_ASC -> genreDao.getAllDisplayOrderedByNameAscLive()
        NAME_DESC -> genreDao.getAllDisplayOrderedByNameDescLive()
    }
}