package com.damsky.danny.libremusic.data.db.nonentity.rowitem.song

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplaySong
import com.damsky.danny.libremusic.util.PopupMenuBuilder
import com.damsky.danny.libremusic.util.RowItemMenuAction

/**
 * An implementation of [AbstractSongRowItem] meant to work for most songs.
 *
 * @author Danny Damsky
 * @see AbstractSongRowItem
 *
 * @param song the entity object for this row item.
 */
class SongRowItem private constructor(song: DisplaySong) : AbstractSongRowItem(song) {

    companion object {
        /**
         * @param dbLiveData the [LiveData] object to transform into a [LiveData] of a [List] of
         * [SongRowItem]s.
         * @return a [LiveData] of a [List] of [SongRowItem]s which are related to the given
         * [dbLiveData].
         */
        @JvmStatic
        fun getLiveData(dbLiveData: LiveData<List<DisplaySong>>) = Transformations.switchMap(dbLiveData) { input ->
            val songModelList = input.map { SongRowItem(it) }
            val liveData = MutableLiveData<List<AbstractSongRowItem>>()
            liveData.postValue(songModelList)
            liveData
        }!!
    }

    override fun buildPopupMenuBuilder() = PopupMenuBuilder<DisplaySong>()
            .inflate(R.menu.menu_row_item_song)
            .setItem(this.song)
            .setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.playSongs -> RowItemMenuAction.ACTION_PLAY
                    R.id.addToQueue -> RowItemMenuAction.ACTION_ADD_TO_QUEUE
                    R.id.addSongsToPlaylist -> RowItemMenuAction.ACTION_ADD_TO_PLAYLIST
                    R.id.setAsRingtone -> RowItemMenuAction.ACTION_SET_AS_RINGTONE
                    R.id.shareSongs -> RowItemMenuAction.ACTION_SHARE
                    else -> RowItemMenuAction.ACTION_PLAY
                }
            }
}