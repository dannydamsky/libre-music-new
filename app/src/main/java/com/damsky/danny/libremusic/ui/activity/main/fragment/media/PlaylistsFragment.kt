package com.damsky.danny.libremusic.ui.activity.main.fragment.media

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.entity.Playlist
import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplaySong
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.PlaylistRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.AbstractSongRowItem
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapter
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks
import com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.RemovePlaylistDialogFragment
import com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.RenamePlaylistDialogFragment
import com.damsky.danny.libremusic.util.CrashUtils
import com.damsky.danny.libremusic.util.IntentUtils
import com.damsky.danny.libremusic.util.RowItemMenuAction
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * An implementation of [AbstractMediaFragment].
 * This implementation displays a list of [Playlist]s and their appropriate songs.
 *
 * @author Danny Damsky
 */
class PlaylistsFragment : AbstractMediaFragment() {

    private val playlistCallbacks = object : MainActivityRecyclerViewAdapterCallbacks<Playlist> {
        override fun onRowClick(item: Playlist, itemPosition: Int) =
                onPlaylistCallbacksActionViewAllSongs(itemPosition, item.id!!)

        override fun onRowMenuClick(item: Playlist, itemPosition: Int, menuAction: RowItemMenuAction) = when (menuAction) {
            RowItemMenuAction.ACTION_PLAY -> callOnPlaylistCallbacksActionPlay(item.id!!)
            RowItemMenuAction.ACTION_ADD_TO_QUEUE -> callOnPlaylistCallbacksAddToQueue(item.id!!)
            RowItemMenuAction.ACTION_VIEW_ALL_SONGS -> onPlaylistCallbacksActionViewAllSongs(itemPosition, item.id!!)
            RowItemMenuAction.ACTION_SHARE -> callOnPlaylistCallbacksShare(item.id!!)
            RowItemMenuAction.ACTION_RENAME_PLAYLIST ->
                RenamePlaylistDialogFragment.show(mainActivity.supportFragmentManager, item)

            RowItemMenuAction.ACTION_REMOVE_PLAYLIST ->
                RemovePlaylistDialogFragment.show(mainActivity.supportFragmentManager, item.id!!)

            RowItemMenuAction.ACTION_REMOVE_FROM_QUEUE,
            RowItemMenuAction.ACTION_ADD_TO_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_FROM_PLAYLIST,
            RowItemMenuAction.ACTION_SET_AS_RINGTONE ->
                onPlaylistCallbacksInvalidAction(item, itemPosition, menuAction)
        }
    }

    private fun onPlaylistCallbacksActionViewAllSongs(itemPosition: Int, playlistId: Long) {
        lastKnownPlaylistId = playlistId
        setSongsAdapterFromPlaylists(playlistId, itemPosition)
    }

    private fun callOnPlaylistCallbacksActionPlay(playlistId: Long) =
            onSongCallbacksActionPlay(0) { songSorting ->
                songSorting.getPlayableSongsForPlaylist(roomDbViewModel.songsPlaylistsLinkDao,
                        playlistId) as ArrayList<PlayableSong>
            }

    private fun callOnPlaylistCallbacksAddToQueue(playlistId: Long) =
            onOtherCallbacksAddToQueue { songSorting ->
                songSorting.getPlayableSongsForPlaylist(roomDbViewModel.songsPlaylistsLinkDao,
                        playlistId) as ArrayList<PlayableSong>
            }

    private fun callOnPlaylistCallbacksShare(playlistId: Long) =
            IntentUtils.shareMultipleSongs(this.mainActivity) {
                roomDbViewModel.songsPlaylistsLinkDao.getSongsDataForPlaylist(playlistId)
            }

    private fun onPlaylistCallbacksInvalidAction(item: Playlist, itemPosition: Int, menuAction: RowItemMenuAction) {
        mainActivity.showSnackBarLong(R.string.error_invalid_menu_action)
        CrashUtils.logError("PlaylistsFragment.playlistCallbacks.onRowMenuClick",
                "INVALID MENU ACTION: menuAction=$menuAction, item=$item, itemPosition=$itemPosition")
    }

    override val songCallbacks = object : MainActivityRecyclerViewAdapterCallbacks<DisplaySong> {
        override fun onRowClick(item: DisplaySong, itemPosition: Int) =
                callOnSongCallbacksActionPlay(itemPosition)

        override fun onRowMenuClick(item: DisplaySong, itemPosition: Int, menuAction: RowItemMenuAction) = when (menuAction) {
            RowItemMenuAction.ACTION_PLAY -> callOnSongCallbacksActionPlay(itemPosition)
            RowItemMenuAction.ACTION_ADD_TO_QUEUE -> onSongCallbacksActionAddToQueue(item.id!!)
            RowItemMenuAction.ACTION_ADD_TO_PLAYLIST -> onSongCallbacksActionAddToPlaylist(item.id!!)
            RowItemMenuAction.ACTION_SHARE -> onSongCallbacksActionShare(item.id!!)
            RowItemMenuAction.ACTION_SET_AS_RINGTONE -> onSongCallbacksActionSetAsRingtone(item)
            RowItemMenuAction.ACTION_REMOVE_FROM_PLAYLIST -> onSongCallbacksRemoveFromPlaylist(item.id!!)
            RowItemMenuAction.ACTION_REMOVE_FROM_QUEUE,
            RowItemMenuAction.ACTION_VIEW_ALL_SONGS,
            RowItemMenuAction.ACTION_RENAME_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_PLAYLIST ->
                onSongCallbacksInvalidAction(item, itemPosition, menuAction)
        }
    }

    private fun callOnSongCallbacksActionPlay(itemPosition: Int) =
            onSongCallbacksActionPlay(itemPosition) { songSorting ->
                songSorting.getPlayableSongsForPlaylist(roomDbViewModel.songsPlaylistsLinkDao,
                        lastKnownPlaylistId) as ArrayList<PlayableSong>
            }

    private fun onSongCallbacksRemoveFromPlaylist(songId: Long) {
        val playlistId = lastKnownPlaylistId
        GlobalScope.launch(Dispatchers.IO) {
            roomDbViewModel.songsPlaylistsLinkDao.deleteSongFromPlaylist(songId, playlistId)
        }
    }

    private val playlistsAdapterObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            super.onItemRangeInserted(positionStart, itemCount)
            if (itemCount != 0 && lastScrollPositionPlaylists != -1) {
                recyclerView.scrollToPosition(lastScrollPositionPlaylists)
                lastScrollPositionPlaylists = -1
            }
        }
    }

    private val songsAdapterObserver = object : RecyclerView.AdapterDataObserver() {

    }

    private var lastKnownPlaylistId = -1L
    private lateinit var playlistsAdapter: MainActivityRecyclerViewAdapter<Playlist, PlaylistRowItem>
    private lateinit var songsAdapter: MainActivityRecyclerViewAdapter<DisplaySong, AbstractSongRowItem>

    private var lastScrollPositionPlaylists = -1

    override fun onViewStubInflated() {
        initAdapters()
        initRecyclerViewProperties()
        setPlaylistsAdapter()
    }

    private fun initAdapters() {
        initPlaylistsAdapter()
        initSongsAdapter()
    }

    private fun initPlaylistsAdapter() {
        playlistsAdapter = MainActivityRecyclerViewAdapter(PlaylistRowItem.DIFF_CALLBACK, playlistCallbacks)
        playlistsAdapter.setLiveData(roomDbViewModel.playlistRowItemsLiveData)
    }

    private fun initSongsAdapter() {
        songsAdapter = MainActivityRecyclerViewAdapter(AbstractSongRowItem.DIFF_CALLBACK, songCallbacks)
    }

    private fun initRecyclerViewProperties() {
        recyclerView.layoutManager = LinearLayoutManager(mainActivity)
        recyclerView.setHasFixedSize(true)
    }

    private fun setPlaylistsAdapter() {
        playlistsAdapter.registerAdapterDataObserver(playlistsAdapterObserver)
        playlistsAdapter.startObserving(this)
        recyclerView.adapter = playlistsAdapter
    }

    private fun setSongsAdapterFromPlaylists(playlistId: Long, lastScrollPositionPlaylists: Int) {
        playlistsAdapter.unregisterAdapterDataObserver(playlistsAdapterObserver)
        val liveData = roomDbViewModel.getSongRowItemsLiveDataForPlaylist(playlistId)
        songsAdapter.setLiveData(liveData)
        songsAdapter.registerAdapterDataObserver(songsAdapterObserver)
        songsAdapter.startObserving(this)
        recyclerView.adapter = songsAdapter
        playlistsAdapter.stopObserving()
        playlistsAdapter.submitList(null)
        this.lastScrollPositionPlaylists = lastScrollPositionPlaylists
    }

    override fun onBackPressed() = if (lastScrollPositionPlaylists != -1) {
        setPlaylistsAdapterFromSongs()
        false
    } else
        true

    private fun setPlaylistsAdapterFromSongs() {
        songsAdapter.unregisterAdapterDataObserver(songsAdapterObserver)
        playlistsAdapter.registerAdapterDataObserver(playlistsAdapterObserver)
        playlistsAdapter.startObserving(this)
        recyclerView.adapter = playlistsAdapter
        songsAdapter.stopObserving()
        songsAdapter.submitList(null)
    }

    override fun reset() = when {
        lastScrollPositionPlaylists != -1 -> {
            lastScrollPositionPlaylists = -1
            setPlaylistsAdapterFromSongs()
        }
        else -> recyclerView.smoothScrollToPosition(0)
    }
}