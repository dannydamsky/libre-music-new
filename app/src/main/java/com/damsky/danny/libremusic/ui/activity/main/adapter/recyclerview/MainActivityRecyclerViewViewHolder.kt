package com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview

import android.support.v7.widget.RecyclerView
import android.view.View
import com.damsky.danny.libremusic.GlideApp
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.EntityRowItem
import kotlinx.android.synthetic.main.row_recyclerview_main.view.*

/**
 * The [RecyclerView.ViewHolder] for [MainActivityRecyclerViewAdapter].
 * The only available functions in this class are [bind] and [unbind], which are used for
 * inflating the layout and cleaning it up respectively.
 *
 * @param itemView the layout of a [RecyclerView]'s row.
 * @param callbacks the [RecyclerView]'s callbacks.
 *
 * @author Danny Damsky
 * @see MainActivityRecyclerViewAdapter
 * @see MainActivityRecyclerViewAdapterCallbacks
 */
class MainActivityRecyclerViewViewHolder<M, T : EntityRowItem<M>>(
        itemView: View, private val callbacks: MainActivityRecyclerViewAdapterCallbacks<M>
) : RecyclerView.ViewHolder(itemView) {

    /**
     * Inflates the [RecyclerView]'s row with the information from [item].
     *
     * @param item the item at [position].
     * @param position the position of the [item].
     */
    fun bind(item: T, position: Int) {
        bindLayout(item, position)
        bindRowImage(item)
        bindMainText(item)
        bindSecondaryText(item)
        bindTertiaryText(item)
        bindRowMenu(item, position)
    }

    private fun bindLayout(item: T, position: Int) = itemView.rowLayout.setOnClickListener {
        callbacks.onRowClick(item.getObject(), position)
    }

    private fun bindRowImage(item: T) {
        val coverData = item.getCoverData()
        val placeholderImage = item.getPlaceHolderImage()
        if (coverData != null)
            loadProperImage(coverData, placeholderImage)
        else
            loadPlaceholderImage(placeholderImage)
    }

    private fun loadProperImage(coverData: String, placeholderImage: Int) {
        GlideApp.with(itemView)
                .load(coverData)
                .circleCrop()
                .placeholder(placeholderImage)
                .into(itemView.rowImage)
    }

    private fun loadPlaceholderImage(placeholderImage: Int) {
        GlideApp.with(itemView)
                .load(placeholderImage)
                .into(itemView.rowImage)
    }

    private fun bindMainText(item: T) {
        itemView.rowMainText.text = item.getMainText()
    }

    private fun bindSecondaryText(item: T) {
        itemView.rowSecondaryText.text = item.getSecondaryText()
    }

    private fun bindTertiaryText(item: T) {
        itemView.rowTertiaryText.text = item.getTertiaryText()
    }

    private fun bindRowMenu(item: T, position: Int) = itemView.rowMenu.setOnClickListener {
        val popupMenu = item.buildPopupMenu(itemView.context, itemView.rowMenu, position, callbacks)
        popupMenu.show()
    }

    /**
     * Cleans up the [RecyclerView]'s row. This function should be called when the view gets
     * recycled.
     */
    fun unbind() {
        itemView.rowLayout.setOnClickListener(null)
        GlideApp.with(itemView).clear(itemView.rowImage)
        itemView.rowImage.setImageDrawable(null)
        itemView.rowMenu.setOnClickListener(null)
    }
}