package com.damsky.danny.libremusic.util

import android.support.v4.app.NotificationCompat

/**
 * This class is meant to be a more efficient way to calculate percentage.
 *
 * @author Danny Damsky
 * @constructor Takes the maximum value and initialises the class' properties.
 * @property maxValue a value that represents 100%.
 * @property onePercentValue a constant that is equal to 1% of [maxValue].
 * @property targetValue a variable target that gets upped by [onePercentValue] every time.
 * @property currentValue the current progress, this value gets incremented by one by running the [increment] function.
 */
class ProgressToPercentConverter(private val maxValue: Int) {

    private val onePercentValue = maxValue * 0.01

    private var targetValue = onePercentValue

    private var currentValue = 0
    private var currentPercent = 0

    /**
     * Increments [currentValue] by one, checks if it's equal to [targetValue], and if it is then
     * [currentPercent] is incremented by one and [targetValue] is incremented by [onePercentValue].
     */
    fun increment() {
        currentValue++
        if (currentValue == targetValue.toInt()) {
            currentPercent++
            targetValue += onePercentValue
        }
    }

    /**
     * Sets relevant properties to the given [notificationCompatBuilder] in order to display the
     * progress.
     *
     * @param notificationCompatBuilder the notification builder to be used.
     */
    fun fillNotificationBuilderProperties(notificationCompatBuilder: NotificationCompat.Builder) {
        notificationCompatBuilder
                .setOngoing(true)
                .setProgress(maxValue, currentValue, false)
                .setContentText(toString())
    }

    /**
     * @return the percentage of progress made so-far.
     */
    override fun toString() = "$currentPercent%"
}