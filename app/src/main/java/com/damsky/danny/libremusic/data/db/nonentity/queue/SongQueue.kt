package com.damsky.danny.libremusic.data.db.nonentity.queue

import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnPlaybackInfoChangedListener
import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnQueueChangedListener
import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnSongChangedListener
import com.damsky.danny.libremusic.data.db.nonentity.queue.manager.SongQueueCallbacksManager
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.QueueSongRowItem
import com.damsky.danny.libremusic.data.prefs.SharedPreferencesHelper
import kotlinx.coroutines.*

class SongQueue(private val sharedPreferencesHelper: SharedPreferencesHelper) {

    private val songQueueCallbacksManager = SongQueueCallbacksManager()
    private val songQueueProperties = SongQueueProperties()

    /**
     * @param onSongChangedListener the new listener to register and send a first callback to.
     */
    fun registerOnSongChangedListener(onSongChangedListener: OnSongChangedListener) {
        songQueueCallbacksManager.registerOnSongChangedListener(onSongChangedListener)
        songQueueProperties.getCurrentSong()?.let {
            onSongChangedListener.onSongChanged(it,
                    songQueueProperties.currentSongIsPlaying(),
                    songQueueProperties.getCurrentSongIndex(),
                    songQueueProperties.getSongsTotal(),
                    songQueueProperties.getPlayerPositionMillis())
        }
    }

    /**
     * @param onSongChangedListener the listener to unregister.
     */
    fun unregisterOnSongChangedListener(onSongChangedListener: OnSongChangedListener) =
            songQueueCallbacksManager.unregisterOnSongChangedListener(onSongChangedListener)

    /**
     * @param onQueueChangedListener the new listener to register and send a first callback to.
     */
    fun registerOnQueueChangedListener(onQueueChangedListener: OnQueueChangedListener) {
        songQueueCallbacksManager.registerOnQueueChangedListener(onQueueChangedListener)
        songQueueProperties.getPlayableSongs()?.let {
            GlobalScope.launch(Dispatchers.Main) {
                val queueSongs = withContext(Dispatchers.IO) { QueueSongRowItem.get(it) }
                if (onQueueChangedListener.isRegistered) {
                    onQueueChangedListener.onQueueChanged(queueSongs)
                }
            }
        }
    }

    /**
     * @param onQueueChangedListener the listener to unregister.
     */
    fun unregisterOnQueueChangedListener(onQueueChangedListener: OnQueueChangedListener) =
            songQueueCallbacksManager.unregisterOnQueueChangedListener(onQueueChangedListener)

    /**
     * @param onPlaybackInfoChangedListener the new listener to register.
     */
    fun registerOnPlaybackInfoChangedListener(onPlaybackInfoChangedListener: OnPlaybackInfoChangedListener) =
            songQueueCallbacksManager.registerOnPlaybackInfoChangedListener(onPlaybackInfoChangedListener)

    /**
     * @param onPlaybackInfoChangedListener the listener to unregister.
     */
    fun unregisterOnPlaybackInfoChangedListener(onPlaybackInfoChangedListener: OnPlaybackInfoChangedListener) =
            songQueueCallbacksManager.unregisterOnPlaybackInfoChangedListener(onPlaybackInfoChangedListener)

    /**
     * Sets the current song as playing and notifies all relevant listeners.
     */
    fun setCurrentSongIsPlaying() {
        songQueueProperties.setCurrentSongIsPlaying()
        songQueueCallbacksManager.notifyOnPlaybackStarted()
    }

    /**
     * Sets the current song as not playing and notifies all relevant listeners.
     */
    fun setCurrentSongIsNotPlaying() {
        songQueueProperties.setCurrentSongIsNotPlaying()
        songQueueCallbacksManager.notifyOnPlaybackStopped()
    }

    /**
     * Sets a new media player seek position and notifies all relevant listeners.
     *
     * @param playerPositionMillis the new seek position of the media player.
     */
    fun setPlayerPositionMillis(playerPositionMillis: Int) {
        songQueueProperties.setPlayerPositionMillis(playerPositionMillis)
        songQueueCallbacksManager.notifyOnPlayerPositionChanged(playerPositionMillis)
    }

    /**
     * Modifies the queue with the new properties and notifies all relevant listeners.
     *
     * @param playableSongs the new songs to be set as the queue.
     * @param currentSongIndex the index of the currently selected song in the queue.
     */
    fun setSongs(playableSongs: ArrayList<PlayableSong>, currentSongIndex: Int = 0) {
        GlobalScope.launch(Dispatchers.Main) {
            val operationSetSongs = launch(Dispatchers.IO) {
                songQueueProperties.setSongs(playableSongs, currentSongIndex)
            }
            val queueSongs = async(Dispatchers.IO) { QueueSongRowItem.get(playableSongs) }
            songQueueCallbacksManager.notifyOnQueueResized(currentSongIndex, playableSongs.size)
            sharedPreferencesHelper.updateQueue(playableSongs)
            sharedPreferencesHelper.putLastKnownIndex(currentSongIndex)
            songQueueCallbacksManager.notifyOnQueueChanged(queueSongs.await())
            operationSetSongs.join()
            songQueueCallbacksManager.notifyOnSongChanged(songQueueProperties)
        }
    }

    /**
     * Selects a song in the queue at the given [currentSongIndex] and notifies all relevant listeners.
     *
     * @param currentSongIndex the index of the song in the queue.
     */
    fun setCurrentSongIndex(currentSongIndex: Int) {
        songQueueProperties.setCurrentSongIndex(currentSongIndex)
        songQueueCallbacksManager.notifyOnQueueResized(currentSongIndex, songQueueProperties.getSongsTotal())
        sharedPreferencesHelper.putLastKnownIndex(currentSongIndex)
        this.songQueueCallbacksManager.notifyOnSongChanged(songQueueProperties)
    }

    /**
     * @param playableSong the song to add to the end of the queue.
     */
    fun addSong(playableSong: PlayableSong) =
            addAnyAmountOfSongs { songQueueProperties.addSong(playableSong) }

    /**
     * @param playableSongs the songs to add to the end of the queue.
     */
    fun addSongs(playableSongs: ArrayList<PlayableSong>) =
            addAnyAmountOfSongs { songQueueProperties.addSongs(playableSongs) }

    private fun addAnyAmountOfSongs(operationAddSongs: () -> Boolean) {
        GlobalScope.launch(Dispatchers.Main) {
            if (withContext(Dispatchers.IO) { operationAddSongs() }) {
                val playableSongs = songQueueProperties.getPlayableSongs()
                val queueSongs = async(Dispatchers.IO) { QueueSongRowItem.get(playableSongs!!) }
                sharedPreferencesHelper.updateQueue(playableSongs!!)
                songQueueCallbacksManager.notifyOnQueueResized(
                        songQueueProperties.getCurrentSongIndex(), playableSongs.size)
                songQueueCallbacksManager.notifyOnQueueChanged(queueSongs.await())
            }
        }
    }

    /**
     * @return the current song that is selected in the queue.
     * @throws KotlinNullPointerException if the song is still null. Only use this if you're sure
     * that the song item has already been set.
     */
    fun getCurrentSong() = songQueueProperties.getCurrentSong()!!

    /**
     * Skips to the previous song in the queue and notifies the relevant listeners.
     */
    fun skipToPreviousSong() = skipToSong { songQueueProperties.skipToPreviousSong() }

    /**
     * Skips to the next song in the queue and notifies the relevant listeners.
     */
    fun skipToNextSong() = skipToSong { songQueueProperties.skipToNextSong() }

    /**
     * Skips to a random song in the queue and notifies the relevant listeners.
     */
    fun skipToRandomSong() = skipToSong { songQueueProperties.skipToRandomSong() }

    private fun skipToSong(skipOperation: () -> Unit) {
        skipOperation()
        this.songQueueCallbacksManager.notifyOnSongChanged(this.songQueueProperties)
        this.sharedPreferencesHelper.putLastKnownIndex(this.songQueueProperties.getCurrentSongIndex())
    }

    /**
     * Removes the song at the given [index] and notifies the listeners.
     *
     * @param index the position of the song to remove.
     * @return true if the removal succeeded, false otherwise.
     */
    fun removeSongAt(index: Int) = if (songQueueProperties.getSongsTotal() > 1) {
        songQueueProperties.removeSongAt(index)
        val playableSongs = songQueueProperties.getPlayableSongs()
        val currentSongIndex = songQueueProperties.getCurrentSongIndex()
        sharedPreferencesHelper.updateQueue(playableSongs!!)
        sharedPreferencesHelper.putLastKnownIndex(currentSongIndex)
        songQueueCallbacksManager.notifyOnSongChanged(songQueueProperties)
        songQueueCallbacksManager.notifyOnQueueResized(currentSongIndex, playableSongs.size)
        GlobalScope.launch(Dispatchers.Main) {
            val queueSongs = async(Dispatchers.IO) { QueueSongRowItem.get(playableSongs) }
            songQueueCallbacksManager.notifyOnQueueChanged(queueSongs.await())
        }
        true
    } else
        false

    override fun toString() =
            "SongQueue(songQueueCallbacksManager=$songQueueCallbacksManager, songQueueProperties=$songQueueProperties)"
}