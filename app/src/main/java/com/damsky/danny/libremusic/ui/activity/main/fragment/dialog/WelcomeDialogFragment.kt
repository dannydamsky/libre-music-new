package com.damsky.danny.libremusic.ui.activity.main.fragment.dialog

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.WelcomeDialogFragment.Listener
import com.damsky.danny.libremusic.util.CrashUtils
import kotlinx.android.synthetic.main.dialog_welcome.view.*

/**
 * This [DialogFragment] class inflates [R.layout.dialog_welcome].
 * It is shown as a "Welcome" screen when the user launches the application for the first time.
 * It requests the user to grant the application access to certain permissions that it requires.
 *
 * To show this interface call [WelcomeDialogFragment.Companion.show] with
 * your activity's [FragmentManager]. Make sure that your activity implements [Listener],
 * otherwise it will throw a [ClassCastException] in [onAttach].
 *
 * @author Danny Damsky
 */
class WelcomeDialogFragment : DialogFragment() {

    companion object {
        private const val TAG =
                "com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.WelcomeDialogFragment.TAG"

        /**
         * Shows the [WelcomeDialogFragment] using its own custom tag.
         *
         * @param fragmentManager the [FragmentManager] of the activity.
         */
        @JvmStatic
        fun show(fragmentManager: FragmentManager) =
                WelcomeDialogFragment().show(fragmentManager, TAG)
    }

    private lateinit var callbacks: Listener

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        callbacks = context as Listener
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog.setCancelable(false)
        try {
            setWindowProperties(dialog.window!!)
        } catch (e: NullPointerException) {
            CrashUtils.logException(e)
        } catch (e: Exception) {
            CrashUtils.logError(TAG, "Unexpected exception: ${e.message}")
        }
    }

    private fun setWindowProperties(window: Window) {
        window.setLayout((360 * resources.displayMetrics.density).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        window.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.transparent, null)))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dialog = inflater.inflate(R.layout.dialog_welcome, container, false)
        setPositiveButtonOnClickListener(dialog)
        setNegativeButtonOnClickListener(dialog)
        return dialog
    }

    private fun setPositiveButtonOnClickListener(dialog: View) =
            dialog.positiveButton.setOnClickListener {
                callbacks.onWelcomeDialogPositiveButtonPressed()
                dismiss()
            }

    private fun setNegativeButtonOnClickListener(dialog: View) =
            dialog.negativeButton.setOnClickListener {
                callbacks.onWelcomeDialogNegativeButtonPressed()
                dismiss()
            }

    /**
     * This interface is used to provide callbacks from the [WelcomeDialogFragment] to the activity
     * that called to show it.
     *
     * @author Danny Damsky
     */
    interface Listener {
        /**
         * This function gets called when the dialog's [positiveButton] gets pressed.
         */
        fun onWelcomeDialogPositiveButtonPressed()

        /**
         * This function gets called when the dialog's [negativeButton] gets pressed.
         */
        fun onWelcomeDialogNegativeButtonPressed()
    }

}