package com.damsky.danny.libremusic.data.db.nonentity.queue.callback

/**
 * This interface is used to indicate of various small changes that happen during playback.
 *
 * @author Danny Damsky
 */
interface OnPlaybackInfoChangedListener : BaseCallback {
    /**
     * The music has stopped playing.
     */
    fun onPlaybackStopped()

    /**
     * The music has started playing.
     */
    fun onPlaybackStarted()

    /**
     * Something in the queue changed, either the index, or the size, or both.
     *
     * @param index the position of the currently selected song in the queue.
     * @param size the total songs count in the queue.
     */
    fun onQueueResized(index: Int, size: Int)

    /**
     * Gets called every 16 milliseconds when the music is playing to allow the updating of the
     * interface 60 times per second (60FPS standard).
     *
     * @param playerPositionMillis the current position of the media player in its playback.
     */
    fun onPlayerPositionChanged(playerPositionMillis: Int)
}