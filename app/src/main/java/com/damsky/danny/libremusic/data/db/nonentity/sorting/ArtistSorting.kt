package com.damsky.danny.libremusic.data.db.nonentity.sorting

import android.arch.lifecycle.LiveData
import com.damsky.danny.libremusic.data.db.dao.ArtistDao
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayArtist

/**
 * The values of this enum class represent the available sorting types for lists representing
 * the artist entity.
 *
 * @author Danny Damsky
 */
enum class ArtistSorting {
    NONE, NAME_ASC, NAME_DESC;

    /**
     * @param artistDao the DAO (Data Access Object) for working with the artists table.
     *
     * @return the appropriate [LiveData] object of a [List] of [DisplayArtist] according to the
     * sorting enum value.
     */
    fun getDisplayArtistsLiveData(artistDao: ArtistDao) = when (this) {
        NONE -> artistDao.getAllDisplayLive()
        NAME_ASC -> artistDao.getAllDisplayOrderedByNameAscLive()
        NAME_DESC -> artistDao.getAllDisplayOrderedByNameDescLive()
    }
}