package com.damsky.danny.libremusic.data.db.nonentity.queue.manager

import android.support.v4.util.ArraySet
import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.BaseCallback

/**
 * A class that manages callbacks which are based on [BaseCallback].
 * It should be extended for different extensions of the [BaseCallback] interface.
 *
 * @author Danny Damsky
 *
 * @param T an interface extension of [BaseCallback].
 * @param registeredListeners the a [Set] of the currently registered listeners to the Manager.
 */
abstract class BaseManager<T : BaseCallback>(private val registeredListeners: ArraySet<T>) {
    constructor() : this(ArraySet())

    /**
     * Registers a new listener.
     *
     * @param callback the new callback to register and manage.
     */
    fun register(callback: T) {
        callback.isRegistered = true
        registeredListeners.add(callback)
    }

    /**
     * Unregisters a listener.
     *
     * @param callback the callback to unregister and stop managing.
     */
    fun unregister(callback: T) {
        callback.isRegistered = false
        registeredListeners.remove(callback)
    }

    /**
     * A [forEach] shortcut for easy iteration over the registered listeners.
     */
    protected fun forEach(operation: (callback: T) -> Unit) =
            registeredListeners.forEach { operation(it) }

    override fun toString() = "${javaClass.simpleName}(registeredListeners=$registeredListeners)"
}