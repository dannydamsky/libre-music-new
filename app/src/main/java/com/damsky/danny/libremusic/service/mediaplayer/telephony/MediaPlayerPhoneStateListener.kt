package com.damsky.danny.libremusic.service.mediaplayer.telephony

import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager

/**
 * This class listens for the device's call state and reacts upon its changes by using callbacks
 * that should be implemented by the class that is implementing this listener.
 *
 * @author Danny Damsky
 *
 * @param callbacks the callbacks that will be sent to the implementing class.
 */
class MediaPlayerPhoneStateListener(private val callbacks: Callbacks) : PhoneStateListener() {

    override fun onCallStateChanged(state: Int, phoneNumber: String?) {
        super.onCallStateChanged(state, phoneNumber)
        when (state) {
            TelephonyManager.CALL_STATE_OFFHOOK,
            TelephonyManager.CALL_STATE_RINGING -> callbacks.onPhoneBusy()
            TelephonyManager.CALL_STATE_IDLE -> callbacks.onPhoneIdle()
        }
    }

    /**
     * @param telephonyManager the [TelephonyManager] object to listen to the call state with.
     */
    fun listen(telephonyManager: TelephonyManager) =
            telephonyManager.listen(this, PhoneStateListener.LISTEN_CALL_STATE)

    /**
     * @param telephonyManager the [TelephonyManager] object to stop listening to the call state with.
     */
    fun stopListening(telephonyManager: TelephonyManager) =
            telephonyManager.listen(this, PhoneStateListener.LISTEN_NONE)

    override fun toString() = "MediaPlayerPhoneStateListener(callbacks=$callbacks)"

    /**
     * These callbacks are called from [MediaPlayerPhoneStateListener], and must be implemented
     * when initialising that class. The functions they include give the implementing class an
     * indication of the phone state.
     *
     * @author Danny Damsky
     */
    interface Callbacks {
        /**
         * The phone is busy, meaning that a call is in progress.
         */
        fun onPhoneBusy()

        /**
         * The phone is idle, meaning that the call has ended.
         */
        fun onPhoneIdle()
    }

}
