package com.damsky.danny.libremusic.data.db.nonentity.queue.callback

import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.AbstractSongRowItem

/**
 * This callback is used in order to pass a new queue to the implementing class whenever the queue
 * changes.
 *
 * @author Danny Damsky
 */
interface OnQueueChangedListener : BaseCallback {

    /**
     * @param queue a list of [AbstractSongRowItem] representing the new queue.
     */
    fun onQueueChanged(queue: List<AbstractSongRowItem>)
}