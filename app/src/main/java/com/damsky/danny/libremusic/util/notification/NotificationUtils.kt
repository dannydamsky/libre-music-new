package com.damsky.danny.libremusic.util.notification

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.util.Constants

/**
 * A singleton utility class for sending notifications.
 *
 * @author Danny Damsky
 */
class NotificationUtils private constructor(context: Context) {

    companion object {
        // Media notification constants.
        private const val MEDIA_NOTIFICATION_ID = 301
        private const val MEDIA_NOTIFICATION_CHANNEL_ID = "libre_music_media_player_id"
        private const val MEDIA_NOTIFICATION_CHANNEL_PURPOSE = "Media Playback"
        private const val MEDIA_NOTIFICATION_CHANNEL_DESCRIPTION = "Media playback controls."

        // Progress notification constants.
        private const val PROGRESS_NOTIFICATION_ID = 401
        private const val PROGRESS_NOTIFICATION_CHANNEL_ID = "libre_music_progress_id"
        private const val PROGRESS_NOTIFICATION_CHANNEL_PURPOSE = "Show Library Scan Progress"
        private const val PROGRESS_NOTIFICATION_CHANNEL_DESCRIPTION = "Shows the progress of the music library scan."

        private var INSTANCE: NotificationUtils? = null

        /**
         * @param context the function accesses the applicationContext property.
         * @return an instance of the [NotificationUtils] class. If the instance doesn't exist yet
         * it is created and returned.
         */
        @JvmStatic
        fun getInstance(context: Context): NotificationUtils {
            if (INSTANCE == null)
                synchronized(NotificationUtils::class) {
                    if (INSTANCE == null)
                        INSTANCE = NotificationUtils(context.applicationContext)
                }
            return INSTANCE!!
        }
    }

    private val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    init {
        if (Constants.IS_OREO_OR_ABOVE) {
            createMediaNotificationChannel()
            createProgressNotificationChannel()
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun createMediaNotificationChannel() {
        val notificationChannel = NotificationChannel(MEDIA_NOTIFICATION_CHANNEL_ID,
                MEDIA_NOTIFICATION_CHANNEL_PURPOSE, NotificationManager.IMPORTANCE_LOW)
        notificationChannel.description = MEDIA_NOTIFICATION_CHANNEL_DESCRIPTION
        notificationChannel.setShowBadge(false)
        notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        notificationManager.createNotificationChannel(notificationChannel)
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun createProgressNotificationChannel() {
        val notificationChannel = NotificationChannel(PROGRESS_NOTIFICATION_CHANNEL_ID,
                PROGRESS_NOTIFICATION_CHANNEL_PURPOSE, NotificationManager.IMPORTANCE_LOW)
        notificationChannel.description = PROGRESS_NOTIFICATION_CHANNEL_DESCRIPTION
        notificationChannel.setShowBadge(false)
        notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        notificationManager.createNotificationChannel(notificationChannel)
    }

    /**
     * Sends an notification with playback information.
     * @param mediaNotification contains the playback information used to send the notification.
     */
    fun sendMediaNotification(mediaNotification: MediaNotification) {
        val notification = NotificationCompat.Builder(mediaNotification.context, MEDIA_NOTIFICATION_CHANNEL_ID)
                .setShowWhen(false)
                .setOngoing(mediaNotification.mediaPlayerIsPlaying)
                .setStyle(android.support.v4.media.app.NotificationCompat.MediaStyle()
                        .setMediaSession(mediaNotification.sessionToken)
                        .setShowActionsInCompactView(0, 1, 2))
                .setColor(mediaNotification.context.getColor(R.color.colorPrimary))
                .setLargeIcon(mediaNotification.buildLargeIcon())
                .setSmallIcon(R.mipmap.ic_foreground)
                .setContentText(mediaNotification.song.artistName)
                .setContentTitle(mediaNotification.song.name)
                .addAction(mediaNotification.createPreviousButton())
                .addAction(mediaNotification.createPlayPauseButton())
                .addAction(mediaNotification.createNextButton())
                .build()
        notificationManager.notify(MEDIA_NOTIFICATION_ID, notification)
    }

    /**
     * Sends a notification with progress information.
     * @param service a context that's performing a heavy operation.
     * @param notification the notification to send.
     */
    fun sendProgressNotification(service: Service, notification: Notification) =
            if (Constants.IS_OREO_OR_ABOVE)
                service.startForeground(PROGRESS_NOTIFICATION_ID, notification)
            else
                notificationManager.notify(PROGRESS_NOTIFICATION_ID, notification)

    /**
     * Initialises the [NotificationCompat.Builder] with default properties for the progress notification.
     */
    fun getProgressNotificationBuilder(context: Context, title: String) =
            NotificationCompat.Builder(context, PROGRESS_NOTIFICATION_CHANNEL_ID)
                    .setShowWhen(false)
                    .setSmallIcon(R.mipmap.ic_foreground)
                    .setCategory(NotificationCompat.CATEGORY_PROGRESS)
                    .setPriority(NotificationCompat.PRIORITY_LOW)
                    .setContentTitle(title)!!

    /**
     * Cancels the media notification.
     */
    fun removeMediaNotification() = notificationManager.cancel(MEDIA_NOTIFICATION_ID)

    /**
     * Cancels the progress notification.
     */
    fun removeProgressNotification() = notificationManager.cancel(PROGRESS_NOTIFICATION_ID)

}