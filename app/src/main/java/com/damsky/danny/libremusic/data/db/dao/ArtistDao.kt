package com.damsky.danny.libremusic.data.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.damsky.danny.libremusic.data.db.RoomDbConstants
import com.damsky.danny.libremusic.data.db.entity.Artist
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayArtist

/**
 * An interface that contains all the functions to be implemented by Room for working with
 * the artists table in the database.
 *
 * @author Danny Damsky
 * @see Artist
 * @see BaseDao
 */
@Dao
interface ArtistDao : BaseDao<Artist> {

    //******************************************************************//
    //******************GENERAL**DAO**FUNCTIONS************************//
    //****************************************************************//

    /**
     * Empties the artists table in the database.
     */
    @Query("DELETE FROM ARTISTS;")
    fun deleteAll()

    /**
     * @return a [LiveData] of a [List] of [DisplayArtist]s. This LiveData represents all artists in
     * the database, it includes all of [Artist]'s columns as well as two other columns representing the
     * [Artist]'s albums count and songs count respectively.
     */
    @Query("SELECT *, (SELECT COUNT(ALBUM_ID) FROM ALBUMS WHERE ALBUM_ARTIST_NAME=ARTIST_NAME) as ${RoomDbConstants.COUNT_ALBUMS}, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ARTIST_NAME=ARTIST_NAME) as ${RoomDbConstants.COUNT_SONGS} FROM ARTISTS;")
    fun getAllDisplayLive(): LiveData<List<DisplayArtist>>

    /**
     * @return a [LiveData] of a [List] of [DisplayArtist]s ordered by the artist's name in ascending order.
     * This LiveData represents all artists in the database,
     * it includes all of [Artist]'s columns as well as two other columns representing the
     * [Artist]'s albums count and songs count respectively.
     */
    @Query("SELECT *, (SELECT COUNT(ALBUM_ID) FROM ALBUMS WHERE ALBUM_ARTIST_NAME=ARTIST_NAME) as ${RoomDbConstants.COUNT_ALBUMS}, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ARTIST_NAME=ARTIST_NAME) as ${RoomDbConstants.COUNT_SONGS} FROM ARTISTS ORDER BY ARTIST_NAME ASC;")
    fun getAllDisplayOrderedByNameAscLive(): LiveData<List<DisplayArtist>>

    /**
     * @return a [LiveData] of a [List] of [DisplayArtist]s ordered by the artist's name in descending order.
     * This LiveData represents all artists in the database,
     * it includes all of [Artist]'s columns as well as two other columns representing the
     * [Artist]'s albums count and songs count respectively.
     */
    @Query("SELECT *, (SELECT COUNT(ALBUM_ID) FROM ALBUMS WHERE ALBUM_ARTIST_NAME=ARTIST_NAME) as ${RoomDbConstants.COUNT_ALBUMS}, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_ARTIST_NAME=ARTIST_NAME) as ${RoomDbConstants.COUNT_SONGS} FROM ARTISTS ORDER BY ARTIST_NAME DESC;")
    fun getAllDisplayOrderedByNameDescLive(): LiveData<List<DisplayArtist>>
}