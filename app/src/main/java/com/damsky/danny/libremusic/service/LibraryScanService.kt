package com.damsky.danny.libremusic.service

import android.annotation.SuppressLint
import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationCompat
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.RoomDbRepository
import com.damsky.danny.libremusic.data.prefs.SharedPreferencesHelper
import com.damsky.danny.libremusic.service.LibraryScanService.Companion.startScan
import com.damsky.danny.libremusic.util.Constants
import com.damsky.danny.libremusic.util.ProgressToPercentConverter
import com.damsky.danny.libremusic.util.cue.CueFile
import com.damsky.danny.libremusic.util.cue.CueFileParser
import com.damsky.danny.libremusic.util.loader.BigMediaObject
import com.damsky.danny.libremusic.util.loader.SmallMediaObject
import com.damsky.danny.libremusic.util.loader.SongLoader
import com.damsky.danny.libremusic.util.notification.NotificationUtils

/**
 * This service is a foreground service that is used to scan the user's storage for audio files
 * and insert them into the application's database.
 *
 * This service can be started by calling [startScan].
 *
 * @author Danny Damsky
 */
class LibraryScanService : IntentService("com.damsky.danny.libremusic.service.LibraryScanService") {

    companion object {

        /**
         * Calling this function will start the [LibraryScanService].
         * @param context a context of the application package used to call this function.
         */
        @SuppressLint("NewApi")
        @JvmStatic
        fun startScan(context: Context) {
            val serviceIntent = Intent(context, LibraryScanService::class.java)
            if (Constants.IS_OREO_OR_ABOVE)
                context.startForegroundService(serviceIntent)
            context.startService(serviceIntent)
        }
    }

    private lateinit var cueFileParser: CueFileParser
    private lateinit var roomDbRepository: RoomDbRepository
    private lateinit var notificationUtils: NotificationUtils
    private lateinit var notificationBuilder: NotificationCompat.Builder

    override fun onHandleIntent(intent: Intent?) {
        roomDbRepository = RoomDbRepository.getInstance(this)
        if (roomDbRepository.songDao.getCount() == 0) {
            notificationUtils = NotificationUtils.getInstance(this)
            loadAudio()
        }
        QueueInitService.start(this)
    }

    private fun loadAudio() {
        SongLoader(contentResolver).use {
            if (it.isAble()) {
                notificationBuilder = notificationUtils.getProgressNotificationBuilder(this,
                        getString(R.string.scanning_music))
                val progressToPercentConverter = ProgressToPercentConverter(it.getCount())
                sendNotification(progressToPercentConverter)
                initCueParserObject()
                roomDbRepository.roomDb.runInTransaction {
                    while (it.moveToNext()) {
                        progressToPercentConverter.increment()
                        sendNotification(progressToPercentConverter)
                        val smallMediaObject = it.getSmallMediaObject()
                        if (shouldInsertRegular(CueFile.newInstance(smallMediaObject)))
                            performRegularInsert(it, smallMediaObject)
                        else
                            performInsertFromCueSheet()
                    }
                }
                notificationUtils.removeProgressNotification()
            } else {
                // TODO NOTIFY NO SONGS FOUND + BROADCAST
            }
        }
    }

    private fun sendNotification(progressToPercentConverter: ProgressToPercentConverter) {
        progressToPercentConverter.fillNotificationBuilderProperties(notificationBuilder)
        val notification = notificationBuilder.build()
        notificationUtils.sendProgressNotification(this, notification)
    }

    private fun initCueParserObject() {
        val sharedPreferencesHelper = SharedPreferencesHelper.getInstance(this)
        cueFileParser = CueFileParser(sharedPreferencesHelper.getCueFileEncoding())
    }

    private fun shouldInsertRegular(cueFile: CueFile?) = if (cueFile != null) {
        cueFileParser.setDataSource(cueFile)
        !cueFileParser.cueFileReadable()
    } else
        true

    private fun performRegularInsert(songLoader: SongLoader, smallMediaObject: SmallMediaObject) {
        val bigMediaObject = BigMediaObject(songLoader, smallMediaObject)
        roomDbRepository.artistDao.insert(bigMediaObject.buildArtist())
        roomDbRepository.albumDao.insert(bigMediaObject.buildAlbum())
        roomDbRepository.genreDao.insert(bigMediaObject.buildGenre())
        roomDbRepository.songDao.insert(bigMediaObject.buildSong())
    }

    private fun performInsertFromCueSheet() {
        cueFileParser.prepare()
        roomDbRepository.artistDao.insert(cueFileParser.buildArtist())
        roomDbRepository.albumDao.insert(cueFileParser.buildAlbum())
        roomDbRepository.genreDao.insert(cueFileParser.buildGenre())
        while (cueFileParser.moveToNext())
            roomDbRepository.songDao.insert(cueFileParser.buildSong())
    }
}