package com.damsky.danny.libremusic

import android.content.Context
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions

/**
 * This module allows the usage of GlideApp on the application level.
 * GlideApp allows for improved caching of images, improving performance.
 *
 * @author Danny Damsky
 * @see GlideApp
 */
@GlideModule
class MyAppGlideModule : AppGlideModule() {

    /**
     * Overriding this function ensures that hardware bitmaps are enabled in Glide.
     * This theoretically improves performance.
     */
    override fun applyOptions(context: Context, builder: GlideBuilder) {
        builder.setDefaultRequestOptions(RequestOptions().format(DecodeFormat.PREFER_ARGB_8888))
    }
}