package com.damsky.danny.libremusic.util.notification

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.support.annotation.DrawableRes
import android.support.v4.app.NotificationCompat
import android.support.v4.media.session.MediaSessionCompat
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService
import com.damsky.danny.libremusic.service.mediaplayer.enumeration.PlaybackAction
import com.damsky.danny.libremusic.service.mediaplayer.enumeration.PlaybackStatus

/**
 * This class is consumed by [NotificationUtils] when sending a notification about the state
 * of music playback in the application.
 *
 * @constructor initializes the remaining class parameters based on [playbackStatus].
 * @property context the Service's context, used to send the actual notification.
 * @property song the song's information will be displayed in the notification.
 * @property sessionToken required for the Notification's media style.
 * @property playbackStatus required to know if the playback is playing or paused.
 * @property mediaPlayerIsPlaying indicates whether or not the media player is playing.
 * @property playPauseAction the action to bind to the Notification's play/pause button.
 * @property playPauseIconId the icon to set to the Notification's play/pause button.
 * @author Danny Damsky
 */
data class MediaNotification(val context: Context, val song: PlayableSong,
                             val sessionToken: MediaSessionCompat.Token,
                             val playbackStatus: PlaybackStatus) {
    companion object {
        private const val NOTIFICATION_PREVIOUS_BUTTON = "Previous"
        private const val NOTIFICATION_PLAY_PAUSE_BUTTON = "Play/Pause"
        private const val NOTIFICATION_NEXT_BUTTON = "Next"
    }

    val mediaPlayerIsPlaying: Boolean
    private val playPauseAction: PlaybackAction
    @DrawableRes
    private val playPauseIconId: Int

    init {
        if (playbackStatus == PlaybackStatus.PLAYING) {
            mediaPlayerIsPlaying = true
            playPauseAction = PlaybackAction.ACTION_PAUSE
            playPauseIconId = R.drawable.ic_pause_white_24dp
        } else {
            mediaPlayerIsPlaying = false
            playPauseAction = PlaybackAction.ACTION_PLAY
            playPauseIconId = R.drawable.ic_play_arrow_white_24dp
        }
    }

    /**
     * @return a [Bitmap] of the [PlayableSong.coverData] if it's not null, else
     *  a [Bitmap] of [R.drawable.ic_song_rectangle_96dp].
     */
    fun buildLargeIcon(): Bitmap = if (song.coverData == null)
        buildBitmapFromSongPlaceholderDrawable()
    else {
        try {
            BitmapFactory.decodeFile(song.coverData)
        } catch (e: Exception) {
            buildBitmapFromSongPlaceholderDrawable()
        }
    }

    private fun buildBitmapFromSongPlaceholderDrawable(): Bitmap {
        val drawable = context.getDrawable(R.drawable.ic_song_rectangle_96dp)!!
        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }

    /**
     * @return a [NotificationCompat.Action] for the previous button with its according icons and actions.
     */
    fun createPreviousButton() = getNotificationAction(context,
            R.drawable.ic_skip_previous_white_24dp,
            NOTIFICATION_PREVIOUS_BUTTON,
            PlaybackAction.ACTION_PREVIOUS)

    /**
     * @return a [NotificationCompat.Action] for the play/pause button with its according icons and actions.
     */
    fun createPlayPauseButton() = getNotificationAction(context,
            this.playPauseIconId,
            NOTIFICATION_PLAY_PAUSE_BUTTON,
            this.playPauseAction)

    /**
     * @return a [NotificationCompat.Action] for the next button with its according icons and actions.
     */
    fun createNextButton() = getNotificationAction(context,
            R.drawable.ic_skip_next_white_24dp,
            NOTIFICATION_NEXT_BUTTON,
            PlaybackAction.ACTION_NEXT)

    private fun getNotificationAction(context: Context, @DrawableRes drawableId: Int, action: String,
                                      playbackAction: PlaybackAction) =
            NotificationCompat.Action.Builder(drawableId, action,
                    getPendingIntentForAction(context, playbackAction)).build()!!

    private fun getPendingIntentForAction(context: Context, playbackAction: PlaybackAction): PendingIntent {
        val actionIntent = Intent(context, MediaPlayerService::class.java)
        actionIntent.action = playbackAction.action
        return PendingIntent.getService(context, playbackAction.index, actionIntent, 0)
    }

    /**
     * Builder class for [MediaNotification].
     * @author Danny Damsky
     */
    class Builder {
        private lateinit var context: Context
        private lateinit var song: PlayableSong
        private lateinit var sessionToken: MediaSessionCompat.Token
        private lateinit var playbackStatus: PlaybackStatus

        /**
         * @param context the Service's context.
         */
        fun setContext(context: Context) = apply {
            this.context = context
        }

        /**
         * @param song the song that is currently chosen in the queue.
         */
        fun setSong(song: PlayableSong) = apply {
            this.song = song
        }

        /**
         * @param sessionToken the [MediaSessionCompat] object's token.
         */
        fun setSessionToken(sessionToken: MediaSessionCompat.Token) = apply {
            this.sessionToken = sessionToken
        }

        /**
         * @param playbackStatus the current playback status.
         */
        fun setPlaybackStatus(playbackStatus: PlaybackStatus) = apply {
            this.playbackStatus = playbackStatus
        }

        /**
         * @return an initialized [MediaNotification] object.
         */
        fun build() = MediaNotification(context, song, sessionToken, playbackStatus)

        override fun toString() =
                "Builder(context=$context, song=$song, sessionToken=$sessionToken, playbackStatus=$playbackStatus)"
    }
}