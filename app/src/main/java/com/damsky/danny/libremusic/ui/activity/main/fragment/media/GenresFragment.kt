package com.damsky.danny.libremusic.ui.activity.main.fragment.media

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.entity.Genre
import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplaySong
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.GenreRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.AbstractSongRowItem
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapter
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks
import com.damsky.danny.libremusic.util.CrashUtils
import com.damsky.danny.libremusic.util.IntentUtils
import com.damsky.danny.libremusic.util.RowItemMenuAction

/**
 * An implementation of [AbstractMediaFragment].
 * This implementation displays a list of [Genre]s and their appropriate songs.
 *
 * @author Danny Damsky
 */
class GenresFragment : AbstractMediaFragment() {

    private val genreCallbacks = object : MainActivityRecyclerViewAdapterCallbacks<Genre> {
        override fun onRowClick(item: Genre, itemPosition: Int) =
                onGenreCallbacksActionViewAllSongs(itemPosition, item)

        override fun onRowMenuClick(item: Genre, itemPosition: Int, menuAction: RowItemMenuAction) = when (menuAction) {
            RowItemMenuAction.ACTION_PLAY -> callOnGenreCallbacksActionPlay(item.name)
            RowItemMenuAction.ACTION_ADD_TO_QUEUE -> callOnGenreCallbacksAddToQueue(item.name)
            RowItemMenuAction.ACTION_ADD_TO_PLAYLIST -> callOnGenreCallbacksAddToPlaylist(item.name)
            RowItemMenuAction.ACTION_VIEW_ALL_SONGS -> onGenreCallbacksActionViewAllSongs(itemPosition, item)
            RowItemMenuAction.ACTION_SHARE -> callOnGenreCallbacksShare(item.name)
            RowItemMenuAction.ACTION_REMOVE_FROM_QUEUE,
            RowItemMenuAction.ACTION_SET_AS_RINGTONE,
            RowItemMenuAction.ACTION_RENAME_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_FROM_PLAYLIST ->
                onGenreCallbacksInvalidAction(item, itemPosition, menuAction)
        }
    }

    private fun onGenreCallbacksActionViewAllSongs(itemPosition: Int, item: Genre) {
        lastKnownGenre = item.name
        setSongsAdapterFromGenres(item.name, itemPosition)
    }

    private fun callOnGenreCallbacksActionPlay(genreName: String) =
            onSongCallbacksActionPlay(0) { songSorting ->
                songSorting.getPlayableSongsForGenre(roomDbViewModel.songDao, genreName)
                        as ArrayList<PlayableSong>
            }

    private fun callOnGenreCallbacksAddToQueue(genreName: String) =
            onOtherCallbacksAddToQueue { songSorting ->
                songSorting.getPlayableSongsForGenre(roomDbViewModel.songDao, genreName)
                        as ArrayList<PlayableSong>
            }

    private fun callOnGenreCallbacksAddToPlaylist(genreName: String) =
            onOtherCallbacksAddToPlaylist {
                roomDbViewModel.songDao.getSongIdsForGenre(genreName)
            }

    private fun callOnGenreCallbacksShare(genreName: String) =
            IntentUtils.shareMultipleSongs(mainActivity) {
                roomDbViewModel.songDao.getSongsDataForGenre(genreName)
            }

    private fun onGenreCallbacksInvalidAction(item: Genre, itemPosition: Int, menuAction: RowItemMenuAction) {
        mainActivity.showSnackBarLong(R.string.error_invalid_menu_action)
        CrashUtils.logError("GenresFragment.genreCallbacks.onRowMenuClick",
                "INVALID MENU ACTION: menuAction=$menuAction, item=$item, itemPosition=$itemPosition")
    }

    override val songCallbacks = object : MainActivityRecyclerViewAdapterCallbacks<DisplaySong> {
        override fun onRowClick(item: DisplaySong, itemPosition: Int) =
                callOnSongCallbacksActionPlay(itemPosition)

        override fun onRowMenuClick(item: DisplaySong, itemPosition: Int, menuAction: RowItemMenuAction) = when (menuAction) {
            RowItemMenuAction.ACTION_PLAY -> callOnSongCallbacksActionPlay(itemPosition)
            RowItemMenuAction.ACTION_ADD_TO_QUEUE -> onSongCallbacksActionAddToQueue(item.id!!)
            RowItemMenuAction.ACTION_ADD_TO_PLAYLIST -> onSongCallbacksActionAddToPlaylist(item.id!!)
            RowItemMenuAction.ACTION_SHARE -> onSongCallbacksActionShare(item.id!!)
            RowItemMenuAction.ACTION_SET_AS_RINGTONE -> onSongCallbacksActionSetAsRingtone(item)
            RowItemMenuAction.ACTION_REMOVE_FROM_QUEUE,
            RowItemMenuAction.ACTION_VIEW_ALL_SONGS,
            RowItemMenuAction.ACTION_RENAME_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_FROM_PLAYLIST ->
                onSongCallbacksInvalidAction(item, itemPosition, menuAction)
        }
    }


    private fun callOnSongCallbacksActionPlay(itemPosition: Int) =
            onSongCallbacksActionPlay(itemPosition) { songSorting ->
                songSorting.getPlayableSongsForGenre(roomDbViewModel.songDao, lastKnownGenre)
                        as ArrayList<PlayableSong>
            }

    private val genresAdapterObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            super.onItemRangeInserted(positionStart, itemCount)
            if (itemCount != 0 && lastScrollPositionGenres != -1) {
                recyclerView.scrollToPosition(lastScrollPositionGenres)
                lastScrollPositionGenres = -1
            }
        }
    }

    private val songsAdapterObserver = object : RecyclerView.AdapterDataObserver() {

    }

    private lateinit var lastKnownGenre: String
    private lateinit var genresAdapter: MainActivityRecyclerViewAdapter<Genre, GenreRowItem>
    private lateinit var songsAdapter: MainActivityRecyclerViewAdapter<DisplaySong, AbstractSongRowItem>

    private var lastScrollPositionGenres: Int = -1

    override fun onViewStubInflated() {
        initAdapters()
        initRecyclerViewProperties()
        setGenresAdapter()
    }

    private fun initAdapters() {
        initGenresAdapter()
        initSongsAdapter()
    }

    private fun initGenresAdapter() {
        genresAdapter = MainActivityRecyclerViewAdapter(GenreRowItem.DIFF_CALLBACK, genreCallbacks)
        genresAdapter.setLiveData(roomDbViewModel.genreRowItemsLiveData)
    }

    private fun initSongsAdapter() {
        songsAdapter = MainActivityRecyclerViewAdapter(AbstractSongRowItem.DIFF_CALLBACK, songCallbacks)
    }

    private fun initRecyclerViewProperties() {
        recyclerView.layoutManager = LinearLayoutManager(mainActivity)
        recyclerView.setHasFixedSize(true)
    }

    private fun setGenresAdapter() {
        genresAdapter.registerAdapterDataObserver(genresAdapterObserver)
        genresAdapter.startObserving(this)
        recyclerView.adapter = genresAdapter
    }

    private fun setSongsAdapterFromGenres(songName: String, lastScrollPositionGenres: Int) {
        genresAdapter.unregisterAdapterDataObserver(genresAdapterObserver)
        val liveData = roomDbViewModel.getSongRowItemsLiveDataForGenre(songName)
        songsAdapter.setLiveData(liveData)
        songsAdapter.registerAdapterDataObserver(songsAdapterObserver)
        songsAdapter.startObserving(this)
        recyclerView.adapter = this.songsAdapter
        genresAdapter.stopObserving()
        genresAdapter.submitList(null)
        this.lastScrollPositionGenres = lastScrollPositionGenres
    }

    override fun onBackPressed(): Boolean = if (lastScrollPositionGenres != -1) {
        setGenresAdapterFromSongs()
        false
    } else
        true

    private fun setGenresAdapterFromSongs() {
        songsAdapter.unregisterAdapterDataObserver(songsAdapterObserver)
        genresAdapter.registerAdapterDataObserver(genresAdapterObserver)
        genresAdapter.startObserving(this)
        recyclerView.adapter = genresAdapter
        songsAdapter.stopObserving()
        songsAdapter.submitList(null)
    }

    override fun reset() = when {
        lastScrollPositionGenres != -1 -> {
            lastScrollPositionGenres = -1
            setGenresAdapterFromSongs()
        }
        else -> recyclerView.smoothScrollToPosition(0)
    }
}