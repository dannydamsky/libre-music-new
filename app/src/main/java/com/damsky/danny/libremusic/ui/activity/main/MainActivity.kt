package com.damsky.danny.libremusic.ui.activity.main

import android.content.Intent
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v7.app.AppCompatActivity
import com.damsky.danny.libremusic.R

class MainActivity : AppCompatActivity(), IMainActivityExtras {
    private lateinit var callbacks: IMainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_NoActionBar)
        super.onCreate(savedInstanceState)
        callbacks = MainActivityDefaultImpl(this)
        callbacks.onCreate(savedInstanceState)
    }

    override fun onBackPressed() = callbacks.onBackPressed()

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        callbacks.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbacks.onActivityResult(requestCode, resultCode, data)
    }

    override fun onWelcomeDialogPositiveButtonPressed() =
            callbacks.onWelcomeDialogPositiveButtonPressed()

    override fun onWelcomeDialogNegativeButtonPressed() =
            callbacks.onWelcomeDialogNegativeButtonPressed()

    override fun showSnackBarShort(@StringRes textId: Int) = callbacks.showSnackBarShort(textId)

    override fun showSnackBarLong(@StringRes textId: Int) = callbacks.showSnackBarLong(textId)
}
