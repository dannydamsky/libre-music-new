package com.damsky.danny.libremusic.util.loader

import android.content.ContentResolver
import android.database.Cursor
import android.provider.MediaStore

/**
 * The main loader class, this class utilizes both [GenreLoader] and [CoverArtLoader] to retrieve
 * all the relevant information about the songs on the user's device.
 *
 * This class implements the [AutoCloseable] interface, meaning that it must be closed after its
 * use has finished, it also means that the closing can be done with a try-with-resources statement.
 *
 * @param contentResolver used to query the user's device for audio files.
 * @author Danny Damsky
 * @see GenreLoader
 * @see CoverArtLoader
 */
class SongLoader(contentResolver: ContentResolver) : AutoCloseable {

    private lateinit var cursor: Cursor

    private val count: Int
    private val able: Boolean

    private val dataColumnIndex: Int
    private val albumIdColumnIndex: Int
    private val durationColumnIndex: Int
    private val artistColumnIndex: Int
    private val albumColumnIndex: Int
    private val yearColumnIndex: Int
    private val idColumnIndex: Int
    private val titleColumnIndex: Int
    private val trackColumnIndex: Int

    private val genreLoader = GenreLoader(contentResolver)
    private val coverArtLoader = CoverArtLoader(contentResolver)

    init {
        val cursor = getCursor(contentResolver)
        if (cursor != null) {
            this.cursor = cursor
            count = cursor.count
            able = count > 0
            if (this.able) {
                dataColumnIndex = cursor.getColumnIndex(MediaStore.Audio.Media.DATA)
                albumIdColumnIndex = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID)
                durationColumnIndex = cursor.getColumnIndex(MediaStore.Audio.Media.DURATION)
                artistColumnIndex = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)
                albumColumnIndex = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM)
                yearColumnIndex = cursor.getColumnIndex(MediaStore.Audio.Media.YEAR)
                idColumnIndex = cursor.getColumnIndex(MediaStore.Audio.Media._ID)
                titleColumnIndex = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)
                trackColumnIndex = cursor.getColumnIndex(MediaStore.Audio.Media.TRACK)
            } else {
                dataColumnIndex = 0
                albumIdColumnIndex = 0
                durationColumnIndex = 0
                artistColumnIndex = 0
                albumColumnIndex = 0
                yearColumnIndex = 0
                idColumnIndex = 0
                titleColumnIndex = 0
                trackColumnIndex = 0
            }
        } else {
            count = 0
            able = false
            dataColumnIndex = 0
            albumIdColumnIndex = 0
            durationColumnIndex = 0
            artistColumnIndex = 0
            albumColumnIndex = 0
            yearColumnIndex = 0
            idColumnIndex = 0
            titleColumnIndex = 0
            trackColumnIndex = 0
        }
    }

    private fun getCursor(contentResolver: ContentResolver) =
            contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    arrayOf(MediaStore.Audio.Media.DATA,
                            MediaStore.Audio.Media.ALBUM_ID,
                            MediaStore.Audio.Media.DURATION,
                            MediaStore.Audio.Media.ARTIST,
                            MediaStore.Audio.Media.ALBUM,
                            MediaStore.Audio.Media.YEAR,
                            MediaStore.Audio.Media._ID,
                            MediaStore.Audio.Media.TITLE,
                            MediaStore.Audio.Media.TRACK),
                    "${MediaStore.Audio.Media.IS_MUSIC} = ?", arrayOf("1"), null)

    /**
     * @return true if the [SongLoader] is able to query the audio files, false otherwise.
     */
    fun isAble() = able

    /**
     * Move the [SongLoader]'s cursor to the next row.
     *
     * This method will return false if the cursor is already past the
     * last entry in the result set.
     *
     * @return whether the move succeeded.
     */
    fun moveToNext() = cursor.moveToNext()

    /**
     * @return the data of the audio file in the current row.
     */
    private fun getSongData() = cursor.getString(dataColumnIndex)!!

    /**
     * @return the cover art path of the audio file in the current row.
     */
    private fun getCoverArtPath(): String? {
        val albumId = cursor.getLong(albumIdColumnIndex)
        coverArtLoader.setAlbumId(albumId)
        return coverArtLoader.getCoverArt()
    }

    /**
     * @return the audio file's duration in milliseconds.
     */
    private fun getDurationInMillis() = cursor.getInt(durationColumnIndex)

    /**
     * @return the artist name of the audio file in the current row.
     */
    fun getArtistName() = cursor.getString(artistColumnIndex)!!

    /**
     * @return the album name of the audio file in the current row.
     */
    fun getAlbumName() = cursor.getString(albumColumnIndex)!!

    /**
     * @return the release date of the audio file in the current row.
     */
    fun getYearPublished() = cursor.getInt(yearColumnIndex)

    /**
     * @return the genre name of the audio file in the current row.
     */
    fun getGenreName() = genreLoader.getGenre(cursor.getString(idColumnIndex))

    /**
     * @return the title of the audio file in the current row.
     */
    fun getSongName() = cursor.getString(titleColumnIndex)!!

    /**
     * @return the track number of the audio file in the current row.
     */
    fun getTrackNumber() = cursor.getInt(trackColumnIndex)

    /**
     * @return a [SmallMediaObject] consisting of information from the audio file in the current row.
     */
    fun getSmallMediaObject() =
            SmallMediaObject(getSongData(), getCoverArtPath(), getDurationInMillis())

    /**
     * @return the number of rows in the [SongLoader]'s [Cursor].
     */
    fun getCount() = count

    override fun close() {
        this.genreLoader.close()
        this.cursor.close()
    }

    override fun toString() =
            "SongLoader(cursor=$cursor, count=$count, able=$able, dataColumnIndex=$dataColumnIndex, albumIdColumnIndex=$albumIdColumnIndex, durationColumnIndex=$durationColumnIndex, artistColumnIndex=$artistColumnIndex, albumColumnIndex=$albumColumnIndex, yearColumnIndex=$yearColumnIndex, idColumnIndex=$idColumnIndex, titleColumnIndex=$titleColumnIndex, trackColumnIndex=$trackColumnIndex, genreLoader=$genreLoader, coverArtLoader=$coverArtLoader)"
}
