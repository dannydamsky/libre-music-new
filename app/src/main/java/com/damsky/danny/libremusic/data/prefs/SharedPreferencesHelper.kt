package com.damsky.danny.libremusic.data.prefs

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import com.damsky.danny.libremusic.data.db.nonentity.sorting.*
import com.damsky.danny.libremusic.util.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * This class manages all of the [SharedPreferences] of the application, any [SharedPreferences] work
 * in the application is done through it.
 *
 * @author Danny Damsky
 */
class SharedPreferencesHelper private constructor(context: Context) {

    companion object {

        // Key constants for the default shared preferences.
        private const val DEFAULT_SHARED_PREFERENCES_ENCODING = "encoding"

        // Key constants for the runtime shared preferences.
        private const val RUNTIME_SHARED_PREFERENCES_NAME = "runtime_shared_preferences"
        private const val RUNTIME_SHARED_PREFERENCES_SHUFFLE_ENABLED = "shuffle_enabled"
        private const val RUNTIME_SHARED_PREFERENCES_REPEAT_ENABLED = "repeat_enabled"
        private const val RUNTIME_SHARED_PREFERENCES_ARTIST_SORTING = "artist_sorting"
        private const val RUNTIME_SHARED_PREFERENCES_ALBUM_SORTING = "album_sorting"
        private const val RUNTIME_SHARED_PREFERENCES_SONG_SORTING = "song_sorting"
        private const val RUNTIME_SHARED_PREFERENCES_GENRE_SORTING = "genre_sorting"
        private const val RUNTIME_SHARED_PREFERENCES_PLAYLIST_SORTING = "playlist_sorting"
        private const val RUNTIME_SHARED_PREFERENCES_LAST_KNOWN_INDEX = "last_known_index"
        private const val RUNTIME_SHARED_PREFERENCES_IS_FIRST_RUN = "is_first_run"

        // Key constants for the queue shared preferences.
        private const val QUEUE_SHARED_PREFERENCES_NAME = "queue_shared_preferences"

        private var INSTANCE: SharedPreferencesHelper? = null

        /**
         * @param context the function accesses the applicationContext property.
         * @return an instance of the [SharedPreferencesHelper] class. If the instance doesn't exist yet
         * it is created and returned.
         */
        @JvmStatic
        fun getInstance(context: Context): SharedPreferencesHelper {
            if (INSTANCE == null)
                synchronized(SharedPreferencesHelper::class) {
                    if (INSTANCE == null)
                        INSTANCE = SharedPreferencesHelper(context.applicationContext)
                }
            return INSTANCE!!
        }


        //******************************************************************//
        //******************STATIC**HELPER**FUNCTIONS**********************//
        //****************************************************************//

        @JvmStatic
        private fun SharedPreferences.putStringAsync(key: String, value: String) =
                edit().putString(key, value).apply()

        @JvmStatic
        private fun SharedPreferences.putBooleanAsync(key: String, value: Boolean) =
                edit().putBoolean(key, value).apply()

        @JvmStatic
        private fun SharedPreferences.putIntAsync(key: String, value: Int) =
                edit().putInt(key, value).apply()

        @JvmStatic
        private inline fun <reified T : Enum<T>> SharedPreferences.getEnum(key: String, defaultValue: T): T {
            getString(key, null)?.let { return enumValueOf(it) }
            return defaultValue
        }

        @JvmStatic
        private fun Context.getSharedPreferences(key: String) =
                getSharedPreferences(key, Context.MODE_PRIVATE)
    }

    // SharedPreferences objects
    private val defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private val runtimeSharedPreferences = context.getSharedPreferences(RUNTIME_SHARED_PREFERENCES_NAME)
    private val queueSharedPreferences = context.getSharedPreferences(QUEUE_SHARED_PREFERENCES_NAME)

    // LiveData objects of preferences
    private val artistSortingLiveData = MutableLiveData<ArtistSorting>()
    private val albumSortingLiveData = MutableLiveData<AlbumSorting>()
    private val songSortingLiveData = MutableLiveData<SongSorting>()
    private val genreSortingLiveData = MutableLiveData<GenreSorting>()
    private val playlistSortingLiveData = MutableLiveData<PlaylistSorting>()
    private val shuffleEnabledLiveData = MutableLiveData<Boolean>()
    private val repeatEnabledLiveData = MutableLiveData<Boolean>()

    init {
        // Stores current preferences in  the LiveData objects.
        artistSortingLiveData.postValue(getArtistSorting())
        albumSortingLiveData.postValue(getAlbumSorting())
        songSortingLiveData.postValue(getSongSorting())
        genreSortingLiveData.postValue(getGenreSorting())
        playlistSortingLiveData.postValue(getPlaylistSorting())
        shuffleEnabledLiveData.postValue(shuffleEnabled())
        repeatEnabledLiveData.postValue(repeatEnabled())
    }

    /**
     * @return the encoding used for parsing Cue sheets.
     */
    fun getCueFileEncoding() =
            defaultSharedPreferences.getString(DEFAULT_SHARED_PREFERENCES_ENCODING, Constants.DEFAULT_ENCODING)!!

    /**
     * @return true if shuffle is enabled, false otherwise.
     */
    fun shuffleEnabled() =
            runtimeSharedPreferences.getBoolean(RUNTIME_SHARED_PREFERENCES_SHUFFLE_ENABLED, false)

    /**
     * @return true if repeat is enabled, false otherwise.
     */
    fun repeatEnabled() =
            runtimeSharedPreferences.getBoolean(RUNTIME_SHARED_PREFERENCES_REPEAT_ENABLED, false)

    /**
     * @param shuffleEnabled a boolean value indicating whether to enable shuffle or not.
     */
    fun putShuffleEnabled(shuffleEnabled: Boolean) {
        runtimeSharedPreferences.putBooleanAsync(RUNTIME_SHARED_PREFERENCES_SHUFFLE_ENABLED, shuffleEnabled)
        shuffleEnabledLiveData.postValue(shuffleEnabled)
    }

    /**
     * @param repeatEnabled a boolean value indicating whether to enable repeat or not.
     */
    fun putRepeatEnabled(repeatEnabled: Boolean) {
        runtimeSharedPreferences.putBooleanAsync(RUNTIME_SHARED_PREFERENCES_REPEAT_ENABLED, repeatEnabled)
        repeatEnabledLiveData.postValue(repeatEnabled)
    }

    /**
     * @param artistSorting an enum value representing the form of the sorting to be used for artists.
     */
    fun putArtistSorting(artistSorting: ArtistSorting) {
        artistSortingLiveData.postValue(artistSorting)
        runtimeSharedPreferences.putStringAsync(RUNTIME_SHARED_PREFERENCES_ARTIST_SORTING, artistSorting.name)
    }

    /**
     * @return an enum value representing the form of sorting to be used for artists.
     */
    fun getArtistSorting() =
            runtimeSharedPreferences.getEnum(RUNTIME_SHARED_PREFERENCES_ARTIST_SORTING, ArtistSorting.NAME_ASC)

    /**
     * @param albumSorting an enum value representing the form of the sorting to be used for albums.
     */
    fun putAlbumSorting(albumSorting: AlbumSorting) {
        albumSortingLiveData.postValue(albumSorting)
        runtimeSharedPreferences.putStringAsync(RUNTIME_SHARED_PREFERENCES_ALBUM_SORTING, albumSorting.name)
    }

    /**
     * @return an enum value representing the form of the sorting to be used for albums.
     */
    fun getAlbumSorting() =
            runtimeSharedPreferences.getEnum(RUNTIME_SHARED_PREFERENCES_ALBUM_SORTING, AlbumSorting.ARTIST_NAME_ASC)

    /**
     * @param songSorting an enum value representing the form of the sorting to be used for songs.
     */
    fun putSongSorting(songSorting: SongSorting) {
        songSortingLiveData.postValue(songSorting)
        runtimeSharedPreferences.putStringAsync(RUNTIME_SHARED_PREFERENCES_SONG_SORTING, songSorting.name)
    }

    /**
     * @return an enum value representing the form of the sorting to be used for songs.
     */
    fun getSongSorting() =
            runtimeSharedPreferences.getEnum(RUNTIME_SHARED_PREFERENCES_SONG_SORTING, SongSorting.ARTIST_NAME_ASC)

    /**
     * @param genreSorting an enum value representing the form of the sorting to be used for genres.
     */
    fun putGenreSorting(genreSorting: GenreSorting) {
        genreSortingLiveData.postValue(genreSorting)
        runtimeSharedPreferences.putStringAsync(RUNTIME_SHARED_PREFERENCES_GENRE_SORTING, genreSorting.name)
    }

    /**
     * @return an enum value representing the form of the sorting to be used for genres.
     */
    fun getGenreSorting() =
            runtimeSharedPreferences.getEnum(RUNTIME_SHARED_PREFERENCES_GENRE_SORTING, GenreSorting.NAME_ASC)

    /**
     * @param playlistSorting an enum value representing the form of the sorting to be used for playlists.
     */
    fun putPlaylistSorting(playlistSorting: PlaylistSorting) {
        playlistSortingLiveData.postValue(playlistSorting)
        runtimeSharedPreferences.putStringAsync(RUNTIME_SHARED_PREFERENCES_PLAYLIST_SORTING, playlistSorting.name)
    }

    /**
     * @return an enum value representing the form of the sorting to be used for playlists.
     */
    fun getPlaylistSorting() =
            runtimeSharedPreferences.getEnum(RUNTIME_SHARED_PREFERENCES_PLAYLIST_SORTING, PlaylistSorting.NAME_ASC)

    /**
     * @return a [LiveData] object that gets updated every time the value from [getArtistSorting] changes.
     */
    fun getArtistSortingLiveData(): LiveData<ArtistSorting> = artistSortingLiveData

    /**
     * @return a [LiveData] object that gets updated every time the value from [getAlbumSorting] changes.
     */
    fun getAlbumSortingLiveData(): LiveData<AlbumSorting> = albumSortingLiveData

    /**
     * @return a [LiveData] object that gets updated every time the value from [getSongSorting] changes.
     */
    fun getSongSortingLiveData(): LiveData<SongSorting> = songSortingLiveData

    /**
     * @return a [LiveData] object that gets updated every time the value from [getGenreSorting] changes.
     */
    fun getGenreSortingLiveData(): LiveData<GenreSorting> = genreSortingLiveData

    /**
     * @return a [LiveData] object that gets updated every time the value from [getPlaylistSorting] changes.
     */
    fun getPlaylistSortingLiveData(): LiveData<PlaylistSorting> = playlistSortingLiveData

    /**
     * @return a [LiveData] object that gets updated every time the value from [shuffleEnabled] changes.
     */
    fun getShuffleEnabledLiveData(): LiveData<Boolean> = shuffleEnabledLiveData

    /**
     * @return a [LiveData] object that gets updated every time the value from [repeatEnabled] changes.
     */
    fun getRepeatEnabledLiveData(): LiveData<Boolean> = repeatEnabledLiveData

    /**
     * Saves the queue in the shared preferences and deletes any old values if they existed.
     *
     * @param playableSongs the songs to save as the queue.
     */
    @SuppressLint("ApplySharedPref")
    fun updateQueue(playableSongs: List<PlayableSong>) {
        GlobalScope.launch(Dispatchers.IO) {
            val queuePrefsEditor = queueSharedPreferences.edit()
            queuePrefsEditor.clear()
            for ((i, song) in playableSongs.withIndex()) {
                queuePrefsEditor.putLong("$i", song.id!!)
            }
            queuePrefsEditor.commit()
        }
    }

    /**
     * @return all of the IDs of the songs from the queue.
     */
    fun getQueue() = queueSharedPreferences.all

    /**
     * Removes the song IDs from the shared preferences.
     */
    fun clearQueue() = queueSharedPreferences.edit().clear().apply()

    /**
     * @param lastKnownIndex the index of the last known song that was selected in the queue.
     */
    fun putLastKnownIndex(lastKnownIndex: Int) = runtimeSharedPreferences.putIntAsync(RUNTIME_SHARED_PREFERENCES_LAST_KNOWN_INDEX, lastKnownIndex)

    /**
     * @return the index of the last known song that was selected in the queue.
     */
    fun getLastKnownIndex() =
            runtimeSharedPreferences.getInt(RUNTIME_SHARED_PREFERENCES_LAST_KNOWN_INDEX, 0)

    /**
     * @return true if this is the first time that the user opens this application, false otherwise.
     */
    fun isFirstRun() =
            runtimeSharedPreferences.getBoolean(RUNTIME_SHARED_PREFERENCES_IS_FIRST_RUN, true)

    /**
     * @param isFirstRun a boolean value indicating true if this is the first time that the user
     * opens this application, false otherwise.
     */
    fun setFirstRun(isFirstRun: Boolean) =
            runtimeSharedPreferences.putBooleanAsync(RUNTIME_SHARED_PREFERENCES_IS_FIRST_RUN, isFirstRun)

}
