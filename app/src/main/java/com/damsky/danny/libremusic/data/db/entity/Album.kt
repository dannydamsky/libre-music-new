package com.damsky.danny.libremusic.data.db.entity

import android.arch.persistence.room.*
import com.damsky.danny.libremusic.data.db.RoomDbConstants

/**
 * This class represents the albums table in the database.
 *
 * Relationships:
 * Album 1-N Songs
 *
 * @author Danny Damsky
 *
 * @param id the ID of the album (auto-increment).
 * @param name the name of the album.
 * @param artistName the name of the album's artist.
 * @param yearPublished the release year of the album.
 * @param coverData the path to the album's cover art image.
 */
@Entity(tableName = RoomDbConstants.TABLE_NAME_ALBUMS,
        foreignKeys = [ForeignKey(entity = Artist::class,
                parentColumns = [RoomDbConstants.COLUMN_ARTIST_NAME],
                childColumns = [RoomDbConstants.COLUMN_ALBUM_ARTIST_NAME],
                onDelete = ForeignKey.CASCADE)],
        indices = [Index(value = [RoomDbConstants.COLUMN_ALBUM_NAME,
            RoomDbConstants.COLUMN_ALBUM_ARTIST_NAME],
                unique = true),
            Index(value = [RoomDbConstants.COLUMN_ALBUM_ARTIST_NAME]),
            Index(value = [RoomDbConstants.COLUMN_ALBUM_NAME])])
data class Album(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = RoomDbConstants.COLUMN_ALBUM_ID)
        val id: Long?,
        @ColumnInfo(name = RoomDbConstants.COLUMN_ALBUM_NAME)
        val name: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_ALBUM_ARTIST_NAME)
        val artistName: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_ALBUM_YEAR_PUBLISHED)
        val yearPublished: Int = RoomDbConstants.YEAR_PUBLISHED_NONE,
        @ColumnInfo(name = RoomDbConstants.COLUMN_ALBUM_COVER_DATA)
        val coverData: String?) {

    /**
     * A builder class for the [Album] entity.
     *
     * @author Danny Damsky
     */
    class Builder {
        private var id: Long? = null
        private lateinit var name: String
        private lateinit var artistName: String
        private var yearPublished = RoomDbConstants.YEAR_PUBLISHED_NONE
        private var coverData: String? = null

        /**
         * @param id the ID of the album.
         */
        fun setId(id: Long?) = apply {
            this.id = id
        }

        /**
         * @param name the name of teh album.
         */
        fun setName(name: String) = apply {
            this.name = name
        }

        /**
         * @param artistName the name of the album's artist.
         */
        fun setArtistName(artistName: String) = apply {
            this.artistName = artistName
        }

        /**
         * @param yearPublished the release year of the album.
         */
        fun setYearPublished(yearPublished: Int) = apply {
            this.yearPublished = yearPublished
        }

        /**
         * @param coverData the path to the album's cover art image.
         */
        fun setCoverData(coverData: String?) = apply {
            this.coverData = coverData
        }

        /**
         * @return a new [Album] object with the properties set in this class.
         */
        fun build() = Album(id, name, artistName, yearPublished, coverData)

        override fun toString() =
                "Builder(id=$id, name='$name', artistName='$artistName', yearPublished=$yearPublished, coverData=$coverData)"
    }
}
