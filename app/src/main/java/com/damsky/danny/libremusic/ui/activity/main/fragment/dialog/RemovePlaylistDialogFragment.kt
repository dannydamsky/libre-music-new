package com.damsky.danny.libremusic.ui.activity.main.fragment.dialog

import android.arch.lifecycle.ViewModelProviders
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.*
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.RoomDbViewModel
import com.damsky.danny.libremusic.util.CrashUtils
import kotlinx.android.synthetic.main.dialog_remove_playlist.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * This [DialogFragment] class inflates [R.layout.dialog_remove_playlist].
 *
 * To show it, use the [RemovePlaylistDialogFragment.Companion.show] function.
 * The dialog handles the removing of the playlist independently
 * and doesn't have any callbacks to the activity.
 *
 * @author Danny Damsky
 */
class RemovePlaylistDialogFragment : DialogFragment() {

    companion object {
        private const val TAG =
                "com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.RemovePlaylistDialogFragment.TAG"

        private const val EXTRA_PLAYLIST_ID = "playlist_id"

        /**
         * Shows the [RemovePlaylistDialogFragment] using its own custom tag,
         * and passes it the [playlistId] as arguments.
         *
         * @param fragmentManager the [FragmentManager] of the activity.
         * @param playlistId the ID of the playlist to get passed over
         * to the [RemovePlaylistDialogFragment].
         */
        @JvmStatic
        fun show(fragmentManager: FragmentManager, playlistId: Long) {
            val fragment = RemovePlaylistDialogFragment()
            val bundle = Bundle()
            bundle.putLong(EXTRA_PLAYLIST_ID, playlistId)
            fragment.arguments = bundle
            fragment.show(fragmentManager, TAG)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog.setCancelable(false)
        try {
            setWindowProperties(dialog.window!!)
        } catch (e: NullPointerException) {
            CrashUtils.logException(e)
        } catch (e: Exception) {
            CrashUtils.logError(TAG, "Unexpected exception: ${e.message}")
        }
    }

    private fun setWindowProperties(window: Window) {
        window.setLayout((360 * resources.displayMetrics.density).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        window.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.transparent, null)))
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dialog = inflater.inflate(R.layout.dialog_remove_playlist, container, false)
        setPositiveButtonOnClickListener(dialog)
        setNegativeButtonOnClickListener(dialog)
        return dialog
    }

    private fun setPositiveButtonOnClickListener(dialog: View) =
            dialog.positiveButton.setOnClickListener {
                val playlistId = arguments!!.getLong(EXTRA_PLAYLIST_ID, 0)
                val playlistDao = ViewModelProviders.of(this).get(RoomDbViewModel::class.java).playlistDao
                GlobalScope.launch(Dispatchers.IO) { playlistDao.deleteById(playlistId) }
                dismiss()
            }

    private fun setNegativeButtonOnClickListener(dialog: View) =
            dialog.negativeButton.setOnClickListener { dismiss() }

}