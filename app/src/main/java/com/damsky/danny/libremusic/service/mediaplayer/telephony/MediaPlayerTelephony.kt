package com.damsky.danny.libremusic.service.mediaplayer.telephony

import android.content.Context
import android.telephony.TelephonyManager

/**
 * This class handles the telephony management for the MediaPlayerService.
 *
 * @author Danny Damsky
 *
 * @param context the context of the MediaPlayerService.
 * @param callbacks the callbacks to the MediaPlayerService from [MediaPlayerPhoneStateListener].
 *
 * @constructor Starts listening for the device's phone state.
 *
 */
class MediaPlayerTelephony(context: Context, callbacks: MediaPlayerPhoneStateListener.Callbacks) {

    private val phoneStateListener = MediaPlayerPhoneStateListener(callbacks)
    private val telephonyManager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    private var resumeAfterCall = false

    init {
        phoneStateListener.listen(telephonyManager)
    }

    /**
     * @param resumeAfterCall a boolean indicating whether or not the Media Player should resume
     * playing after the call has ended.
     */
    fun setResumeAfterCall(resumeAfterCall: Boolean) {
        this.resumeAfterCall = resumeAfterCall
    }

    /**
     * @returns true if the Media Player should resume playing after the call has ended,
     * false otherwise.
     */
    fun resumeAfterCall() = resumeAfterCall

    /**
     * Stops listening for the device's phone state.
     */
    fun stopListening() = phoneStateListener.stopListening(telephonyManager)

    override fun toString() =
            "MediaPlayerTelephony(phoneStateListener=$phoneStateListener, telephonyManager=$telephonyManager, resumeAfterCall=$resumeAfterCall)"
}