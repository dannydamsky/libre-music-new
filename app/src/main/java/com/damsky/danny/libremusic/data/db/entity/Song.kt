package com.damsky.danny.libremusic.data.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.PrimaryKey
import com.damsky.danny.libremusic.data.db.RoomDbConstants

/**
 * This class represents the songs table in the database.
 *
 * Relationships:
 * Artist 1-N Songs
 * Album 1-N Songs
 * Genre 1-N Songs
 * Playlists M-N Songs
 *
 * @author Danny Damsky
 *
 * @param id the ID of the song (auto-increment).
 * @param songData the path of the song's file.
 * @param name the name of the song.
 * @param albumName the name of the song's album.
 * @param artistName the name of the song's artist.
 * @param genreName the name of the song's genre.
 * @param trackNumber the song's track number in its album.
 * @param yearPublished the song's release year.
 * @param startTimeInMillis the time when the song starts in its file (in milliseconds).
 * @param endTimeInMillis the time when the song ends in its file (in milliseconds).
 * @param durationInMillis the total duration of the song (=[endTimeInMillis] - [startTimeInMillis])
 * @param coverData the path to the song's cover art image.
 */
@Entity(tableName = RoomDbConstants.TABLE_NAME_SONGS,
        foreignKeys = [ForeignKey(entity = Album::class,
                parentColumns = [RoomDbConstants.COLUMN_ALBUM_ARTIST_NAME, RoomDbConstants.COLUMN_ALBUM_NAME],
                childColumns = [RoomDbConstants.COLUMN_SONG_ARTIST_NAME, RoomDbConstants.COLUMN_SONG_ALBUM_NAME],
                onDelete = ForeignKey.CASCADE),
            ForeignKey(entity = Genre::class,
                    parentColumns = [RoomDbConstants.COLUMN_GENRE_NAME],
                    childColumns = [RoomDbConstants.COLUMN_SONG_GENRE_NAME],
                    onDelete = ForeignKey.CASCADE)])
data class Song(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_ID)
        val id: Long?,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_DATA)
        val songData: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_NAME)
        val name: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_ALBUM_NAME)
        val albumName: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_ARTIST_NAME)
        val artistName: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_GENRE_NAME)
        val genreName: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_TRACK_NUMBER)
        val trackNumber: Int = RoomDbConstants.TRACK_NUMBER_DEFAULT,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_YEAR_PUBLISHED)
        val yearPublished: Int = RoomDbConstants.YEAR_PUBLISHED_NONE,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_START_TIME_IN_MILLIS)
        val startTimeInMillis: Int,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_END_TIME_IN_MILLIS)
        val endTimeInMillis: Int,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_DURATION_IN_MILLIS)
        val durationInMillis: Int,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_COVER_DATA)
        val coverData: String?) {

    /**
     * A builder for the [Song] entity.
     *
     * @author Danny Damsky
     */
    class Builder {
        private var id: Long? = null
        private lateinit var songData: String
        private lateinit var name: String
        private lateinit var albumName: String
        private lateinit var artistName: String
        private lateinit var genreName: String
        private var trackNumber = RoomDbConstants.TRACK_NUMBER_DEFAULT
        private var yearPublished = RoomDbConstants.YEAR_PUBLISHED_NONE
        private var startTimeInMillis = 0
        private var endTimeInMillis = 0
        private var durationInMillis = 0
        private var coverData: String? = null

        /**
         * @param id the ID of the song.
         */
        fun setId(id: Long?) = apply {
            this.id = id
        }

        /**
         * @param songData the path of the song's file.
         */
        fun setSongData(songData: String) = apply {
            this.songData = songData
        }

        /**
         * @param name the name of the song.
         */
        fun setName(name: String) = apply {
            this.name = name
        }

        /**
         * @param albumName the name of the song's album.
         */
        fun setAlbumName(albumName: String) = apply {
            this.albumName = albumName
        }

        /**
         * @param artistName the name of the song's artist.
         */
        fun setArtistName(artistName: String) = apply {
            this.artistName = artistName
        }

        /**
         * @param genreName the name of the song's genre.
         */
        fun setGenreName(genreName: String) = apply {
            this.genreName = genreName
        }

        /**
         * @param trackNumber the song's track number in its album.
         */
        fun setTrackNumber(trackNumber: Int) = apply {
            this.trackNumber = trackNumber
        }

        /**
         * @param yearPublished the song's release year.
         */
        fun setYearPublished(yearPublished: Int) = apply {
            this.yearPublished = yearPublished
        }

        /**
         * @param startTimeInMillis the time when the song starts in its file (in milliseconds).
         */
        fun setStartTimeInMillis(startTimeInMillis: Int) = apply {
            this.startTimeInMillis = startTimeInMillis
        }

        /**
         * @param endTimeInMillis the time when the song ends in its file (in milliseconds).
         */
        fun setEndTimeInMillis(endTimeInMillis: Int) = apply {
            this.endTimeInMillis = endTimeInMillis
        }

        /**
         * @param durationInMillis the total duration of the song (=[endTimeInMillis] - [startTimeInMillis])
         */
        fun setDurationInMillis(durationInMillis: Int) = apply {
            this.durationInMillis = durationInMillis
        }

        /**
         * @param coverData the path to the song's cover art image.
         */
        fun setCoverData(coverData: String?) = apply {
            this.coverData = coverData
        }

        /**
         * @return a new [Song] object with the properties set in this class.
         */
        fun build() = Song(id, songData, name, albumName, artistName, genreName, trackNumber,
                yearPublished, startTimeInMillis, endTimeInMillis, durationInMillis, coverData)

        override fun toString() =
                "Builder(id=$id, songData='$songData', name='$name', albumName='$albumName', artistName='$artistName', genreName='$genreName', trackNumber=$trackNumber, yearPublished=$yearPublished, startTimeInMillis=$startTimeInMillis, endTimeInMillis=$endTimeInMillis, durationInMillis=$durationInMillis, coverData=$coverData)"
    }
}