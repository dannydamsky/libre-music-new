package com.damsky.danny.libremusic.ui.activity.main.widget

import android.content.Context
import android.support.annotation.DrawableRes
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import com.damsky.danny.libremusic.GlideApp
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.RoomDbRepository
import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import com.damsky.danny.libremusic.data.db.nonentity.queue.SongQueue
import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnPlaybackInfoChangedListener
import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnSongChangedListener
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService
import kotlinx.android.synthetic.main.widget_now_playing_panel.view.*

/**
 * This View is a panel that contains information regarding the current song in the [SongQueue],
 * as well as playback buttons.
 *
 * @author Danny Damsky
 * @see SongQueue
 * @see OnSongChangedListener
 * @see OnPlaybackInfoChangedListener
 */
class NowPlayingPanel(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs),
        OnSongChangedListener, OnPlaybackInfoChangedListener {

    override var isRegistered: Boolean = false

    private var songQueue: SongQueue? = null
    private var onClickListener: OnClickListener? = null
    @DrawableRes
    private var playPauseResource: Int = 0

    init {
        inflate(context, R.layout.widget_now_playing_panel, this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        songQueue = RoomDbRepository.getInstance(context).songQueue
        songQueue!!.registerOnSongChangedListener(this)
        songQueue!!.registerOnPlaybackInfoChangedListener(this)
    }

    override fun onSongChanged(song: PlayableSong, isPlaying: Boolean, currentSongIndex: Int, maxIndex: Int, playerPositionMillis: Int) =
            updateLayout(song, isPlaying)

    private fun updateLayout(playableSong: PlayableSong, isPlaying: Boolean) {
        glideLoadIntoImageView(playableSong.coverData)
        nowPlayingSongTitle.text = playableSong.name
        nowPlayingSongArtist.text = playableSong.artistName
        togglePlayPauseResource(isPlaying)
        initOnClickListeners()
    }

    private fun togglePlayPauseResource(toggle: Boolean) {
        playPauseResource = if (toggle)
            R.drawable.ic_pause_white_24dp
        else
            R.drawable.ic_play_arrow_white_24dp
        playPause.setImageResource(playPauseResource)
    }

    private fun initOnClickListeners() {
        bottomMain.setOnClickListener { onClickListener?.onNowPlayingPanelSheetClicked() }
        playPrevious.setOnClickListener { MediaPlayerService.previous(context) }
        playNext.setOnClickListener { MediaPlayerService.next(context) }
        playPause.setOnClickListener {
            playPauseResource = if (playPauseResource == R.drawable.ic_pause_white_24dp) {
                MediaPlayerService.pause(context)
                R.drawable.ic_play_arrow_white_24dp
            } else {
                MediaPlayerService.start(context)
                R.drawable.ic_pause_white_24dp
            }
            playPause.setImageResource(playPauseResource)
        }
    }

    private fun glideLoadIntoImageView(coverData: String?) {
        GlideApp.with(this)
                .load(coverData)
                .placeholder(R.drawable.ic_song_rectangle_96dp)
                .into(nowPlayingSongImage)
    }

    override fun onPlaybackStopped() {
        playPauseResource = R.drawable.ic_play_arrow_white_24dp
        playPause.setImageResource(playPauseResource)
    }

    override fun onPlaybackStarted() {
        playPauseResource = R.drawable.ic_pause_white_24dp
        playPause.setImageResource(playPauseResource)
    }

    override fun onQueueResized(index: Int, size: Int) = Unit
    override fun onPlayerPositionChanged(playerPositionMillis: Int) = Unit

    override fun onDetachedFromWindow() {
        songQueue?.unregisterOnSongChangedListener(this)
        songQueue?.unregisterOnPlaybackInfoChangedListener(this)
        songQueue = null
        onClickListener = null
        bottomMain.setOnClickListener(null)
        playPrevious.setOnClickListener(null)
        playNext.setOnClickListener(null)
        playPause.setOnClickListener(null)
        super.onDetachedFromWindow()
    }

    /**
     * Sets a listener to all of the panel's button presses.
     *
     * @param onClickListener the listener to all of the panel's button presses.
     */
    fun setOnClickListener(onClickListener: OnClickListener?) {
        this.onClickListener = onClickListener
    }

    /**
     * This interface contains callbacks from the panel's click events.
     *
     * @author Danny Damsky
     */
    interface OnClickListener {
        /**
         * This function gets called when the panel itself was pressed.
         */
        fun onNowPlayingPanelSheetClicked()
    }
}