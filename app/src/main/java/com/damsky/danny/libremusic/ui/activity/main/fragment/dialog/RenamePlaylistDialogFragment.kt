package com.damsky.danny.libremusic.ui.activity.main.fragment.dialog

import android.arch.lifecycle.ViewModelProviders
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.*
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.RoomDbViewModel
import com.damsky.danny.libremusic.data.db.entity.Playlist
import com.damsky.danny.libremusic.util.CrashUtils
import kotlinx.android.synthetic.main.dialog_rename_playlist.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * This [DialogFragment] class inflates [R.layout.dialog_rename_playlist].
 *
 * To show it, use the [RenamePlaylistDialogFragment.Companion.show] function.
 * The dialog handles the renaming of the playlist independently
 * and doesn't have any callbacks to the activity.
 *
 * @author Danny Damsky
 */
class RenamePlaylistDialogFragment : DialogFragment() {

    companion object {
        private const val TAG =
                "com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.RenamePlaylistDialogFragment.TAG"

        private const val EXTRA_PLAYLIST_ID = "playlist_id"
        private const val EXTRA_PLAYLIST_NAME = "playlist_name"

        /**
         * Shows the [RenamePlaylistDialogFragment] using its own custom tag,
         * and passes it the ID and name of the [Playlist] as arguments.
         *
         * @param fragmentManager the [FragmentManager] of the activity.
         * @param playlist the [Playlist.id] and [Playlist.name] properties get passed over
         * to the [RenamePlaylistDialogFragment].
         */
        @JvmStatic
        fun show(fragmentManager: FragmentManager, playlist: Playlist) {
            val fragment = RenamePlaylistDialogFragment()
            val bundle = Bundle()
            bundle.putLong(EXTRA_PLAYLIST_ID, playlist.id!!)
            bundle.putString(EXTRA_PLAYLIST_NAME, playlist.name)
            fragment.arguments = bundle
            fragment.show(fragmentManager, TAG)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog.setCancelable(false)
        try {
            setWindowProperties(dialog.window!!)
        } catch (e: NullPointerException) {
            CrashUtils.logException(e)
        } catch (e: Exception) {
            CrashUtils.logError(TAG, "Unexpected exception: ${e.message}")
        }
    }

    private fun setWindowProperties(window: Window) {
        window.setLayout((360 * resources.displayMetrics.density).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        window.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.transparent, null)))
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dialog = inflater.inflate(R.layout.dialog_rename_playlist, container, false)
        configureEditText(dialog)
        setPositiveButtonOnClickListener(dialog)
        setNegativeButtonOnClickListener(dialog)
        return dialog
    }

    private fun configureEditText(dialog: View) {
        dialog.editText.requestFocus()
        val playlistName = arguments!!.getString(EXTRA_PLAYLIST_NAME)!!
        dialog.editText.setText(playlistName)
        dialog.editText.setSelection(0, playlistName.length)
    }

    private fun setPositiveButtonOnClickListener(dialog: View) =
            dialog.positiveButton.setOnClickListener {
                if (!dialog.editText.text.isNullOrBlank()) {
                    updatePlaylistName(dialog.editText.text.toString())
                    dismiss()
                } else
                    dialog.warningText.visibility = View.VISIBLE
            }

    private fun updatePlaylistName(playlistName: String) {
        val playlistDao = ViewModelProviders.of(this).get(RoomDbViewModel::class.java).playlistDao
        val playlistId = arguments!!.getLong(EXTRA_PLAYLIST_ID)
        GlobalScope.launch(Dispatchers.IO) { playlistDao.updateNameById(playlistId, playlistName) }
    }

    private fun setNegativeButtonOnClickListener(dialog: View) =
            dialog.negativeButton.setOnClickListener { dismiss() }

}