package com.damsky.danny.libremusic.util

import android.content.ContentValues
import android.content.Context
import android.media.RingtoneManager
import android.provider.MediaStore
import com.damsky.danny.libremusic.data.db.RoomDbRepository
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplaySong
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File

/**
 * Utility class for setting a ringtone.
 *
 * @author Danny Damsky
 */
class RingtoneUtils private constructor() {
    companion object {
        /**
         * Sets a song as the ringtone for the device.
         *
         * @param context the context from where the function is called, this context won't really
         * be used as the function takes the applicationContext from the given context instead.
         * @param song the song to use as a ringtone.
         * @param onSuccess a callback that runs if everything went well.
         */
        @JvmStatic
        fun setRingtone(context: Context, song: DisplaySong, onSuccess: () -> Unit) {
            // Use the much-safer for this operation Application Context
            val applicationContext = context.applicationContext
            GlobalScope.launch(Dispatchers.Main) {
                launch(Dispatchers.IO) {
                    // Get the song data from the database.
                    val songData = RoomDbRepository.getInstance(applicationContext).songDao.getSongDataById(song.id!!)
                    // Build a file object from the song data.
                    val ringtoneFile = File(songData)
                    // Pass the song's information to a ContentValues object.
                    val contentValues = buildContentValues(songData, song.name, ringtoneFile.length(), song.durationInMillis)
                    // Get the URI of the song.
                    val uri = MediaStore.Audio.Media.getContentUriForPath(songData)
                    // Delete the song from the contentResolver.
                    applicationContext.contentResolver.delete(uri, "${MediaStore.MediaColumns.DATA}=?", arrayOf(songData))
                    // Insert the song back to the contentResolver, this time with the new ContentValues
                    // that indicate that this song is a ringtone.
                    val newUri = applicationContext.contentResolver.insert(uri, contentValues)
                    // Set the song as a ringtone via the RingtoneManager.
                    RingtoneManager.setActualDefaultRingtoneUri(applicationContext, RingtoneManager.TYPE_RINGTONE, newUri)
                }.join() // Wait for the asynchronous operation to finish
                onSuccess() // run the higher-order onSuccess function on the main thread.
            }
        }

        /**
         * Sets a song as the ringtone for the device.
         *
         * @param context the context from where the function is called, this context won't really
         * be used as the function takes the applicationContext from the given context instead.
         * @param songId the ID of the song to use as a ringtone.
         * @param onSuccess a callback that runs if everything went well.
         */
        @JvmStatic
        fun setRingtone(context: Context, songId: Long, onSuccess: () -> Unit) {
            // Use the much-safer for this operation Application Context
            val applicationContext = context.applicationContext
            GlobalScope.launch(Dispatchers.Main) {
                launch(Dispatchers.IO) {
                    // Get the RingtoneSong object from the database.
                    val ringtoneSong = RoomDbRepository.getInstance(applicationContext).songDao.getRingtoneSongById(songId)
                    // Build a file object from the song data.
                    val ringtoneFile = File(ringtoneSong.songData)
                    // Pass the song's information to a ContentValues object.
                    val content = buildContentValues(ringtoneSong.songData, ringtoneSong.name, ringtoneFile.length(), ringtoneSong.durationInMillis)
                    // Get the URI of the song.
                    val uri = MediaStore.Audio.Media.getContentUriForPath(ringtoneSong.songData)
                    // Delete the song from the contentResolver.
                    applicationContext.contentResolver.delete(uri, "${MediaStore.MediaColumns.DATA}=?", arrayOf(ringtoneSong.songData))
                    // Insert the song back to the contentResolver, this time with the new ContentValues
                    // that indicate that this song is a ringtone.
                    val newUri = applicationContext.contentResolver.insert(uri, content)
                    // Set the song as a ringtone via the RingtoneManager.
                    RingtoneManager.setActualDefaultRingtoneUri(applicationContext, RingtoneManager.TYPE_RINGTONE, newUri)
                }.join() // Wait for the asynchronous operation to finish
                onSuccess() // run the higher-order onSuccess function on the main thread.
            }
        }

        /**
         * Build new ContentValues for the song, these content values will replace the old ones,
         * and they will add the indicator that the song is a ringtone.
         */
        private fun buildContentValues(songData: String, songName: String, songSize: Long, songDuration: Int): ContentValues {
            val contentValues = ContentValues()
            contentValues.put(MediaStore.MediaColumns.DATA, songData)
            contentValues.put(MediaStore.MediaColumns.TITLE, songName)
            contentValues.put(MediaStore.MediaColumns.SIZE, songSize)
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "audio/*")
            contentValues.put(MediaStore.Audio.Media.DURATION, songDuration)
            contentValues.put(MediaStore.Audio.Media.IS_RINGTONE, true)
            contentValues.put(MediaStore.Audio.Media.IS_NOTIFICATION, false)
            contentValues.put(MediaStore.Audio.Media.IS_ALARM, false)
            contentValues.put(MediaStore.Audio.Media.IS_MUSIC, false)
            return contentValues
        }
    }
}