package com.damsky.danny.libremusic.util.loader

import android.content.ContentResolver
import android.database.Cursor
import android.provider.MediaStore
import com.damsky.danny.libremusic.util.CrashUtils

/**
 * This class is responsible for querying the user's device for cover art image paths.
 *
 * @param contentResolver used to query the user's device for cover art image paths.
 * @author Danny Damsky
 * @see SongLoader
 * @see GenreLoader
 * @constructor queries the user's device for genres.
 */
class CoverArtLoader(private val contentResolver: ContentResolver) {

    private var cursor: Cursor? = null
    private val queryProjection = arrayOf(MediaStore.Audio.Albums.ALBUM_ART)

    /**
     * This function queries the device for a cover art image whose album ID matches the one
     * given in the parameter.
     *
     * @param albumId the ID of the album to query the cover art path image for.
     */
    fun setAlbumId(albumId: Long) {
        cursor = contentResolver.query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                queryProjection, "${MediaStore.Audio.Albums._ID} = ?", arrayOf("$albumId"), null)
    }

    /**
     * Note: this function needs to run after [setAlbumId] was called, otherwise it will not work.
     *
     * @return the path of the cover art image file, or null if it wasn't found.
     */
    fun getCoverArt(): String? = this.cursor.use {
        val queryResult: Boolean = it!!.moveToFirst()
        if (queryResult) {
            try {
                return it.getString(0)
            } catch (e: Exception) {
                CrashUtils.logException(e)
            }
        }
        return null
    }

    override fun toString() = "CoverArtLoader(contentResolver=$contentResolver, cursor=$cursor)"
}