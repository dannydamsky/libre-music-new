package com.damsky.danny.libremusic.ui.activity.main.fragment.media

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.entity.Album
import com.damsky.danny.libremusic.data.db.entity.Artist
import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplaySong
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.AlbumRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.ArtistRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.AbstractSongRowItem
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapter
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks
import com.damsky.danny.libremusic.util.CrashUtils
import com.damsky.danny.libremusic.util.IntentUtils
import com.damsky.danny.libremusic.util.RowItemMenuAction

/**
 * An implementation of [AbstractMediaFragment].
 * This implementation displays a list of [Artist]s and their appropriate albums and songs.
 *
 * @author Danny Damsky
 */
class ArtistsFragment : AbstractMediaFragment() {

    private val artistCallbacks = object : MainActivityRecyclerViewAdapterCallbacks<Artist> {
        override fun onRowClick(item: Artist, itemPosition: Int) =
                setAlbumsAdapterFromArtists(item.name, itemPosition)

        override fun onRowMenuClick(item: Artist, itemPosition: Int, menuAction: RowItemMenuAction) = when (menuAction) {
            RowItemMenuAction.ACTION_PLAY -> callOnArtistCallbacksActionPlay(item.name)
            RowItemMenuAction.ACTION_ADD_TO_QUEUE -> callOnArtistCallbacksAddToQueue(item.name)
            RowItemMenuAction.ACTION_ADD_TO_PLAYLIST -> callOnArtistCallbacksAddToPlaylist(item.name)
            RowItemMenuAction.ACTION_VIEW_ALL_SONGS -> setSongsAdapterFromArtists(item.name, itemPosition)
            RowItemMenuAction.ACTION_SHARE -> callOnArtistCallbacksShare(item.name)
            RowItemMenuAction.ACTION_REMOVE_FROM_QUEUE,
            RowItemMenuAction.ACTION_SET_AS_RINGTONE,
            RowItemMenuAction.ACTION_RENAME_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_FROM_PLAYLIST ->
                onArtistCallbacksInvalidAction(item, itemPosition, menuAction)
        }
    }

    private fun callOnArtistCallbacksActionPlay(artistName: String) =
            onSongCallbacksActionPlay(0) { songSorting ->
                songSorting.getPlayableSongsForArtist(roomDbViewModel.songDao, artistName)
                        as ArrayList<PlayableSong>
            }

    private fun callOnArtistCallbacksAddToQueue(artistName: String) =
            onOtherCallbacksAddToQueue { songSorting ->
                songSorting.getPlayableSongsForArtist(roomDbViewModel.songDao, artistName)
                        as ArrayList<PlayableSong>
            }

    private fun callOnArtistCallbacksAddToPlaylist(artistName: String) =
            onOtherCallbacksAddToPlaylist {
                roomDbViewModel.songDao.getSongIdsForArtist(artistName)
            }

    private fun callOnArtistCallbacksShare(artistName: String) =
            IntentUtils.shareMultipleSongs(mainActivity) {
                roomDbViewModel.songDao.getSongsDataForArtist(artistName)
            }

    private fun onArtistCallbacksInvalidAction(item: Artist, itemPosition: Int, menuAction: RowItemMenuAction) {
        mainActivity.showSnackBarLong(R.string.error_invalid_menu_action)
        CrashUtils.logError("ArtistsFragment.artistCallbacks.onRowMenuClick",
                "INVALID MENU ACTION: menuAction=$menuAction, item=$item, itemPosition=$itemPosition")
    }

    private val albumCallbacks = object : MainActivityRecyclerViewAdapterCallbacks<Album> {
        override fun onRowClick(item: Album, itemPosition: Int) =
                setSongsAdapterFromAlbums(item.name, itemPosition)

        override fun onRowMenuClick(item: Album, itemPosition: Int, menuAction: RowItemMenuAction) = when (menuAction) {
            RowItemMenuAction.ACTION_PLAY -> callOnAlbumCallbacksActionPlay(item.name)
            RowItemMenuAction.ACTION_ADD_TO_QUEUE -> callOnAlbumCallbacksAddToQueue(item.name)
            RowItemMenuAction.ACTION_ADD_TO_PLAYLIST -> callOnAlbumCallbacksAddToPlaylist(item.name)
            RowItemMenuAction.ACTION_VIEW_ALL_SONGS -> setSongsAdapterFromAlbums(item.name, itemPosition)
            RowItemMenuAction.ACTION_SHARE -> callOnAlbumCallbacksShare(item.name)
            RowItemMenuAction.ACTION_REMOVE_FROM_QUEUE,
            RowItemMenuAction.ACTION_SET_AS_RINGTONE,
            RowItemMenuAction.ACTION_RENAME_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_FROM_PLAYLIST ->
                onAlbumCallbacksInvalidAction(item, itemPosition, menuAction)
        }
    }

    private fun callOnAlbumCallbacksActionPlay(albumName: String) =
            onSongCallbacksActionPlay(0) { songSorting ->
                songSorting.getPlayableSongsForAlbum(roomDbViewModel.songDao, albumName)
                        as ArrayList<PlayableSong>
            }

    private fun callOnAlbumCallbacksAddToQueue(albumName: String) =
            onOtherCallbacksAddToQueue { songSorting ->
                songSorting.getPlayableSongsForAlbum(roomDbViewModel.songDao, albumName)
                        as ArrayList<PlayableSong>
            }

    private fun callOnAlbumCallbacksAddToPlaylist(albumName: String) =
            onOtherCallbacksAddToPlaylist {
                roomDbViewModel.songDao.getSongIdsForAlbum(albumName)
            }

    private fun callOnAlbumCallbacksShare(albumName: String) =
            IntentUtils.shareMultipleSongs(mainActivity) {
                roomDbViewModel.songDao.getSongsDataForAlbum(albumName)
            }

    private fun onAlbumCallbacksInvalidAction(item: Album, itemPosition: Int, menuAction: RowItemMenuAction) {
        mainActivity.showSnackBarLong(R.string.error_invalid_menu_action)
        CrashUtils.logError("ArtistsFragment.albumCallbacks.onRowMenuClick",
                "INVALID MENU ACTION: menuAction=$menuAction, item=$item, itemPosition=$itemPosition")
    }

    override val songCallbacks = object : MainActivityRecyclerViewAdapterCallbacks<DisplaySong> {
        override fun onRowClick(item: DisplaySong, itemPosition: Int) =
                callOnSongCallbacksActionPlay(itemPosition, item)

        override fun onRowMenuClick(item: DisplaySong, itemPosition: Int, menuAction: RowItemMenuAction) = when (menuAction) {
            RowItemMenuAction.ACTION_PLAY -> callOnSongCallbacksActionPlay(itemPosition, item)
            RowItemMenuAction.ACTION_ADD_TO_QUEUE -> onSongCallbacksActionAddToQueue(item.id!!)
            RowItemMenuAction.ACTION_ADD_TO_PLAYLIST -> onSongCallbacksActionAddToPlaylist(item.id!!)
            RowItemMenuAction.ACTION_SHARE -> onSongCallbacksActionShare(item.id!!)
            RowItemMenuAction.ACTION_SET_AS_RINGTONE -> onSongCallbacksActionSetAsRingtone(item)
            RowItemMenuAction.ACTION_REMOVE_FROM_QUEUE,
            RowItemMenuAction.ACTION_VIEW_ALL_SONGS,
            RowItemMenuAction.ACTION_RENAME_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_PLAYLIST,
            RowItemMenuAction.ACTION_REMOVE_FROM_PLAYLIST ->
                onSongCallbacksInvalidAction(item, itemPosition, menuAction)
        }
    }

    private fun callOnSongCallbacksActionPlay(itemPosition: Int, item: DisplaySong) =
            onSongCallbacksActionPlay(itemPosition) { songSorting ->
                if (songsFromArtists)
                    songSorting.getPlayableSongsForArtist(roomDbViewModel.songDao, item.artistName)
                            as ArrayList<PlayableSong>
                else
                    songSorting.getPlayableSongsForAlbum(roomDbViewModel.songDao, item.albumName)
                            as ArrayList<PlayableSong>
            }

    private val artistsAdapterObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            super.onItemRangeInserted(positionStart, itemCount)
            if (itemCount != 0 && lastScrollPositionArtists != -1) {
                recyclerView.scrollToPosition(lastScrollPositionArtists)
                lastScrollPositionArtists = -1
            }
        }
    }

    private val albumsAdapterObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
            super.onItemRangeInserted(positionStart, itemCount)
            if (itemCount != 0 && lastScrollPositionAlbums != -1) {
                recyclerView.scrollToPosition(lastScrollPositionAlbums)
                lastScrollPositionAlbums = -1
            }
        }
    }

    private val songsAdapterObserver = object : RecyclerView.AdapterDataObserver() {

    }

    private lateinit var artistsAdapter: MainActivityRecyclerViewAdapter<Artist, ArtistRowItem>
    private lateinit var albumsAdapter: MainActivityRecyclerViewAdapter<Album, AlbumRowItem>
    private lateinit var songsAdapter: MainActivityRecyclerViewAdapter<DisplaySong, AbstractSongRowItem>

    private var lastScrollPositionArtists = -1
    private var lastScrollPositionAlbums = -1
    private var songsFromArtists = false

    override fun onViewStubInflated() {
        initAdapters()
        initRecyclerViewProperties()
        setArtistsAdapter()
    }

    private fun initAdapters() {
        initArtistsAdapter()
        initAlbumsAdapter()
        initSongsAdapter()
    }

    private fun initArtistsAdapter() {
        artistsAdapter = MainActivityRecyclerViewAdapter(ArtistRowItem.DIFF_CALLBACK, artistCallbacks)
        artistsAdapter.setLiveData(roomDbViewModel.artistRowItemsLiveData)
    }

    private fun initAlbumsAdapter() {
        albumsAdapter = MainActivityRecyclerViewAdapter(AlbumRowItem.DIFF_CALLBACK, albumCallbacks)
    }

    private fun initSongsAdapter() {
        songsAdapter = MainActivityRecyclerViewAdapter(AbstractSongRowItem.DIFF_CALLBACK, songCallbacks)
    }

    private fun initRecyclerViewProperties() {
        recyclerView.layoutManager = LinearLayoutManager(mainActivity)
        recyclerView.setHasFixedSize(true)
    }

    private fun setArtistsAdapter() {
        artistsAdapter.registerAdapterDataObserver(artistsAdapterObserver)
        artistsAdapter.startObserving(this)
        recyclerView.adapter = artistsAdapter
    }

    private fun setAlbumsAdapterFromArtists(artistName: String, lastScrollPositionArtists: Int) {
        artistsAdapter.unregisterAdapterDataObserver(artistsAdapterObserver)
        val liveData = roomDbViewModel.getAlbumRowItemsLiveDataForArtist(mainActivity.resources, artistName)
        albumsAdapter.setLiveData(liveData)
        albumsAdapter.registerAdapterDataObserver(albumsAdapterObserver)
        albumsAdapter.startObserving(this)
        recyclerView.adapter = albumsAdapter
        artistsAdapter.stopObserving()
        artistsAdapter.submitList(null)
        this.lastScrollPositionArtists = lastScrollPositionArtists
    }

    private fun setSongsAdapterFromAlbums(albumName: String, lastScrollPositionAlbums: Int) {
        albumsAdapter.unregisterAdapterDataObserver(albumsAdapterObserver)
        val liveData = roomDbViewModel.getSongRowItemsLiveDataForAlbum(albumName)
        songsAdapter.setLiveData(liveData)
        songsAdapter.registerAdapterDataObserver(songsAdapterObserver)
        songsAdapter.startObserving(this)
        recyclerView.adapter = songsAdapter
        albumsAdapter.stopObserving()
        albumsAdapter.submitList(null)
        this.lastScrollPositionAlbums = lastScrollPositionAlbums
    }

    private fun setSongsAdapterFromArtists(artistName: String, lastScrollPositionArtists: Int) {
        artistsAdapter.unregisterAdapterDataObserver(artistsAdapterObserver)
        val liveData = roomDbViewModel.getSongRowItemsLiveDataForArtist(artistName)
        songsAdapter.setLiveData(liveData)
        songsAdapter.registerAdapterDataObserver(songsAdapterObserver)
        songsAdapter.startObserving(this)
        recyclerView.adapter = songsAdapter
        artistsAdapter.stopObserving()
        artistsAdapter.submitList(null)
        this.lastScrollPositionArtists = lastScrollPositionArtists
        songsFromArtists = true
    }

    override fun onBackPressed() = when {
        songsFromArtists -> {
            setArtistsAdapterFromSongs()
            false
        }
        lastScrollPositionAlbums != -1 -> {
            setAlbumsAdapterFromSongs()
            false
        }
        lastScrollPositionArtists != -1 -> {
            setArtistsAdapterFromAlbums()
            false
        }
        else -> true
    }

    private fun setAlbumsAdapterFromSongs() {
        songsAdapter.unregisterAdapterDataObserver(songsAdapterObserver)
        albumsAdapter.registerAdapterDataObserver(albumsAdapterObserver)
        albumsAdapter.startObserving(this)
        recyclerView.adapter = albumsAdapter
        songsAdapter.stopObserving()
        songsAdapter.submitList(null)
    }

    private fun setArtistsAdapterFromAlbums() {
        albumsAdapter.unregisterAdapterDataObserver(albumsAdapterObserver)
        artistsAdapter.registerAdapterDataObserver(artistsAdapterObserver)
        artistsAdapter.startObserving(this)
        recyclerView.adapter = this.artistsAdapter
        albumsAdapter.stopObserving()
        albumsAdapter.submitList(null)
    }

    private fun setArtistsAdapterFromSongs() {
        songsAdapter.unregisterAdapterDataObserver(songsAdapterObserver)
        artistsAdapter.registerAdapterDataObserver(artistsAdapterObserver)
        artistsAdapter.startObserving(this)
        recyclerView.adapter = artistsAdapter
        songsAdapter.stopObserving()
        songsAdapter.submitList(null)
        songsFromArtists = false
    }

    override fun reset() = when {
        songsFromArtists -> {
            lastScrollPositionArtists = -1
            setArtistsAdapterFromSongs()
        }
        lastScrollPositionAlbums != -1 -> setArtistsAdapterFromAlbumSongs()
        lastScrollPositionArtists != -1 -> {
            lastScrollPositionArtists = -1
            setArtistsAdapterFromAlbums()
        }
        else -> recyclerView.smoothScrollToPosition(0)
    }

    private fun setArtistsAdapterFromAlbumSongs() {
        lastScrollPositionAlbums = -1
        lastScrollPositionArtists = -1
        songsAdapter.unregisterAdapterDataObserver(songsAdapterObserver)
        artistsAdapter.registerAdapterDataObserver(artistsAdapterObserver)
        artistsAdapter.startObserving(this)
        recyclerView.adapter = artistsAdapter
        songsAdapter.stopObserving()
        songsAdapter.submitList(null)
    }
}