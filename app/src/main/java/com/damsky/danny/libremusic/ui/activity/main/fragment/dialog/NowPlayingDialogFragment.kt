package com.damsky.danny.libremusic.ui.activity.main.fragment.dialog

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.annotation.DrawableRes
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageButton
import android.widget.SeekBar
import com.damsky.danny.libremusic.GlideApp
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.RoomDbViewModel
import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnPlaybackInfoChangedListener
import com.damsky.danny.libremusic.data.db.nonentity.queue.callback.OnSongChangedListener
import com.damsky.danny.libremusic.data.prefs.SharedPreferencesHelper
import com.damsky.danny.libremusic.service.mediaplayer.MediaPlayerService
import com.damsky.danny.libremusic.util.CrashUtils
import com.damsky.danny.libremusic.util.TimeUtils
import kotlinx.android.synthetic.main.dialog_now_playing.*
import kotlinx.android.synthetic.main.dialog_now_playing.view.*

/**
 * This [DialogFragment] class inflates [R.layout.dialog_now_playing].
 * This dialog provides all of the information about the current song that is selected in the queue
 * and allows the user to interact with playback functionality such as play/pause, skip-to-previous,
 * skip-to-next, enable/disable shuffle and repeat, seek-functionality, etc.
 *
 * The dialog handles the calls to the [MediaPlayerService] independently
 * and doesn't have any callbacks to the activity.
 */
class NowPlayingDialogFragment : DialogFragment(), OnPlaybackInfoChangedListener,
        OnSongChangedListener {
    companion object {
        private const val TAG =
                "com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.NowPlayingDialogFragment"

        /**
         * Shows the [NowPlayingDialogFragment] using its own custom tag.
         *
         * @param fragmentManager the [FragmentManager] of the activity.
         */
        @JvmStatic
        fun show(fragmentManager: FragmentManager) =
                NowPlayingDialogFragment().show(fragmentManager, TAG)
    }

    @DrawableRes
    private var playPauseResource = 0

    override var isRegistered = false
    private var referenceInitComplete = false
    private lateinit var activityContext: Context
    private lateinit var roomDbViewModel: RoomDbViewModel
    private lateinit var sharedPreferencesHelper: SharedPreferencesHelper

    private lateinit var repeatEnabledObserver: Observer<Boolean>
    private lateinit var shuffleEnabledObserver: Observer<Boolean>

    private val onShuffleEnabledOnClickListener = View.OnClickListener {
        sharedPreferencesHelper.putShuffleEnabled(false)
    }

    private val onShuffleDisabledOnClickListener = View.OnClickListener {
        sharedPreferencesHelper.putShuffleEnabled(true)
    }

    private val onRepeatEnabledOnClickListener = View.OnClickListener {
        sharedPreferencesHelper.putRepeatEnabled(false)
    }

    private val onRepeatDisabledOnClickListener = View.OnClickListener {
        sharedPreferencesHelper.putRepeatEnabled(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activityContext = context
        if (!referenceInitComplete) {
            roomDbViewModel = ViewModelProviders.of(this).get(RoomDbViewModel::class.java)
            sharedPreferencesHelper = SharedPreferencesHelper.getInstance(context)
            referenceInitComplete = true
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        try {
            setWindowProperties(dialog.window!!)
        } catch (e: NullPointerException) {
            CrashUtils.logException(e)
        } catch (e: Exception) {
            CrashUtils.logError(TAG, "Unexpected exception: ${e.message}")
        }
    }

    private fun setWindowProperties(window: Window) {
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        window.attributes.windowAnimations = R.style.FullScreenDialogAnim
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dialog = inflater.inflate(R.layout.dialog_now_playing, container, false)
        inflateLayout(dialog)
        return dialog
    }

    private fun inflateLayout(dialog: View) {
        inflateToolbar(dialog)
        inflateRepeatButton(dialog)
        inflateShuffleButton(dialog)
        inflateCallbackButtons(dialog)
    }

    private fun inflateToolbar(dialog: View) {
        dialog.superToolbar.setNavigationIcon(R.drawable.ic_keyboard_arrow_down_white_24dp)
        dialog.superToolbar.setNavigationOnClickListener { dismiss() }
    }

    private fun inflateRepeatButton(dialog: View) {
        repeatEnabledObserver = Observer { repeatEnabled ->
            if (repeatEnabled!!) {
                dialog.repeatButton.setImageResource(R.drawable.ic_repeat_one_accent_24dp)
                dialog.repeatButton.setOnClickListener(onRepeatEnabledOnClickListener)
            } else {
                dialog.repeatButton.setImageResource(R.drawable.ic_repeat_white_24dp)
                dialog.repeatButton.setOnClickListener(onRepeatDisabledOnClickListener)
            }
        }
        sharedPreferencesHelper.getRepeatEnabledLiveData().observe(this, repeatEnabledObserver)
    }

    private fun inflateShuffleButton(dialog: View) {
        shuffleEnabledObserver = Observer { shuffleEnabled ->
            if (shuffleEnabled!!) {
                dialog.shuffleButton.setImageResource(R.drawable.ic_shuffle_accent_24dp)
                dialog.shuffleButton.setOnClickListener(onShuffleEnabledOnClickListener)
            } else {
                dialog.shuffleButton.setImageResource(R.drawable.ic_shuffle_white_24dp)
                dialog.shuffleButton.setOnClickListener(onShuffleDisabledOnClickListener)
            }
        }
        sharedPreferencesHelper.getShuffleEnabledLiveData().observe(this, shuffleEnabledObserver)
    }

    private fun inflateCallbackButtons(dialog: View) {
        dialog.playPreviousButton.setOnClickListener { MediaPlayerService.previous(activityContext) }
        dialog.playNextButton.setOnClickListener { MediaPlayerService.next(activityContext) }
        inflatePlayPauseButton(dialog.playPauseButton)
        inflateSeekBar(dialog.seekBar)
    }

    private fun inflatePlayPauseButton(playPauseButton: ImageButton) =
            playPauseButton.setOnClickListener {
                if (playPauseResource == R.drawable.ic_pause_white_24dp) {
                    onPlaybackStopped()
                    MediaPlayerService.pause(activityContext)
                } else {
                    onPlaybackStarted()
                    MediaPlayerService.start(activityContext)
                }
            }

    private fun inflateSeekBar(seekBar: SeekBar) {
        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    val songStartTimeMillis = roomDbViewModel.songQueue.getCurrentSong().startTimeInMillis
                    val newPlayerPositionMillis = progress + songStartTimeMillis
                    MediaPlayerService.seekTo(activityContext, newPlayerPositionMillis)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit
            override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val songQueue = roomDbViewModel.songQueue
        songQueue.registerOnSongChangedListener(this)
        songQueue.registerOnPlaybackInfoChangedListener(this)
    }

    override fun onPlaybackStopped() {
        playPauseResource = R.drawable.ic_play_arrow_white_24dp
        playPauseButton.setImageResource(playPauseResource)
    }

    override fun onPlaybackStarted() {
        playPauseResource = R.drawable.ic_pause_white_24dp
        playPauseButton.setImageResource(playPauseResource)
    }

    override fun onQueueResized(index: Int, size: Int) {
        inflateNowPlayingTrackNumber(index, size)
    }

    override fun onPlayerPositionChanged(playerPositionMillis: Int) {
        inflateNowPlayingTimeDisplay(playerPositionMillis)
    }

    @SuppressLint("SetTextI18n")
    override fun onSongChanged(song: PlayableSong, isPlaying: Boolean, currentSongIndex: Int, maxIndex: Int, playerPositionMillis: Int) {
        inflateNowPlayingSongImage(song.coverData)
        nowPlayingSongName.text = song.name
        nowPlayingArtistAndAlbumName.text = "${song.artistName} - ${song.albumName}"
        inflateNowPlayingTrackNumber(currentSongIndex, maxIndex)
        inflateNowPlayingTimeDisplay(playerPositionMillis, song.startTimeInMillis, song.durationInMillis)
        togglePlayPauseButtonIcon(isPlaying)
    }

    private fun inflateNowPlayingSongImage(coverData: String?) {
        GlideApp.with(this)
                .load(coverData)
                .placeholder(R.drawable.ic_song_rectangle_96dp)
                .into(nowPlayingSongImage)
    }

    @SuppressLint("SetTextI18n")
    private fun inflateNowPlayingTrackNumber(currentSongIndex: Int, maxIndex: Int) {
        nowPlayingTrackNumber.text = "${currentSongIndex + 1} / $maxIndex"
    }

    @SuppressLint("SetTextI18n")
    private fun inflateNowPlayingTimeDisplay(playerPositionMillis: Int, startTimeMillis: Int, durationMillis: Int) {
        val currentPosition = playerPositionMillis - startTimeMillis
        nowPlayingTimeDisplay.text = getTimeDisplayText(currentPosition, durationMillis)
        seekBar.max = durationMillis
        seekBar.progress = currentPosition
    }

    private fun inflateNowPlayingTimeDisplay(playerPositionMillis: Int) {
        val startTimeMillis = roomDbViewModel.songQueue.getCurrentSong().startTimeInMillis
        val currentPosition = playerPositionMillis - startTimeMillis
        nowPlayingTimeDisplay.text = getTimeDisplayText(currentPosition, seekBar.max)
        seekBar.progress = currentPosition
    }

    private fun getTimeDisplayText(currentPosition: Int, maxPosition: Int): String {
        val currentPositionText = TimeUtils.getFormattedDuration(currentPosition.toLong())
        val maxPositionText = TimeUtils.getFormattedDuration(maxPosition.toLong())
        return "$currentPositionText/$maxPositionText"
    }

    private fun togglePlayPauseButtonIcon(isPlaying: Boolean) = if (isPlaying) {
        onPlaybackStarted()
    } else {
        onPlaybackStopped()
    }

    override fun onDestroyView() {
        sharedPreferencesHelper.getRepeatEnabledLiveData().removeObserver(repeatEnabledObserver)
        sharedPreferencesHelper.getShuffleEnabledLiveData().removeObserver(shuffleEnabledObserver)
        roomDbViewModel.songQueue.unregisterOnSongChangedListener(this)
        roomDbViewModel.songQueue.unregisterOnPlaybackInfoChangedListener(this)
        GlideApp.with(this).clear(nowPlayingSongImage)
        super.onDestroyView()
    }
}