package com.damsky.danny.libremusic.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Parcelable
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import com.damsky.danny.libremusic.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

/**
 * A utility class meant to simplify work with Android [Intent]s.
 *
 * @author Danny Damsky
 * @see Intent
 */
class IntentUtils private constructor() {
    companion object {
        /**
         * This function launches a Chooser [Intent] that allows the user to share a single song
         * to various applications on their device.
         *
         * @param context the context from where to launch the [Intent].
         * @param functionToGetSongData a function that returns a [String] containing the Song data
         * to send. This function gets called on a separate thread.
         */
        @JvmStatic
        fun shareSingleSong(context: Context, functionToGetSongData: () -> String) {
            GlobalScope.launch(Dispatchers.Main) {
                val songToSend = async(Dispatchers.IO) { Uri.parse(functionToGetSongData()) }
                val shareViaString = context.getString(R.string.share_via)
                val sendIntent = Intent(Intent.ACTION_SEND)
                sendIntent.type = "audio/*"
                sendIntent.putExtra(Intent.EXTRA_STREAM, songToSend.await())
                context.startActivity(Intent.createChooser(sendIntent, shareViaString))
            }
        }

        /**
         * This function launches a Chooser [Intent] that allows the user to share a multiple songs
         * to various applications on their device.
         *
         * @param context the context from where to launch the [Intent].
         * @param functionToGetSongData a function that returns a [List] of [String]s containing
         * the Song data to send. This function gets called on a separate thread.
         */
        @JvmStatic
        fun shareMultipleSongs(context: Context, functionToGetSongData: () -> List<String>) {
            GlobalScope.launch(Dispatchers.Main) {
                val songsToSend = async(Dispatchers.IO) {
                    functionToGetSongData().map {
                        Uri.parse(it)
                    } as ArrayList<out Parcelable>
                }
                val shareViaString = context.getString(R.string.share_via)
                val sendIntent = Intent(Intent.ACTION_SEND_MULTIPLE)
                sendIntent.type = "audio/*"
                sendIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, songsToSend.await())
                context.startActivity(Intent.createChooser(sendIntent, shareViaString))
            }
        }

        /**
         * Requests all the permissions in [PermissionUtils.APP_PERMISSIONS] from the user.
         *
         * @param activity the activity that wants to request permissions.
         * @param requestCode the request code that will be returned in [Activity.onRequestPermissionsResult].
         */
        @JvmStatic
        fun requestPermissions(activity: Activity, requestCode: Int) {
            ActivityCompat.requestPermissions(activity, PermissionUtils.APP_PERMISSIONS, requestCode)
        }

        /**
         * Requests the permission to modify system settings from the user.
         *
         * @param activity the activity that wants to request the permission to modify system settings.
         * @param requestCode the request code that will be returned in [Activity.onRequestPermissionsResult]
         */
        @JvmStatic
        fun requestModifySystemSettingsPermission(activity: Activity, requestCode: Int) {
            val permissionRequestIntent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS)
            permissionRequestIntent.data = Uri.parse("package:${activity.packageName}")
            activity.startActivityForResult(permissionRequestIntent, requestCode)
        }
    }
}