package com.damsky.danny.libremusic.ui.activity.main.fragment.dialog

import android.arch.lifecycle.ViewModelProviders
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.*
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.RoomDbViewModel
import com.damsky.danny.libremusic.data.db.entity.Playlist
import com.damsky.danny.libremusic.util.CrashUtils
import kotlinx.android.synthetic.main.dialog_create_playlist.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * This [DialogFragment] class inflates [R.layout.dialog_create_playlist].
 *
 * To show it, use the [CreatePlaylistDialogFragment.Companion.show] function.
 * The dialog handles the creating of the playlist independently
 * and doesn't have any callbacks to the activity.
 *
 * @author Danny Damsky
 */
class CreatePlaylistDialogFragment : DialogFragment() {

    companion object {
        private const val TAG =
                "com.damsky.danny.libremusic.ui.activity.main.fragment.dialog.CreatePlaylistDialogFragment.TAG"

        /**
         * Shows the [CreatePlaylistDialogFragment] using its own custom tag.
         *
         * @param fragmentManager the [FragmentManager] of the activity.
         */
        @JvmStatic
        fun show(fragmentManager: FragmentManager) =
                CreatePlaylistDialogFragment().show(fragmentManager, TAG)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog.setCancelable(false)
        try {
            setWindowProperties(dialog.window!!)
        } catch (e: NullPointerException) {
            CrashUtils.logException(e)
        } catch (e: Exception) {
            CrashUtils.logError(TAG, "Unexpected exception: ${e.message}")
        }
    }

    private fun setWindowProperties(window: Window) {
        window.setLayout((360 * resources.displayMetrics.density).toInt(), ViewGroup.LayoutParams.WRAP_CONTENT)
        window.setBackgroundDrawable(ColorDrawable(resources.getColor(R.color.transparent, null)))
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dialog = inflater.inflate(R.layout.dialog_create_playlist, container, false)
        dialog.editText.requestFocus()
        setPositiveButtonOnClickListener(dialog)
        setNegativeButtonOnClickListener(dialog)
        return dialog
    }

    private fun setPositiveButtonOnClickListener(dialog: View) =
            dialog.positiveButton.setOnClickListener {
                if (!dialog.editText.text.isNullOrBlank()) {
                    createPlaylist(dialog.editText.text.toString())
                    dismiss()
                } else
                    dialog.warningText.visibility = View.VISIBLE
            }

    private fun createPlaylist(playlistName: String) {
        val playlistDao = ViewModelProviders.of(this).get(RoomDbViewModel::class.java).playlistDao
        GlobalScope.launch(Dispatchers.IO) {
            playlistDao.insert(Playlist.Builder().setName(playlistName).build())
        }
    }

    private fun setNegativeButtonOnClickListener(dialog: View) =
            dialog.negativeButton.setOnClickListener { dismiss() }

}