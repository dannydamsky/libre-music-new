package com.damsky.danny.libremusic.data.db.nonentity.display

import android.arch.persistence.room.ColumnInfo
import com.damsky.danny.libremusic.data.db.RoomDbConstants
import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong

/**
 * This is a more compact representation of the song entity.
 * It only holds all the necessary properties for display rather than playback.
 *
 * @author Danny Damsky
 *
 * @param id the ID of the song.
 * @param name the name of the song.
 * @param albumName the name of the song's album.
 * @param artistName the name of the song's artist.
 * @param durationInMillis the duration of the song in milliseconds.
 * @param coverData the song's cover art image path.
 */
data class DisplaySong(
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_ID)
        val id: Long?,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_NAME)
        val name: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_ALBUM_NAME)
        val albumName: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_ARTIST_NAME)
        val artistName: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_DURATION_IN_MILLIS)
        val durationInMillis: Int,
        @ColumnInfo(name = RoomDbConstants.COLUMN_SONG_COVER_DATA)
        val coverData: String?
) {
    constructor(playableSong: PlayableSong) : this(playableSong.id, playableSong.name,
            playableSong.albumName, playableSong.artistName, playableSong.durationInMillis,
            playableSong.coverData)
}