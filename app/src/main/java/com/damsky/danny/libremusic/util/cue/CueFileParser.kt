package com.damsky.danny.libremusic.util.cue

import com.damsky.danny.libremusic.data.db.RoomDbConstants
import com.damsky.danny.libremusic.data.db.entity.Album
import com.damsky.danny.libremusic.data.db.entity.Artist
import com.damsky.danny.libremusic.data.db.entity.Genre
import com.damsky.danny.libremusic.data.db.entity.Song
import java.io.BufferedInputStream
import java.io.FileInputStream
import java.nio.charset.Charset
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

/**
 * This class is in charge of parsing a cue sheet and moving over the sheet's information with
 * cursor-like behaviour.
 *
 * @param encoding the encoding that will be used when parsing the cue sheet.
 * @author Danny Damsky
 */
class CueFileParser(encoding: String) {

    private val charset = Charset.forName(encoding)
    private val cueSheetPattern = Pattern.compile("cuesheet=")
    private val performerPattern = Pattern.compile("PERFORMER\\s\"(.*)\"|PERFORMER\\s(.*)")
    private val genrePattern = Pattern.compile("GENRE\\s\"(.*)\"|GENRE\\s(.*)")
    private val titlePattern = Pattern.compile("TITLE\\s\"(.*)\"|TITLE\\s(.*)")
    private val datePattern = Pattern.compile("DATE\\s(\\d+)")
    private val indexPattern = Pattern.compile("INDEX 01 (\\d\\d:\\d\\d:\\d\\d)")
    private val timePattern = Pattern.compile("\\d+")

    private lateinit var cueFile: CueFile
    private var cueFileReadable = false
    private lateinit var cueFileContent: String

    private lateinit var songTitles: ArrayList<String>
    private lateinit var artistName: String
    private lateinit var albumName: String
    private lateinit var genreName: String
    private lateinit var songPlayTimes: ArrayList<Int>
    private var yearPublished = 0
    private var index = -1
    private var songTitlesSize = 0
    private var songTitlesSizeMinusOne = 0

    private val stringBuilder = StringBuilder(500_000)
    private val contents = ByteArray(4_096)

    /**
     * Prepares the [CueFileParser] to parse the given object.
     *
     * @param cueFile the data source, this object will be parsed.
     */
    fun setDataSource(cueFile: CueFile) {
        this.cueFile = cueFile
        cueFileContent = readFile()
        cueFileReadable = if (cueFile.cueFileEqualsSongFilePath())
            cueSheetPattern.matcher(cueFileContent).find()
        else
            true
    }

    private fun readFile(): String {
        BufferedInputStream(FileInputStream(cueFile.file)).use {
            var bytesRead = it.read(contents)
            while (bytesRead != -1 && stringBuilder.length < 500_000) {
                stringBuilder.append(kotlin.text.String(contents, 0, bytesRead, charset))
                bytesRead = it.read(contents)
            }
        }
        val fileString = stringBuilder.toString()
        stringBuilder.clear()
        return fileString
    }

    /**
     * @return whether or not it is possible to parse the file given on [setDataSource].
     */
    fun cueFileReadable() = cueFileReadable

    /**
     * Prepares all of the class' cue-parsing tools for querying.
     */
    fun prepare() {
        songTitles = getSongTitlesByPattern()
        artistName = getArtistNameByPattern()
        albumName = songTitles[0]
        songTitles.removeAt(0)
        genreName = getGenreNameByPattern()
        songPlayTimes = getSongPlayTimesByPattern()
        yearPublished = getYearPublishedByPattern()
        index = -1
        songTitlesSize = songTitles.size
        songTitlesSizeMinusOne = songTitlesSize - 1
    }

    /**
     * Moves on to the next row in the cue sheet.
     *
     * @return true if the move succeeded and the next row is available, false otherwise.
     */
    fun moveToNext() = if (index < songTitlesSizeMinusOne) {
        index++
        true
    } else
        false

    /**
     * Builds a [Song] object from the information in the current row of the cue sheet.
     *
     * @return a new [Song] object.
     */
    fun buildSong(): Song {
        val indexPlusOne = index + 1
        val endTime = songPlayTimes[indexPlusOne]
        val startTime = songPlayTimes[index]
        return Song.Builder()
                .setSongData(cueFile.songFilePath)
                .setName(songTitles[index])
                .setAlbumName(albumName)
                .setArtistName(artistName)
                .setGenreName(genreName)
                .setTrackNumber(indexPlusOne)
                .setYearPublished(yearPublished)
                .setStartTimeInMillis(startTime)
                .setEndTimeInMillis(endTime)
                .setDurationInMillis(endTime - startTime)
                .setCoverData(cueFile.coverArtPath)
                .build()
    }

    /**
     * Builds an [Artist] object from the information in the current row of the cue sheet.
     *
     * @return a new [Artist] object.
     */
    fun buildArtist() = Artist.Builder()
            .setName(artistName)
            .build()

    /**
     * Builds an [Album] object from the information in the current row of the cue sheet.
     *
     * @return a new [Album] object.
     */
    fun buildAlbum() = Album.Builder()
            .setName(albumName)
            .setArtistName(artistName)
            .setYearPublished(yearPublished)
            .setCoverData(cueFile.coverArtPath)
            .build()

    /**
     * Builds an [Genre] object from the information in the current row of the cue sheet.
     *
     * @return a new [Genre] object.
     */
    fun buildGenre() = Genre.Builder()
            .setName(genreName)
            .build()

    private fun getSongTitlesByPattern(): ArrayList<String> {
        val titlePatternMatcher = titlePattern.matcher(cueFileContent)
        val titlesList = ArrayList<String>()

        while (titlePatternMatcher.find()) {
            val matcherGroupOne = titlePatternMatcher.group(1)
            titlesList.add(matcherGroupOne ?: titlePatternMatcher.group(2))
        }

        return titlesList
    }

    private fun getArtistNameByPattern() = getFirstResult(performerPattern)

    private fun getGenreNameByPattern() = getFirstResult(this.genrePattern)

    private fun getFirstResult(pattern: Pattern): String {
        val matcher = pattern.matcher(this.cueFileContent)
        return if (matcher.find())
            matcher.group(1) ?: matcher.group(2)
        else
            RoomDbConstants.DEFAULT_NAME
    }

    private fun getSongPlayTimesByPattern(): ArrayList<Int> {
        val indexPatternMatcher = indexPattern.matcher(cueFileContent)
        val songPlayTimes = ArrayList<Int>(1)
        songPlayTimes.add(0)
        indexPatternMatcher.find()
        while (indexPatternMatcher.find())
            songPlayTimes.add(convertToMillisecondsFromPattern(indexPatternMatcher.group(1)))
        songPlayTimes.add(cueFile.durationInMillis)
        return songPlayTimes
    }

    private fun convertToMillisecondsFromPattern(timeFormat: String): Int {
        val timePatternMatcher = timePattern.matcher(timeFormat)
        timePatternMatcher.find()
        val minutes = timePatternMatcher.group().toLong()
        timePatternMatcher.find()
        val seconds = timePatternMatcher.group().toLong()
        timePatternMatcher.find()
        val millis = timePatternMatcher.group().toLong()
        return (TimeUnit.MINUTES.toMillis(minutes) + TimeUnit.SECONDS.toMillis(seconds) + millis).toInt()
    }

    private fun getYearPublishedByPattern(): Int {
        val getDate = datePattern.matcher(cueFileContent)
        return if (getDate.find())
            getDate.group(1).toInt()
        else
            RoomDbConstants.YEAR_PUBLISHED_NONE
    }

    override fun toString() =
            "CueFileParser(charset=$charset, cueSheetPattern=$cueSheetPattern, performerPattern=$performerPattern, genrePattern=$genrePattern, titlePattern=$titlePattern, datePattern=$datePattern, indexPattern=$indexPattern, timePattern=$timePattern, cueFile=$cueFile, cueFileReadable=$cueFileReadable, cueFileContent='$cueFileContent', songTitles=$songTitles, artistName='$artistName', albumName='$albumName', genreName='$genreName', songPlayTimes=$songPlayTimes, yearPublished=$yearPublished, index=$index, songTitlesSize=$songTitlesSize, songTitlesSizeMinusOne=$songTitlesSizeMinusOne)"
}
