package com.damsky.danny.libremusic.data.db.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Query
import com.damsky.danny.libremusic.data.db.RoomDbConstants
import com.damsky.danny.libremusic.data.db.entity.Genre
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayGenre

/**
 * An interface that contains all the functions to be implemented by Room for working with
 * the genres table in the database.
 *
 * @author Danny Damsky
 * @see Genre
 * @see BaseDao
 */
@Dao
interface GenreDao : BaseDao<Genre> {

    //******************************************************************//
    //******************GENERAL**DAO**FUNCTIONS************************//
    //****************************************************************//

    /**
     * Empties the genres table in the database.
     */
    @Query("DELETE FROM GENRES;")
    fun deleteAll()

    /**
     * @return a [LiveData] of a [List] of [DisplayGenre]s. This LiveData represents all genres in
     * the database, it includes all of [Genre]'s columns as well as another column representing the
     * [Genre]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_GENRE_NAME=GENRE_NAME) as ${RoomDbConstants.COUNT_SONGS} FROM GENRES;")
    fun getAllDisplayLive(): LiveData<List<DisplayGenre>>

    /**
     * @return a [LiveData] of a [List] of [DisplayGenre]s ordered by the name of the genre in ascending order.
     * This LiveData represents all genres in the database,
     * it includes all of [Genre]'s columns as well as another column representing the [Genre]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_GENRE_NAME=GENRE_NAME) as ${RoomDbConstants.COUNT_SONGS} FROM GENRES ORDER BY GENRE_NAME ASC;")
    fun getAllDisplayOrderedByNameAscLive(): LiveData<List<DisplayGenre>>

    /**
     * @return a [LiveData] of a [List] of [DisplayGenre]s ordered by the name of the genre in descending order.
     * This LiveData represents all genres in the database,
     * it includes all of [Genre]'s columns as well as another column representing the [Genre]'s songs count.
     */
    @Query("SELECT *, (SELECT COUNT(SONG_ID) FROM SONGS WHERE SONG_GENRE_NAME=GENRE_NAME) as ${RoomDbConstants.COUNT_SONGS} FROM GENRES ORDER BY GENRE_NAME DESC;")
    fun getAllDisplayOrderedByNameDescLive(): LiveData<List<DisplayGenre>>
}