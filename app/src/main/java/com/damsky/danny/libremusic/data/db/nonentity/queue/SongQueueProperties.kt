package com.damsky.danny.libremusic.data.db.nonentity.queue

import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import kotlin.random.Random

/**
 * This class includes all of the properties required by [SongQueue] as well as methods
 * for working with them.
 *
 * @author Danny Damsky
 */
class SongQueueProperties {
    private var playableSongs: ArrayList<PlayableSong>? = null
    private var currentSongIndex = 0
    private var playerPositionMillis = 0
    private var currentSongIsPlaying = false
    private var currentSong: PlayableSong? = null

    /**
     * @return the currently selected song in the queue.
     */
    fun getCurrentSong() = currentSong

    /**
     * @return true if the currently selected song is playing, false if it is paused.
     */
    fun currentSongIsPlaying() = currentSongIsPlaying

    /**
     * @return the index of the currently selected song.
     */
    fun getCurrentSongIndex() = currentSongIndex

    /**
     * @return the total amount of songs in the queue.
     */
    fun getSongsTotal(): Int {
        playableSongs?.let { return it.size }
        return 0
    }

    /**
     * @return the current seek position of the media player.
     */
    fun getPlayerPositionMillis() = playerPositionMillis

    /**
     * @return the [ArrayList] of the queue songs.
     */
    fun getPlayableSongs() = playableSongs

    /**
     * Modifies the [SongQueueProperties] object with the information that the current song is playing.
     */
    fun setCurrentSongIsPlaying() {
        currentSongIsPlaying = true
    }

    /**
     * Modifies the [SongQueueProperties] object with the information that the current song is paused.
     */
    fun setCurrentSongIsNotPlaying() {
        currentSongIsPlaying = false
    }

    /**
     * @param playerPositionMillis the current seek position of the media player.
     */
    fun setPlayerPositionMillis(playerPositionMillis: Int) {
        this.playerPositionMillis = playerPositionMillis
    }

    /**
     * @param playableSongs the new songs to set as the queue.
     * @param currentSongIndex the index of the currently selected song in [playableSongs].
     */
    fun setSongs(playableSongs: ArrayList<PlayableSong>, currentSongIndex: Int = 0) {
        this.playableSongs = playableSongs
        this.currentSongIndex = currentSongIndex
        updateCurrentSong()
    }

    /**
     * @param playableSong the new song to add to the end of the queue.
     * @return true if the item has been added, false if it already exists in the queue.
     */
    fun addSong(playableSong: PlayableSong) = if (playableSongs == null) {
        playableSongs = ArrayList()
        playableSongs!!.add(playableSong)
    } else if (!playableSongs!!.contains(playableSong))
        playableSongs!!.add(playableSong)
    else
        false

    /**
     * @param songs the new songs to add to the end of the queue.
     * @return true if new items have been added to the queue, false if these items have all
     * already existed in the queue.
     */
    fun addSongs(songs: ArrayList<PlayableSong>): Boolean {
        var playableSongsChanged = false
        if (playableSongs == null) {
            playableSongs = songs
            playableSongsChanged = true
        } else {
            songs.forEach {
                if (!playableSongs!!.contains(it)) {
                    playableSongs!!.add(it)
                    playableSongsChanged = true
                }
            }
        }
        return playableSongsChanged
    }

    /**
     * Modifies the [SongQueueProperties] to inform it that the song changed to the previous one
     * in the queue.
     */
    fun skipToPreviousSong() {
        if (currentSongIndex == 0) {
            currentSongIndex = playableSongs!!.size - 1
        } else {
            currentSongIndex--
        }
        updateCurrentSong()
    }

    /**
     * Modifies the [SongQueueProperties] to inform it that the song changed to the next one
     * in the queue.
     */
    fun skipToNextSong() {
        if (currentSongIndex == playableSongs!!.size - 1) {
            currentSongIndex = 0
        } else {
            currentSongIndex++
        }
        updateCurrentSong()
    }

    /**
     * Modifies the [SongQueueProperties] to inform it that a random song has been selected.
     * Note: This method does the job of selecting the random song.
     */
    fun skipToRandomSong() {
        currentSongIndex = Random.nextInt(playableSongs!!.size)
        updateCurrentSong()
    }

    /**
     * Modifies the [SongQueueProperties] with a specific song index and informs it that the song
     * has been changed to the song at the [currentSongIndex].
     *
     * @param currentSongIndex the index of the newly selected song.
     */
    fun setCurrentSongIndex(currentSongIndex: Int) {
        this.currentSongIndex = currentSongIndex
        updateCurrentSong()
    }

    /**
     * @param index the position of the song to remove from the queue.
     */
    fun removeSongAt(index: Int) {
        playableSongs!!.removeAt(index)
        if (currentSongIndex == index) {
            skipToPreviousSong()
        }
    }

    private fun updateCurrentSong() {
        currentSong = playableSongs!![currentSongIndex]
        playerPositionMillis = currentSong!!.startTimeInMillis
    }

    override fun toString() =
            "SongQueueProperties(playableSongs=$playableSongs, currentSongIndex=$currentSongIndex, playerPositionMillis=$playerPositionMillis, currentSongIsPlaying=$currentSongIsPlaying, currentSong=$currentSong)"
}