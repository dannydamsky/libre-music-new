package com.damsky.danny.libremusic.data.db

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.content.Context
import android.content.res.Resources
import com.damsky.danny.libremusic.data.db.nonentity.display.*
import com.damsky.danny.libremusic.data.db.nonentity.queue.SongQueue
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.AlbumRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.ArtistRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.GenreRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.PlaylistRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.AbstractSongRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.PlaylistSongRowItem
import com.damsky.danny.libremusic.data.db.nonentity.rowitem.song.SongRowItem
import com.damsky.danny.libremusic.data.prefs.SharedPreferencesHelper

/**
 * This is the repository class, a singleton that implements all of the methods in [IRoomDbRepository].
 * These methods are used for working with the database.
 *
 * @author Danny Damsky
 */
class RoomDbRepository private constructor(context: Context) : IRoomDbRepository {
    companion object {
        private var INSTANCE: RoomDbRepository? = null

        /**
         * @param context the function accesses the applicationContext property.
         * @return an instance of the [RoomDbRepository] class. If the instance doesn't exist yet
         * it is created and returned.
         */
        @JvmStatic
        fun getInstance(context: Context): RoomDbRepository {
            if (INSTANCE == null)
                synchronized(RoomDbRepository::class) {
                    if (INSTANCE == null)
                        INSTANCE = RoomDbRepository(context.applicationContext)
                }
            return INSTANCE!!
        }
    }

    private val sharedPreferencesHelper = SharedPreferencesHelper.getInstance(context)
    override val roomDb = RoomDb.getInstance(context)
    override val artistDao = roomDb.getArtistDao()
    override val albumDao = roomDb.getAlbumDao()
    override val songDao = roomDb.getSongDao()
    override val genreDao = roomDb.getGenreDao()
    override val playlistDao = roomDb.getPlaylistDao()
    override val songsPlaylistsLinkDao = roomDb.getSongsPlaylistsLinkDao()
    override val songQueue = SongQueue(sharedPreferencesHelper)

    private val displayArtistsLiveData = buildDisplayArtistsLiveData()
    private val displayAlbumsLiveData = buildDisplayAlbumsLiveData()
    private val songsLiveData = buildDisplaySongsLiveData()
    private val displayGenresLiveData = buildDisplayGenresLiveData()
    private val displayPlaylistsLiveData = buildDisplayPlaylistsLiveData()

    override val artistRowItemsLiveData: LiveData<List<ArtistRowItem>>
    override val albumRowItemsLiveData: LiveData<List<AlbumRowItem>>
    override val songRowItemsLiveData: LiveData<List<AbstractSongRowItem>>
    override val genreRowItemsLiveData: LiveData<List<GenreRowItem>>
    override val playlistRowItemsLiveData: LiveData<List<PlaylistRowItem>>

    init {
        val resources = context.resources
        this.artistRowItemsLiveData = ArtistRowItem.getLiveData(resources, displayArtistsLiveData)
        this.albumRowItemsLiveData = AlbumRowItem.getLiveData(resources, displayAlbumsLiveData)
        this.songRowItemsLiveData = SongRowItem.getLiveData(songsLiveData)
        this.genreRowItemsLiveData = GenreRowItem.getLiveData(resources, displayGenresLiveData)
        this.playlistRowItemsLiveData = PlaylistRowItem.getLiveData(resources, displayPlaylistsLiveData)
    }

    private fun buildDisplayArtistsLiveData():
            LiveData<List<DisplayArtist>> {
        val artistSortingLiveData = sharedPreferencesHelper.getArtistSortingLiveData()
        return Transformations.switchMap(artistSortingLiveData) { input ->
            input.getDisplayArtistsLiveData(artistDao)
        }
    }

    private fun buildDisplayAlbumsLiveData():
            LiveData<List<DisplayAlbum>> {
        val albumSortingLiveData = sharedPreferencesHelper.getAlbumSortingLiveData()
        return Transformations.switchMap(albumSortingLiveData) { input ->
            input.getDisplayAlbumsLiveData(albumDao)
        }
    }

    private fun buildDisplaySongsLiveData():
            LiveData<List<DisplaySong>> {
        val songSortingLiveData = sharedPreferencesHelper.getSongSortingLiveData()
        return Transformations.switchMap(songSortingLiveData) { input ->
            input.getDisplaySongsLiveData(songDao)
        }
    }

    private fun buildDisplayGenresLiveData():
            LiveData<List<DisplayGenre>> {
        val genreSortingLiveData = sharedPreferencesHelper.getGenreSortingLiveData()
        return Transformations.switchMap(genreSortingLiveData) { input ->
            input.getDisplayGenresLiveData(genreDao)
        }
    }

    private fun buildDisplayPlaylistsLiveData():
            LiveData<List<DisplayPlaylist>> {
        val playlistSortingLiveData = sharedPreferencesHelper.getPlaylistSortingLiveData()
        return Transformations.switchMap(playlistSortingLiveData) { input ->
            input.getDisplayPlaylistsLiveData(playlistDao)
        }
    }

    override fun getAlbumRowItemsLiveDataForArtist(resources: Resources, artistName: String): LiveData<List<AlbumRowItem>> {
        val albumSorting = sharedPreferencesHelper.getAlbumSorting()
        val albumsDisplay = albumSorting.getDisplayAlbumsForArtistLiveData(albumDao, artistName)
        return AlbumRowItem.getLiveData(resources, albumsDisplay)
    }

    override fun getSongRowItemsLiveDataForArtist(artistName: String): LiveData<List<AbstractSongRowItem>> {
        val songSorting = sharedPreferencesHelper.getSongSorting()
        val songsForArtistLiveData = songSorting.getDisplaySongsForArtistLiveData(songDao, artistName)
        return SongRowItem.getLiveData(songsForArtistLiveData)
    }

    override fun getSongRowItemsLiveDataForAlbum(albumName: String): LiveData<List<AbstractSongRowItem>> {
        val songSorting = sharedPreferencesHelper.getSongSorting()
        val songsForAlbumLiveData = songSorting.getDisplaySongsForAlbumLiveData(songDao, albumName)
        return SongRowItem.getLiveData(songsForAlbumLiveData)
    }

    override fun getSongRowItemsLiveDataForGenre(genreName: String): LiveData<List<AbstractSongRowItem>> {
        val songSorting = sharedPreferencesHelper.getSongSorting()
        val songsForGenreLiveData = songSorting.getDisplaySongsForGenreLiveData(songDao, genreName)
        return SongRowItem.getLiveData(songsForGenreLiveData)
    }

    override fun getSongRowItemsLiveDataForPlaylist(playlistId: Long): LiveData<List<AbstractSongRowItem>> {
        val songSorting = sharedPreferencesHelper.getSongSorting()
        val songsForPlaylistLiveData = songSorting.getDisplaySongsForPlaylistLiveData(songsPlaylistsLinkDao, playlistId)
        return PlaylistSongRowItem.getLiveData(songsForPlaylistLiveData)
    }

    override fun deleteAll() = roomDb.runInTransaction {
        songsPlaylistsLinkDao.deleteAll()
        playlistDao.deleteAll()
        artistDao.deleteAll()
        albumDao.deleteAll()
        genreDao.deleteAll()
        songDao.deleteAll()
    }

    override fun toString() =
            "RoomDbRepository(sharedPreferencesHelper=$sharedPreferencesHelper, roomDb=$roomDb, artistDao=$artistDao, albumDao=$albumDao, songDao=$songDao, genreDao=$genreDao, playlistDao=$playlistDao, songsPlaylistsLinkDao=$songsPlaylistsLinkDao, songQueue=$songQueue, displayArtistsLiveData=$displayArtistsLiveData, displayAlbumsLiveData=$displayAlbumsLiveData, songsLiveData=$songsLiveData, displayGenresLiveData=$displayGenresLiveData, displayPlaylistsLiveData=$displayPlaylistsLiveData, artistRowItemsLiveData=$artistRowItemsLiveData, albumRowItemsLiveData=$albumRowItemsLiveData, songRowItemsLiveData=$songRowItemsLiveData, genreRowItemsLiveData=$genreRowItemsLiveData, playlistRowItemsLiveData=$playlistRowItemsLiveData)"
}
