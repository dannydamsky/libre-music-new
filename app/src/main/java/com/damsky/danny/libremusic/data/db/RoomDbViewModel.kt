package com.damsky.danny.libremusic.data.db

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.content.res.Resources

/**
 * This is the [AndroidViewModel] class containing methods for working with the Room database.
 *
 * @author Danny Damsky
 */
class RoomDbViewModel(application: Application) : AndroidViewModel(application), IRoomDbRepository {

    private val roomDbRepository = RoomDbRepository.getInstance(application)
    override val roomDb = roomDbRepository.roomDb
    override val artistDao = roomDbRepository.artistDao
    override val albumDao = roomDbRepository.albumDao
    override val songDao = roomDbRepository.songDao
    override val genreDao = roomDbRepository.genreDao
    override val playlistDao = roomDbRepository.playlistDao
    override val songsPlaylistsLinkDao = roomDbRepository.songsPlaylistsLinkDao
    override val songQueue = roomDbRepository.songQueue
    override val artistRowItemsLiveData = roomDbRepository.artistRowItemsLiveData
    override val albumRowItemsLiveData = roomDbRepository.albumRowItemsLiveData
    override val songRowItemsLiveData = roomDbRepository.songRowItemsLiveData
    override val playlistRowItemsLiveData = roomDbRepository.playlistRowItemsLiveData
    override val genreRowItemsLiveData = roomDbRepository.genreRowItemsLiveData

    override fun deleteAll() = roomDbRepository.deleteAll()

    override fun getAlbumRowItemsLiveDataForArtist(resources: Resources, artistName: String) =
            roomDbRepository.getAlbumRowItemsLiveDataForArtist(resources, artistName)

    override fun getSongRowItemsLiveDataForArtist(artistName: String) =
            roomDbRepository.getSongRowItemsLiveDataForArtist(artistName)

    override fun getSongRowItemsLiveDataForAlbum(albumName: String) =
            roomDbRepository.getSongRowItemsLiveDataForAlbum(albumName)

    override fun getSongRowItemsLiveDataForGenre(genreName: String) =
            roomDbRepository.getSongRowItemsLiveDataForGenre(genreName)

    override fun getSongRowItemsLiveDataForPlaylist(playlistId: Long) =
            roomDbRepository.getSongRowItemsLiveDataForPlaylist(playlistId)

    override fun toString() =
            "RoomDbViewModel(roomDbRepository=$roomDbRepository, roomDb=$roomDb, artistDao=$artistDao, albumDao=$albumDao, songDao=$songDao, genreDao=$genreDao, playlistDao=$playlistDao, songsPlaylistsLinkDao=$songsPlaylistsLinkDao, songQueue=$songQueue, artistRowItemsLiveData=$artistRowItemsLiveData, albumRowItemsLiveData=$albumRowItemsLiveData, songRowItemsLiveData=$songRowItemsLiveData, playlistRowItemsLiveData=$playlistRowItemsLiveData, genreRowItemsLiveData=$genreRowItemsLiveData)"
}