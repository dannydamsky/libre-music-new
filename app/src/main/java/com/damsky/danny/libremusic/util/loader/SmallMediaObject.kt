package com.damsky.danny.libremusic.util.loader

/**
 * An object consisting of some important audio file information.
 *
 * @author Danny Damsky
 * @see BigMediaObject
 *
 * @property songData the data of the current audio file.
 * @property coverArtPath the path to the audio file's cover art image.
 * @property durationInMillis the total duration of the audio file in milliseconds.
 */
data class SmallMediaObject(
        val songData: String,
        val coverArtPath: String?,
        val durationInMillis: Int
)