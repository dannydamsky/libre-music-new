package com.damsky.danny.libremusic.data.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import com.damsky.danny.libremusic.data.db.RoomDbConstants

/**
 * This class represents the playlists table in the database.
 *
 * Relationships:
 * Playlists M-N Songs
 *
 * @author Danny Damsky
 *
 * @param id the ID of the playlist (auto-increment)
 * @param name the name of the playlist.
 * @param coverData the path to the playlist's cover art image.
 */
@Entity(tableName = RoomDbConstants.TABLE_NAME_PLAYLISTS,
        indices = [Index(value = [RoomDbConstants.COLUMN_PLAYLIST_NAME], unique = true)])
data class Playlist(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = RoomDbConstants.COLUMN_PLAYLIST_ID)
        val id: Long?,
        @ColumnInfo(name = RoomDbConstants.COLUMN_PLAYLIST_NAME)
        val name: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_PLAYLIST_COVER_DATA)
        val coverData: String?) {
    class Builder {
        private var id: Long? = null
        private lateinit var name: String
        private var coverData: String? = null

        /**
         * @param id the ID of the playlist.
         */
        fun setId(id: Long?) = apply {
            this.id = id
        }

        /**
         * @param name the name of the playlist.
         */
        fun setName(name: String) = apply {
            this.name = name
        }

        /**
         * @param coverData the path to the playlist's cover art image.
         */
        fun setCoverData(coverData: String?) = apply {
            this.coverData = coverData
        }

        /**
         * @return a new [Playlist] object with the properties set in this class.
         */
        fun build() = Playlist(id, name, coverData)

        override fun toString() = "Builder(id=$id, name='$name', coverData=$coverData)"

    }
}
