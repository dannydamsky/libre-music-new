package com.damsky.danny.libremusic.ui.activity.main.adapter

import android.support.design.widget.TabLayout
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.util.SparseArray
import android.view.ViewGroup
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.ui.activity.main.MainActivity
import com.damsky.danny.libremusic.ui.activity.main.adapter.MainActivityViewPagerAdapter.Companion.setup
import com.damsky.danny.libremusic.ui.activity.main.fragment.media.*
import com.damsky.danny.libremusic.util.Constants
import java.util.*

/**
 * The adapter for MainActivity's [ViewPager].
 * It holds all fragments that extend [AbstractMediaFragment].
 * Use [setup] in order to obtain a preconfigured instance of this class.
 *
 * @author Danny Damsky
 */
class MainActivityViewPagerAdapter private constructor(mainActivity: MainActivity) :
        FragmentStatePagerAdapter(mainActivity.supportFragmentManager) {

    companion object {
        const val FRAGMENT_QUEUE = 0
        const val FRAGMENT_ARTISTS = 1
        const val FRAGMENT_ALBUMS = 2
        const val FRAGMENT_SONGS = 3
        const val FRAGMENT_GENRES = 4
        const val FRAGMENT_PLAYLISTS = 5
        const val FRAGMENTS_COUNT = 6

        /**
         * @param mainActivity the [MainActivity], where the [ViewPager] is used. It is required
         * to instantiate the [MainActivityViewPagerAdapter].
         * @param tabLayout the [MainActivity]'s [TabLayout], the [TabLayout] gets inflated
         * with appropriate icons in every tab.
         * @param viewPager the [MainActivity]'s [ViewPager], it will be setup with the
         * [MainActivityViewPagerAdapter].
         * @return a preconfigured [MainActivityViewPagerAdapter] instance.
         */
        @JvmStatic
        fun setup(mainActivity: MainActivity, tabLayout: TabLayout, viewPager: ViewPager):
                MainActivityViewPagerAdapter {
            viewPager.offscreenPageLimit = determinePagerOffscreenLimit()
            val viewPagerAdapter = MainActivityViewPagerAdapter(mainActivity)
            viewPager.adapter = viewPagerAdapter
            tabLayout.setupWithViewPager(viewPager)
            setIcons(tabLayout)
            viewPager.currentItem = FRAGMENT_ARTISTS
            return viewPagerAdapter
        }

        @JvmStatic
        private fun determinePagerOffscreenLimit(): Int {
            val yearDiff = Constants.DEVICE_YEAR_OPTIMAL - Constants.DEVICE_YEAR
            if (yearDiff >= 0)
                return FRAGMENTS_COUNT - yearDiff
            return FRAGMENTS_COUNT
        }

        @JvmStatic
        private fun setIcons(tabLayout: TabLayout) {
            tabLayout.getTabAt(FRAGMENT_QUEUE)!!.setIcon(R.drawable.ic_queue_music_white_24dp)
            tabLayout.getTabAt(FRAGMENT_ARTISTS)!!.setIcon(R.drawable.ic_artist_white_24dp)
            tabLayout.getTabAt(FRAGMENT_ALBUMS)!!.setIcon(R.drawable.ic_album_white_24dp)
            tabLayout.getTabAt(FRAGMENT_SONGS)!!.setIcon(R.drawable.ic_song_white_24dp)
            tabLayout.getTabAt(FRAGMENT_GENRES)!!.setIcon(R.drawable.ic_genre_white_24dp)
            tabLayout.getTabAt(FRAGMENT_PLAYLISTS)!!.setIcon(R.drawable.ic_playlist_white_24dp)
        }
    }

    private val fragmentReferenceMap = SparseArray<AbstractMediaFragment>()
    private val titlesArray: Array<String> =
            mainActivity.resources.getStringArray(R.array.main_activity_view_pager_titles)

    override fun getItem(index: Int): AbstractMediaFragment {
        val abstractMediaFragment = when (index) {
            FRAGMENT_QUEUE -> QueueFragment()
            FRAGMENT_ARTISTS -> ArtistsFragment()
            FRAGMENT_ALBUMS -> AlbumsFragment()
            FRAGMENT_SONGS -> SongsFragment()
            FRAGMENT_GENRES -> GenresFragment()
            FRAGMENT_PLAYLISTS -> PlaylistsFragment()
            else -> throw IndexOutOfBoundsException("Indexes not up-to-date with adapter.")
        }
        fragmentReferenceMap.append(index, abstractMediaFragment)
        return abstractMediaFragment
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
        fragmentReferenceMap.remove(position)
    }

    override fun getCount() = FRAGMENTS_COUNT

    override fun getPageTitle(position: Int) = titlesArray[position]

    /**
     * @param index the index of the [AbstractMediaFragment] in the [ViewPager].
     * @return the [AbstractMediaFragment] at [index].
     */
    fun getFragmentAt(index: Int) = fragmentReferenceMap[index]!!

    override fun toString() = "MainActivityViewPagerAdapter(fragmentReferenceMap=$fragmentReferenceMap, titlesArray=${Arrays.toString(titlesArray)})"

}
