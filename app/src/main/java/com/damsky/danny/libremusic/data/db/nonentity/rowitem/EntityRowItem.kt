package com.damsky.danny.libremusic.data.db.nonentity.rowitem

import android.content.Context
import android.support.annotation.DrawableRes
import android.support.v7.widget.PopupMenu
import android.view.View
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks

/**
 * An interface for row items that'll be used in the MainActivity's RecyclerViews.
 *
 * @author Danny Damsky
 *
 * @param T a type of a database entity.
 */
interface EntityRowItem<T> {

    /**
     * @return the cover art image path of the current item, or null if nonexistent.
     */
    fun getCoverData(): String?

    /**
     * @return the ID of the placeholder image resource of the current item.
     */
    @DrawableRes
    fun getPlaceHolderImage(): Int

    /**
     * @return the main text to be displayed in the item.
     */
    fun getMainText(): String

    /**
     * @return the secondary text to be displayed in the item.
     */
    fun getSecondaryText(): String

    /**
     * @return the tertiary text to be displayed in the item.
     */
    fun getTertiaryText(): String

    /**
     * @param context the context of the [anchorView].
     * @param anchorView the view that the [PopupMenu] is associated with.
     * @param callbacks implemented callbacks for the onclick events of the [PopupMenu]'s items.
     * @return a fully implemented [PopupMenu] for the row's menu button.
     */
    fun buildPopupMenu(context: Context, anchorView: View, itemPosition: Int, callbacks: MainActivityRecyclerViewAdapterCallbacks<T>): PopupMenu

    /**
     * @return the entity object stored in the row item.
     */
    fun getObject(): T
}