package com.damsky.danny.libremusic.util.loader

import android.content.ContentResolver
import android.provider.MediaStore
import android.util.ArrayMap
import android.util.LongSparseArray
import com.damsky.danny.libremusic.data.db.RoomDbConstants

/**
 * This class is responsible for querying the user's device for genres, and mapping them to the audio
 * files on their device.
 *
 * This class implements the [AutoCloseable] interface, meaning that it must be closed after its
 * use has finished, it also means that the closing can be done with a try-with-resources statement.
 *
 * @param contentResolver used to query the user's device for genres.
 * @author Danny Damsky
 * @see SongLoader
 * @see CoverArtLoader
 * @constructor queries the user's device for genres.
 */
class GenreLoader(contentResolver: ContentResolver) : AutoCloseable {

    private val mapGenreIdToGenreName = LongSparseArray<String>()
    private val mapSongIdToGenreId = ArrayMap<String, Long>()

    init {
        mapGenreIdToGenreName(contentResolver)
    }

    private fun mapGenreIdToGenreName(contentResolver: ContentResolver) {
        contentResolver.query(MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI,
                arrayOf(MediaStore.Audio.Genres._ID,
                        MediaStore.Audio.Genres.NAME), null, null, null)?.use {
            val idColumnIndex = it.getColumnIndex(MediaStore.Audio.Media._ID)
            val innerQueryProjection = arrayOf(MediaStore.Audio.Media._ID)
            while (it.moveToNext()) {
                val genreId = it.getLong(0)
                mapGenreIdToGenreName.put(genreId, it.getString(1))
                mapSongIdToGenreId(contentResolver, genreId, idColumnIndex, innerQueryProjection)
            }
        }
    }

    private fun mapSongIdToGenreId(contentResolver: ContentResolver, genreId: Long, idColumnIndex: Int, innerQueryProjection: Array<String>) {
        contentResolver.query(MediaStore.Audio.Genres.Members.getContentUri("external", genreId),
                innerQueryProjection, null, null, null)?.use {
            while (it.moveToNext())
                mapSongIdToGenreId[it.getString(idColumnIndex)] = genreId
        }
    }

    /**
     * @param songId the id of the song to get the genre for.
     * @return the genre associated with the given songId.
     */
    fun getGenre(songId: String): String {
        val currentGenreId = mapSongIdToGenreId[songId]
        currentGenreId?.let {
            val currentGenreName = mapGenreIdToGenreName[it]
            currentGenreName?.let { genreName -> return genreName }
        }
        return RoomDbConstants.DEFAULT_NAME
    }

    override fun close() {
        mapGenreIdToGenreName.clear()
        mapSongIdToGenreId.clear()
    }

    override fun toString(): String {
        return "GenreLoader(mapGenreIdToGenreName=$mapGenreIdToGenreName, mapSongIdToGenreId=$mapSongIdToGenreId)"
    }

}