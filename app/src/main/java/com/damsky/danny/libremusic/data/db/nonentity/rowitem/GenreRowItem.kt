package com.damsky.danny.libremusic.data.db.nonentity.rowitem

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.content.Context
import android.content.res.Resources
import android.support.v7.util.DiffUtil
import android.view.View
import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.entity.Genre
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplayGenre
import com.damsky.danny.libremusic.ui.activity.main.adapter.recyclerview.MainActivityRecyclerViewAdapterCallbacks
import com.damsky.danny.libremusic.util.PopupMenuBuilder
import com.damsky.danny.libremusic.util.RowItemMenuAction

/**
 * An [EntityRowItem] for the [Genre] entity.
 *
 * @author Danny Damsky
 *
 * @param resources are used to retrieve string resources for the [GenreRowItem].
 * @param displayGenre the entity object for this row item.
 */
class GenreRowItem private constructor(resources: Resources, private val displayGenre: DisplayGenre) : EntityRowItem<Genre> {

    companion object {
        /**
         * An interface implementation used to compare different [GenreRowItem]s.
         */
        @JvmField
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<GenreRowItem>() {
            override fun areItemsTheSame(p0: GenreRowItem, p1: GenreRowItem) = p0 === p1
            override fun areContentsTheSame(p0: GenreRowItem, p1: GenreRowItem) = p0.displayGenre == p1.displayGenre
        }

        /**
         * @param resources are used to retrieve string resources for the [GenreRowItem].
         * @param dbLiveData the [LiveData] object to transform into a [LiveData] of a [List] of
         * [GenreRowItem]s.
         * @return a [LiveData] of a [List] of [GenreRowItem]s which are related to the given
         * [dbLiveData].
         */
        @JvmStatic
        fun getLiveData(resources: Resources, dbLiveData: LiveData<List<DisplayGenre>>) = Transformations.switchMap(dbLiveData) { input ->
            val genreModelList = input.map { GenreRowItem(resources, it) }
            val liveData = MutableLiveData<List<GenreRowItem>>()
            liveData.postValue(genreModelList)
            liveData
        }!!
    }

    private val secondaryText = buildSecondaryText(resources)
    private val popupMenuBuilder = buildPopupMenuBuilder()

    private fun buildSecondaryText(resources: Resources): String {
        val genreSongsCount = displayGenre.songsCount
        return resources.getQuantityString(R.plurals.songs, genreSongsCount, genreSongsCount)
    }

    private fun buildPopupMenuBuilder() = PopupMenuBuilder<Genre>()
            .inflate(R.menu.menu_row_item_artist)
            .setItem(displayGenre.genre)
            .setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.playSongs -> RowItemMenuAction.ACTION_PLAY
                    R.id.addToQueue -> RowItemMenuAction.ACTION_ADD_TO_QUEUE
                    R.id.addSongsToPlaylist -> RowItemMenuAction.ACTION_ADD_TO_PLAYLIST
                    R.id.shareSongs -> RowItemMenuAction.ACTION_SHARE
                    R.id.viewSongs -> RowItemMenuAction.ACTION_VIEW_ALL_SONGS
                    else -> RowItemMenuAction.ACTION_PLAY
                }
            }

    override fun getCoverData() = displayGenre.genre.coverData

    override fun getPlaceHolderImage() = R.drawable.ic_genre_round_72dp

    override fun getMainText() = displayGenre.genre.name

    override fun getSecondaryText() = secondaryText

    override fun getTertiaryText() = ""

    override fun buildPopupMenu(context: Context, anchorView: View, itemPosition: Int, callbacks: MainActivityRecyclerViewAdapterCallbacks<Genre>) =
            popupMenuBuilder
                    .setItemPosition(itemPosition)
                    .setRecyclerViewOnClickListener(callbacks)
                    .build(context, anchorView)

    override fun getObject() = displayGenre.genre

    override fun toString() = "GenreRowItem(displayGenre=$displayGenre)"

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as GenreRowItem

        if (displayGenre != other.displayGenre) return false

        return true
    }

    override fun hashCode() = displayGenre.hashCode()

}