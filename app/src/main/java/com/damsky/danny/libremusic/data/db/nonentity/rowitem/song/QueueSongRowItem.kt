package com.damsky.danny.libremusic.data.db.nonentity.rowitem.song

import com.damsky.danny.libremusic.R
import com.damsky.danny.libremusic.data.db.nonentity.PlayableSong
import com.damsky.danny.libremusic.data.db.nonentity.display.DisplaySong
import com.damsky.danny.libremusic.util.PopupMenuBuilder
import com.damsky.danny.libremusic.util.RowItemMenuAction

/**
 * An implementation of [AbstractSongRowItem] meant to work for songs that are part of a queue.
 *
 * @author Danny Damsky
 * @see AbstractSongRowItem
 *
 * @param song the entity object for this row item.
 */
class QueueSongRowItem private constructor(song: PlayableSong) : AbstractSongRowItem(DisplaySong(song)) {

    companion object {
        /**
         * @param playableSongs the songs to transform into a [List] of [QueueSongRowItem]s.
         * @return a [List] of [QueueSongRowItem]s which are related to the given [playableSongs].
         */
        @JvmStatic
        fun get(playableSongs: List<PlayableSong>) = playableSongs.map { QueueSongRowItem(it) }
    }

    override fun buildPopupMenuBuilder() = PopupMenuBuilder<DisplaySong>()
            .inflate(R.menu.menu_row_item_queue)
            .setItem(this.song)
            .setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.playSongs -> RowItemMenuAction.ACTION_PLAY
                    R.id.removeFromQueue -> RowItemMenuAction.ACTION_REMOVE_FROM_QUEUE
                    R.id.addSongsToPlaylist -> RowItemMenuAction.ACTION_ADD_TO_PLAYLIST
                    R.id.setAsRingtone -> RowItemMenuAction.ACTION_SET_AS_RINGTONE
                    R.id.shareSongs -> RowItemMenuAction.ACTION_SHARE
                    else -> RowItemMenuAction.ACTION_PLAY
                }
            }

}