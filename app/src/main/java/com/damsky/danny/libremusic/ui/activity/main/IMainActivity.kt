package com.damsky.danny.libremusic.ui.activity.main

import android.content.Intent
import android.os.Bundle

/**
 * This interface contains all functions from [MainActivity] that are implemented by it.
 * Implementing this interface will allow the passing of calls from [MainActivity] to the implementing
 * class, allowing multiple implementations of [MainActivity] in separate classes.
 *
 * @author Danny Damsky
 * @see MainActivity
 * @see IMainActivityExtras
 */
interface IMainActivity : IMainActivityExtras {

    /**
     * @see MainActivity.onCreate
     */
    fun onCreate(savedInstanceState: Bundle?)

    /**
     * @see MainActivity.onBackPressed
     */
    fun onBackPressed()

    /**
     * @see MainActivity.onRequestPermissionsResult
     */
    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)

    /**
     * @see MainActivity.onActivityResult
     */
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)

}