package com.damsky.danny.libremusic.data.db.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import com.damsky.danny.libremusic.data.db.RoomDbConstants

/**
 * This class represents the artists table in the database.
 *
 * Relationships:
 * Artist 1-N Albums
 * Artist 1-N Songs
 *
 * @author Danny Damsky
 *
 * @param id the ID of the artist (auto-increment).
 * @param name the name of the artist.
 * @param coverData the path to the artist's cover art image path.
 */
@Entity(tableName = RoomDbConstants.TABLE_NAME_ARTISTS,
        indices = [Index(value = [RoomDbConstants.COLUMN_ARTIST_NAME], unique = true)])
data class Artist(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = RoomDbConstants.COLUMN_ARTIST_ID)
        val id: Long?,
        @ColumnInfo(name = RoomDbConstants.COLUMN_ARTIST_NAME)
        val name: String,
        @ColumnInfo(name = RoomDbConstants.COLUMN_ARTIST_COVER_DATA)
        val coverData: String?) {

    /**
     * A builder class for the [Artist] entity.
     *
     * @author Danny Damsky
     */
    class Builder {
        private var id: Long? = null
        private lateinit var name: String
        private var coverData: String? = null

        /**
         * @param id the ID of the artist.
         */
        fun setId(id: Long?) = apply {
            this.id = id
        }

        /**
         * @param name the name of the artist.
         */
        fun setName(name: String) = apply {
            this.name = name
        }

        /**
         * @param coverData the path to the artist's cover art image path.
         */
        fun setCoverData(coverData: String?) = apply {
            this.coverData = coverData
        }

        /**
         * @return a new [Artist] object with the properties set in this class.
         */
        fun build() = Artist(id, name, coverData)

        override fun toString() = "Builder(id=$id, name='$name', coverData=$coverData)"
    }
}
