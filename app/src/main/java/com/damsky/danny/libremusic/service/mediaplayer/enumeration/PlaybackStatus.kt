package com.damsky.danny.libremusic.service.mediaplayer.enumeration

/**
 * This enum is used to indicate the status of the MediaPlayer in MediaPlayerService.
 *
 * @author Danny Damsky
 */
enum class PlaybackStatus {
    PLAYING,
    PAUSED
}